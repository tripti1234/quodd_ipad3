//
//  TickerLookupCell.h
//  Quodd_iPad
//
//  Created by Nishant on 28/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TickerLookupCell : UITableViewCell

@property (nonatomic, retain) NSArray *arrayOfKeys;

@property (nonatomic, retain) IBOutlet UIButton *ticker;
@property (nonatomic, retain) IBOutlet UIButton *company;
@property (nonatomic, retain) IBOutlet UIButton *exchg;
@property (nonatomic, retain) IBOutlet UIButton *type;
@property (nonatomic, retain) IBOutlet UIButton *currency;
@property (nonatomic, retain) IBOutlet UIButton *cusip;

@property (nonatomic, retain) IBOutlet UILabel *copiedLabel;

- (void)loadWithDictionary:(NSDictionary*)dict;

- (IBAction)copyToPasteBoard:(UIButton*)senderButton;

- (IBAction)showDetailAlert:(UIButton*)senderButton;

@end
