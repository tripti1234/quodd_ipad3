//
//  AlertSummaryVC.m
//  Quodd_iPad2
//
//  Created by Nishant on 12/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "AlertSummaryVC.h"
#import "AlertSummaryCell.h"
#import <QuartzCore/QuartzCore.h>

#define TABLE_ROW_HEIGHT 35

#define ACTIVE_TABLE_TAG 1233
#define HIST_TABLE_TAG 3245

#define HIST_BATCH_SIZE 20

#define kActiveAlertCell        @"ActiveAlertCell"
#define kHistoricalAlertCell    @"HistoricalAlertCell"

#define kAlertId                @"alertId"

@interface AlertSummaryVC ()

@property (nonatomic, retain) NSDictionary *columnNames;

@property (nonatomic, retain) NSMutableArray *activeAlerts;

@property (nonatomic, retain) NSMutableArray *historicalAlerts;
@property (nonatomic, retain) NSMutableArray *alertsToDelete;
@property (nonatomic) NSInteger fetchedHistCount;
@property (nonatomic) NSInteger totalHistCount;

@end

@implementation AlertSummaryVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = VIEW_BACKGROUND_IMAGE;
    
    _summaryTable.layer.cornerRadius = 10.0;
    
    _columnNames = [[NSDictionary alloc] initWithObjectsAndKeys:
                    @"Symbol", @"ticker",
                    @"Component", @"component",
                    @"Condition", @"condition",
                    @"Value", @"value",
                    @"Alert Name", @"name",
                    @"Comment", @"comments",
                    //@"N", @"newsId",
                    @"Edit", @"editButton",
                    
                    @"☑", @"checkBox",
                    @"Date/Time", @"date",
                    @"Delete", @"deleteButton",
                    nil];

    
    [_summarySegment setBackgroundImage:[UIImage imageNamed:@"ControlBg.png"]  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_summarySegment setBackgroundImage:[UIImage imageNamed:@"ControlBg-ActiveLeft.png"]  forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    _alertsToDelete = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [_alertsToDelete removeAllObjects];
    
    [self fetchActiveAlerts];
    
    NSString *getTotalHistCount = [NSString stringWithFormat:HIST_ALERTS_COUNT_URL, currentSessionID];
    NSDictionary *response = [ProjectHandler dataFromAPIWithStringURL:getTotalHistCount];
    _totalHistCount = [response[@"count"] integerValue];

    [self getMoreHistorical:_first];
}

- (void)fetchActiveAlerts
{
    [_summaryLoading startAnimating];

    NSString *newsSource = isDowJonesEntitled ? @"DOWJONES" : @"COMTEX";
     
    NSString *activeAlertsURL = [NSString stringWithFormat:ACTIVE_ALERTS_URL, currentSessionID, newsSource];
    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:activeAlertsURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             _activeAlerts = [[NSMutableArray alloc] initWithArray:JSON];
                                             
                                             for (int i=0; i<_activeAlerts.count; i++) {
                                                 
                                                 NSMutableDictionary *dict = [_activeAlerts[i] mutableCopy];
                                                 [_activeAlerts replaceObjectAtIndex:i withObject:[self alertTypeMappingInDict:dict]];
                                             }
                                             
                                             [_summaryTable reloadData];
                                             
                                             [_summaryLoading stopAnimating];

                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [[[UIAlertView alloc] initWithTitle:@"Error Retrieving Active Alerts"
                                                                         message:error.localizedDescription.capitalizedString
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];

                                             [_summaryLoading stopAnimating];

                                         }];
    [operation start];
}

- (void)fetchHistoricalAlerts
{
    [_summaryLoading startAnimating];
    [_summaryTable reloadData];

    NSString *historicalAlertsURL = [NSString stringWithFormat:HIST_ALERTS_LIST_URL, currentSessionID, _fetchedHistCount, _fetchedHistCount+HIST_BATCH_SIZE];
    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:historicalAlertsURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             if (!_historicalAlerts) {
                                                 _historicalAlerts = [[NSMutableArray alloc] initWithArray:JSON];
                                             }
                                             [_historicalAlerts removeAllObjects];
                                             
                                             _historicalAlerts = [NSMutableArray arrayWithArray:JSON];
                                             
                                             for (int i=0; i<_historicalAlerts.count; i++) {
                                                 
                                                 NSMutableDictionary *dict = [_historicalAlerts[i] mutableCopy];
                                                 [_historicalAlerts replaceObjectAtIndex:i withObject:[self alertTypeMappingInDict:dict]];
                                             }
                                             
                                             [_summaryTable reloadData];
                                             
                                             _fetchedHistCount += _historicalAlerts.count;
                                             
                                             [_summaryLoading stopAnimating];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [[[UIAlertView alloc] initWithTitle:@"Error Retrieving Historical Alert"
                                                                         message:error.localizedDescription
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                             
                                             [_summaryLoading stopAnimating];
                                             
                                         }];
    [operation start];
}

- (NSMutableDictionary*)alertTypeMappingInDict:(NSMutableDictionary*)dict
{
    NSString *alertType = dict[@"type"];
    
    NSString *component = @"component"; 
    NSString *condition = @"condition";             //sign
    
    if ([alertType hasPrefix:@"A"]) {
        [dict setValue:@"ASK" forKey:component];
        
        if ([alertType hasSuffix:@"O"]) {
            [dict setValue:@">" forKey:condition];
        }
        else if ([alertType hasSuffix:@"U"]) {
            [dict setValue:@"<" forKey:condition];
        }
        else if ([alertType hasSuffix:@"E"]) {
            [dict setValue:@"=" forKey:condition];
        }
    }
    else if ([alertType hasPrefix:@"B"]) {
        [dict setValue:@"BID" forKey:component];
        
        if ([alertType hasSuffix:@"O"]) {
            [dict setValue:@">" forKey:condition];
        }
        else if ([alertType hasSuffix:@"U"]) {
            [dict setValue:@"<" forKey:condition];
        }
        else if ([alertType hasSuffix:@"E"]) {
            [dict setValue:@"=" forKey:condition];
        }
        if ([alertType hasSuffix:@"1"]) {
            [dict setValue:@"New 52 week high" forKey:component];
        }
        else if ([alertType hasSuffix:@"2"]) {
            [dict setValue:@"New 52 week low" forKey:component];
        }
    }
    else if ([alertType hasPrefix:@"C"]) {
        [dict setValue:@"CHANGE" forKey:component];
        
        if ([alertType hasSuffix:@"U"]) {
            [dict setValue:@"↑" forKey:condition];
        }
        else if ([alertType hasSuffix:@"D"]) {
            [dict setValue:@"↓" forKey:condition];
        }
        else if ([alertType hasSuffix:@"1"]) {
            [dict setValue:@"CNBC TV Appearance Today" forKey:component];
        }
        else if ([alertType hasSuffix:@"4"]) {
            [dict setValue:@"Stock Split Announced Today" forKey:component];
        }
        else if ([alertType hasSuffix:@"6"]) {
            [dict setValue:@"Stock Buyback Announced Today" forKey:component];
        }
        else if ([alertType hasSuffix:@"7"]) {
            [dict setValue:@"New Products/Services Announced Today" forKey:component];
        }
        else if ([alertType hasSuffix:@"9"]) {
            [dict setValue:@"Upgraded Within Last 24 hrs" forKey:component];
        }
        else if ([alertType hasSuffix:@"10"]) {
            [dict setValue:@"Downgraded Within Last 24 hrs" forKey:component];
        }
        else if ([alertType hasSuffix:@"11"]) {
            [dict setValue:@"New Coverage Within Last 24 hrs" forKey:component];
        }
        else if ([alertType hasSuffix:@"12"]) {
            [dict setValue:@"Earnings are being Reported Today" forKey:component];
        }
        else if ([alertType hasSuffix:@"13"]) {
            [dict setValue:@"Insider Activity Reported Today" forKey:component];
        }
        else if ([alertType hasSuffix:@"15"]) {
            [dict setValue:@"in News Today" forKey:component];
        }
        else if ([alertType hasSuffix:@"17"]) {
            [dict setValue:@"Buyout/Merger Talks Today" forKey:component];
        }
    }
    else if ([alertType hasPrefix:@"L"]) {
        [dict setValue:@"LAST" forKey:component];

        if ([alertType hasSuffix:@"O"]) {
            [dict setValue:@">" forKey:condition];
        }
        else if ([alertType hasSuffix:@"U"]) {
            [dict setValue:@"<" forKey:condition];
        }
        else if ([alertType hasSuffix:@"E"]) {
            [dict setValue:@"=" forKey:condition];
        }
    }
    else if ([alertType hasPrefix:@"V"]) {
        [dict setValue:@"VOLUME" forKey:component];
        
        if ([alertType hasSuffix:@"O"]) {
            [dict setValue:@">" forKey:condition];
        }
        else if ([alertType hasSuffix:@"E"]) {
            [dict setValue:@"=" forKey:condition];
        }
        
    }
    
    return dict;
}

#pragma mark - IBActions
- (IBAction)backPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)alertSummaryTypeChanged:(UISegmentedControl*)senderSegment
{
    NSString *segmentImage = [NSString stringWithFormat:@"ControlBg-Active%@.png", _summarySegment.selectedSegmentIndex ? @"Right" : @"Left"];
    
    [_summarySegment setBackgroundImage:[UIImage imageNamed:segmentImage]  forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];

    switch (senderSegment.selectedSegmentIndex) {
            
        case 0:
            _summaryTypeLabel.text = @"ACTIVE  USER  ALERTS";
            [_summaryTable setFrame:CGRectMake(10, 100, 1004, 638)];

            [self fetchActiveAlerts];
            
            break;
            
        case 1:
            _summaryTypeLabel.text = @"HISTORICAL  USER  ALERTS";
            [_summaryTable setFrame:CGRectMake(10, 100+50, 1004, 638-50)];
            
            _fetchedHistCount = 0;

            [self fetchHistoricalAlerts];
            
            break;
    }

    [_summaryTable reloadData];
}

- (IBAction)selectAllAlerts
{
    for (NSDictionary *dict in _historicalAlerts)
    {
        NSString *alertID = dict[kAlertId];
        
        if (![_alertsToDelete containsObject:alertID]) {
            [_alertsToDelete addObject:alertID];
        }
    }
    
    [_summaryTable reloadData];
}

- (IBAction)clearSelectedAlerts
{
    [_alertsToDelete removeAllObjects];
    [_summaryTable reloadData];
}

- (IBAction)deleteSelectedAlerts
{
    for (NSString *alertId in _alertsToDelete)
    {
        [self deleteAlertWithID:alertId];
    }
}

- (IBAction)getMoreHistorical:(UIButton*)senderButton
{    
    if (senderButton == _first)
    {
        _fetchedHistCount = 0;
    }
    else if (senderButton == _previous)
    {
        _fetchedHistCount -= 2*HIST_BATCH_SIZE;
        
        if (_fetchedHistCount < 0) _fetchedHistCount = 0;
    }
    else
    {
        if (senderButton == _last)
        {
            _fetchedHistCount = _totalHistCount - HIST_BATCH_SIZE;
            
            if (_fetchedHistCount < 0) _fetchedHistCount = 0;
        }
    }
    
    _first.hidden = _fetchedHistCount == 0;
    _previous.hidden = _fetchedHistCount == 0;
    _next.hidden = _totalHistCount - _fetchedHistCount < HIST_BATCH_SIZE || _fetchedHistCount < HIST_BATCH_SIZE;
    _last.hidden = _totalHistCount - _fetchedHistCount < HIST_BATCH_SIZE || _fetchedHistCount < HIST_BATCH_SIZE;
    
    [self fetchHistoricalAlerts];
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _summarySegment.selectedSegmentIndex==0 ? _activeAlerts.count : _historicalAlerts.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return TABLE_ROW_HEIGHT;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AlertSummaryCell *cell;
    
    if (cell == nil)
    {
        NSString *nibName = _summarySegment.selectedSegmentIndex==0 ? kActiveAlertCell : kHistoricalAlertCell;
        
        NSArray *topObjects = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:nil];
        
        for (id currentObject in topObjects)
        {
            if ([currentObject isKindOfClass:[AlertSummaryCell class]])
            {
                cell = (AlertSummaryCell *)currentObject;
                break;
            }
        }
        
        cell.backgroundColor = [UIColor blackColor];
        cell.delegate = self;
    }
    
    BOOL isAllSelected = _alertsToDelete.count;
    
    [cell loadCellAtIndex:NSNotFound withDictionary:_columnNames selected:isAllSelected];
        
    for (UIView *subview  in cell.contentView.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton*)subview;
            button.titleLabel.textColor = [UIColor whiteColor];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_ROW_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    static NSString *CellIdentifier = kActiveAlertCell;
    
    AlertSummaryCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSString *nibName = _summarySegment.selectedSegmentIndex==0 ? kActiveAlertCell : kHistoricalAlertCell;
        
        NSArray *topObjects = [[NSBundle mainBundle] loadNibNamed:nibName
                                                            owner:self
                                                          options:nil];
        for (id currentObject in topObjects)
        {
            if ([currentObject isKindOfClass:[AlertSummaryCell class]])
            {
                cell = (AlertSummaryCell *)currentObject;
                break;
            }
        }
        
        cell.delegate = self;
    }
    
    if (_summarySegment.selectedSegmentIndex==0) {
        
        [cell loadCellAtIndex:indexPath.row withDictionary:_activeAlerts[indexPath.row] selected:NO];
    }
    else {
        
        BOOL isSelected = [_alertsToDelete containsObject:_historicalAlerts[indexPath.row][kAlertId]];
        [cell loadCellAtIndex:indexPath.row withDictionary:_historicalAlerts[indexPath.row] selected:isSelected];
    }
    
    return cell;
}

#pragma mark - AlertSummaryCellDelegate
- (void)checkBoxAtIndex:(NSInteger)index
{
    if (index == NSNotFound) {
        
        _alertsToDelete.count ? [self clearSelectedAlerts] : [self selectAllAlerts];
    }
    else {
        
        NSString *alertID = _historicalAlerts[index][kAlertId];
        
        if ([_alertsToDelete containsObject:alertID]) {
            [_alertsToDelete removeObject:alertID];
        }
        else [_alertsToDelete addObject:alertID];
    }
}

- (void)editDeleteAlertAtIndex:(NSInteger)index
{
    NSString *alert;
    
    switch (_summarySegment.selectedSegmentIndex) {

        case 0: //EDIT button
            
            alert = _activeAlerts[index][@"name"];
            [self editAlertWithName:alert];
            
            break;
            
        case 1: //DELETE button

            alert = _historicalAlerts[index][kAlertId];
            [self deleteAlertWithID:alert];
            
            if ([_alertsToDelete containsObject:alert]) {
                [_alertsToDelete removeObject:alert];
            }
            
            break;
    }
}

- (void)deleteAlertWithID:(NSString*)alertID
{
    NSString *deleteAlertURL = [NSString stringWithFormat:HIST_ALERTS_DELETE_URL, currentSessionID, alertID];
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:deleteAlertURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSDictionary *deleteResponse = [NSDictionary dictionaryWithDictionary:JSON];
                                             
                                             if ([deleteResponse[@"status"] boolValue])
                                             {
                                                 for (int i=0; i<_historicalAlerts.count; i++)
                                                 {
                                                     NSString *currentAlertID = [NSString stringWithFormat:@"%@", _historicalAlerts[i][kAlertId]];
                                                     NSString *deletedAlertID = [NSString stringWithFormat:@"%@", alertID];
                                                     
                                                     if ([currentAlertID isEqualToString:deletedAlertID])
                                                     {    
                                                         [_historicalAlerts removeObjectAtIndex:i];
                                                         [_alertsToDelete removeObject:alertID];
                                                         
                                                         _totalHistCount--;
                                                         
                                                         [_summaryTable deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]]
                                                                              withRowAnimation:UITableViewRowAnimationAutomatic];
                                                         break;
                                                     }
                                                 }
                                             }
                                             else
                                                 [[[UIAlertView alloc] initWithTitle:@"Delete Failed!"
                                                                             message:@"Status = False\nPlease try again."
                                                                            delegate:nil
                                                                   cancelButtonTitle:@"OK"
                                                                   otherButtonTitles:nil] show];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [[[UIAlertView alloc] initWithTitle:@"Error Deleting Alert!"
                                                                         message:error.localizedDescription.capitalizedString
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                             
                                         }];
    [operation start];
}

- (void)editAlertWithName:(NSString*)name
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kLoadAlertToEdit
                                                            object:nil
                                                          userInfo:@{@"alertName": name}];       
    }];
}

@end
