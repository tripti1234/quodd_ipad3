//
//  ChartsViewController.m
//  Quodd_iPad3
//
//  Created by Nishant on 31/07/13.
//  Copyright (c) 2013 Paxcel Technologies. All rights reserved.
//

#import "ChartsViewController.h"
#import <QuartzCore/QuartzCore.h>

#define CHART_URL @"http://chart.finance.yahoo.com/z?z=l"

@interface ChartsViewController ()

@property (nonatomic, retain) NSString *timeSpan;
@property (nonatomic, retain) NSString *chartType;
@property (nonatomic, retain) NSMutableArray *compareWith;

@end

@implementation ChartsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = VIEW_BACKGROUND_IMAGE;
    _chartImage.layer.cornerRadius = 5.0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [Flurry logEvent:@"Charts View"];

    _tickerID.text = _tickerName;
    _compareWith = [[NSMutableArray alloc] init];
    
    _timeSpan = @"1d";
    _chartType = @"l";
    
    [self loadChart];
}

- (void)loadChart
{
    [_indicator startAnimating];

    NSString *chartURL = [CHART_URL stringByAppendingFormat:@"&s=%@&t=%@&q=%@", _tickerName, _timeSpan, _chartType];
    
    if (_compareWith.count>0) {
        chartURL = [chartURL stringByAppendingFormat:@"&c=%@",[_compareWith componentsJoinedByString:@","]];
    }
    
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:chartURL]];
    
    AFImageRequestOperation *operation = [AFImageRequestOperation
                                          imageRequestOperationWithRequest:request
                                          imageProcessingBlock:nil
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              _chartImage.image = image;
                                              [_indicator stopAnimating];

                                          } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                             
                                              //NSLog(@"%@", error.localizedDescription);
                                              [_indicator stopAnimating];
                                          }];
    
    [operation start];
}

#pragma mark - IBActions
- (IBAction)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)segmentChanged:(UISegmentedControl*)senderSegment
{
    if (senderSegment == _timeSpanSegment)
    {
        NSArray *timeSpan = @[@"1d", @"5d", @"3m",@"6m", @"1y", @"2y", @"5y", @"my"];
        _timeSpan = timeSpan[senderSegment.selectedSegmentIndex];
    }
    else if (senderSegment == _chartTypeSegment)
    {
        NSArray *chartType = @[@"l", @"b", @"c"];
        _chartType = chartType[senderSegment.selectedSegmentIndex];
    }
    
    [self loadChart];
}

- (IBAction)addToCompare
{
    UIAlertView *compareAlert = [[UIAlertView alloc] initWithTitle:@"Add Ticker To Compare"
                                                           message:nil
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                                 otherButtonTitles:@"OK", nil];
    
    compareAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    
    [[compareAlert textFieldAtIndex:0] setTextAlignment:NSTextAlignmentCenter];
    
    [compareAlert show];
}

- (IBAction)resetCompare
{
    [_compareWith removeAllObjects];
    [self loadChart];
}

#pragma mark - UIAlertViewDelegate
- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    NSString *text = [[alertView textFieldAtIndex:0] text];
    
    if (text.length==0) return NO;
        
    NSCharacterSet *letters = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    
    BOOL isFirstLetter = text.length==0;
    BOOL isLetter = [text rangeOfCharacterFromSet:letters].location != NSNotFound;
    
    if (isFirstLetter)
        return isLetter;
    
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~#$%^*-+=\\/:."] invertedSet];
    
    BOOL isValidCharacter = ([text rangeOfCharacterFromSet:notAllowedChars].location == NSNotFound);
    
    return isValidCharacter;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        
        NSString *newTicker = [[[alertView textFieldAtIndex:0] text] uppercaseString];
        
        if (newTicker.length && ![_compareWith containsObject:newTicker] && ![_tickerName isEqualToString:newTicker]) {
            [_compareWith addObject:newTicker];
        }
        else [[[UIAlertView alloc] initWithTitle:@"Error"
                                         message:@"Symbol already exists"
                                        delegate:nil
                               cancelButtonTitle:@"OK"
                               otherButtonTitles:nil] show];
        
        [self loadChart];
    }
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length>0) {
        _tickerName = textField.text.uppercaseString;
        textField.text = _tickerName;
        [_compareWith removeAllObjects];
        [self loadChart];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
