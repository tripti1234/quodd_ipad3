//
//  HGMessageHUD.m
//  HGMessageHUD
//
//  Created by Paxcel Technologies on 27/12/12.
//  Copyright (c) 2012 Paxcel Technologies. All rights reserved.
//

#import "HGMessageHUD.h"
#import <QuartzCore/QuartzCore.h>

#define kFontName @"Helvetica Neue"
#define kFontSize 15
#define kAnimationDuration 1

#define isPortrait UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])

@implementation HGMessageHUD

+ (void) showInView : (UIView *) view withMessage: (NSString *) message
{
    NSUInteger length = [message length]+2;
    CGFloat width = length * kFontSize;
    CGFloat maxWidth = view.window.frame.size.width * 0.75;
    CGFloat height;
    
    if (width > maxWidth)
    {
        height = ((width/maxWidth) * kFontSize) + kFontSize;
        width = maxWidth;
    }
    else
    {
        height = kFontSize * 3;
    }
    
    HGMessageHUD *HUD = [[HGMessageHUD alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    HUD.text = [NSString stringWithFormat:@" %@ ", message];
    HUD.backgroundColor = [UIColor darkGrayColor];
    HUD.layer.cornerRadius = 5;
    HUD.layer.masksToBounds = YES;
    HUD.textColor = [UIColor whiteColor];
    HUD.font = [UIFont fontWithName:kFontName size:kFontSize];
    HUD.textAlignment = NSTextAlignmentCenter;
    HUD.numberOfLines = 0;
    
    HUD.frame = CGRectMake(HUD.frame.origin.x,
                           HUD.frame.origin.y + (view.window.frame.size.height/4),
                           HUD.frame.size.width,
                           HUD.frame.size.height);
    
    HUD.center = view.window.center;

    [view.window addSubview: HUD];
    
    if (!isPortrait)
    {
        if ([[UIApplication sharedApplication] statusBarOrientation] != UIInterfaceOrientationLandscapeLeft)
            HUD.transform = CGAffineTransformMakeRotation(M_PI_2);
        else
            HUD.transform = CGAffineTransformMakeRotation(-M_PI_2);
    }
    
    [NSTimer scheduledTimerWithTimeInterval:1
                                     target:self
                                   selector:@selector(startAnimation:)
                                   userInfo:HUD
                                    repeats:NO];
}

+ (void) startAnimation: (NSTimer *) timer
{
    HGMessageHUD *HUD = (HGMessageHUD *)[timer userInfo];
    [UIView beginAnimations:nil context:(__bridge void *)(HUD)];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView setAnimationDuration:kAnimationDuration];
    HUD.alpha = 0.0;
    [UIView commitAnimations];
    
}

+ (void)animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    HGMessageHUD *HUD = (__bridge HGMessageHUD *)context;
    [HUD removeFromSuperview];
}

@end
