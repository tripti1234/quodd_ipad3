//
//  NewsDetailWebViewController.m
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 3/21/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "NewsDetailWebViewController.h"
#import "NewsDetailCell.h"

@interface NewsDetailWebViewController ()

@property (nonatomic,retain) NSString *newsSource;

@end

@implementation NewsDetailWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = VIEW_BACKGROUND_IMAGE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [_newsTable reloadData];     //IMPORTANT against assertion failure.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [Flurry logEvent:@"Breaking News"];

    _newsTable.scrollEnabled = YES;

    _newsSource = _dowJonesFlag ? @"DOWJONES" : @"COMTEX";
    
    if (_openSectionIndex != NSNotFound) {
        
        [_newsTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:_openSectionIndex]
                         atScrollPosition:UITableViewScrollPositionTop
                                 animated:YES];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    _openSectionIndex = NSNotFound;
    [_newsTable reloadData];
}

- (IBAction)backPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _newsList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==_openSectionIndex) {
        return 1;
    }
    else return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 650;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50.0;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{    
    BOOL isRead = [_isNewsRead containsObject:[NSString stringWithFormat:@"%@", _newsList[section][@"id"]]];
    
    NewsHeadlineView *headerView = [[NewsHeadlineView alloc]
                                    initWithFrame:CGRectMake(0, 0, _newsTable.bounds.size.width, 50.0)
                                    inSection:section
                                    WithDictionary:_newsList[section]
                                    andStatus:isRead];
    
    headerView.delegate = self;
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NewsDetailCell";
    
    NewsDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        NSArray *topObjects = [[NSBundle mainBundle] loadNibNamed:@"NewsDetailCell" owner:self options:nil];
        for (id currentObject in topObjects)
        {
            if ([currentObject isKindOfClass:[NewsDetailCell class]])
            {
                cell = (NewsDetailCell *)currentObject;
                break;
            }
        }
    }
    
    [cell loadNewsForURL:[NSString stringWithFormat:COMSTOCK_NEWS_DETAIL_URL,_newsList[indexPath.section][@"id"],_newsSource]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //no action
}

#pragma mark - NewsHeadlineView Delegate

- (NSInteger) openSectionIndex
{
    return  _openSectionIndex;
}

- (void) sectionClosed : (NSInteger) section
{
    [_newsTable beginUpdates];
    self.openSectionIndex = NSNotFound;

    NSIndexPath *indexPathToDelete = [NSIndexPath indexPathForRow:0
                                                        inSection:section];
    [_newsTable deleteRowsAtIndexPaths:@[indexPathToDelete]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [_newsTable endUpdates];
    
    _newsTable.scrollEnabled = YES;
    
    [_newsTable performSelector:@selector(reloadData) withObject:nil afterDelay:0.5];
}

- (void) sectionOpened : (NSInteger) section
{
    NSInteger previousOpenIndex = self.openSectionIndex;
    
    NSIndexPath *indexPathToInsert = [NSIndexPath indexPathForRow:0 inSection:section];
    NSIndexPath *indexPathToDelete;
    
    if (previousOpenIndex != NSNotFound)
    {
        for (NewsHeadlineView *subview in _newsTable.subviews)
        {
            if ([subview isKindOfClass:[NewsHeadlineView class]] && subview.section!=section)
            {
                [subview toggleButtonPressed:FALSE isSelected:NO];
                break;
            }
        }
        
        indexPathToDelete = [NSIndexPath indexPathForRow:0 inSection:previousOpenIndex];
    }
    
    self.openSectionIndex = section;

    //[_isNewsRead setValue:[NSNumber numberWithBool:YES] forKey:_newsList[section][@"mLastNewsID"]];
    
    if (![_isNewsRead containsObject:[NSString stringWithFormat:@"%@", _newsList[section][@"id"]]])
        [_isNewsRead addObject:[[NSString alloc] initWithFormat:@"%@", _newsList[section][@"id"]]];
    
    [_newsTable beginUpdates];
    [_newsTable insertRowsAtIndexPaths:@[indexPathToInsert] withRowAnimation:UITableViewRowAnimationAutomatic];

    if (indexPathToDelete)
        [_newsTable deleteRowsAtIndexPaths:@[indexPathToDelete] withRowAnimation:UITableViewRowAnimationAutomatic];
  
    [_newsTable endUpdates];
    
    [_newsTable scrollToRowAtIndexPath:indexPathToInsert
                     atScrollPosition:UITableViewScrollPositionTop
                             animated:YES];
    
    _newsTable.scrollEnabled = NO;
    
}

@end