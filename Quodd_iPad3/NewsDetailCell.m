//
//  NewsDetailCell.m
//  Quood_iPad
//
//  Created by Nishant on 14/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "NewsDetailCell.h"

@interface NewsDetailCell ()

@property (nonatomic, retain) IBOutlet UIWebView *newsWebView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *newsActivity;

@end

@implementation NewsDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)loadNewsForURL:(NSString*)urlString
{
    NSURL *newsURL = [NSURL URLWithString:urlString];
    [_newsWebView loadRequest:[NSURLRequest requestWithURL:newsURL]];
    
    _newsWebView.backgroundColor = VIEW_BACKGROUND_IMAGE;
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_newsActivity startAnimating];
    webView.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    float textScaleFactor = 150.0;
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%f%%'", textScaleFactor];
    [webView stringByEvaluatingJavaScriptFromString:jsString];

    [_newsActivity stopAnimating];
    webView.hidden = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [_newsActivity stopAnimating];
    webView.hidden = NO;
}

@end
