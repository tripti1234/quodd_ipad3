//
//  SettingsViewController.m
//  Quodd_iPad3
//
//  Created by Nishant on 11/09/13.
//  Copyright (c) 2013 Paxcel Technologies. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = VIEW_BACKGROUND_IMAGE;
    
    [Flurry logEvent:@"Settings View"];

    _userNameLabel.text = [NSString stringWithFormat:@"Logged in as: %@", currentUserName];
    
    _standardOSI.selected = !IS_DISPLAY_TYPE_ALTERNATE;
    _alternateOSI.selected = IS_DISPLAY_TYPE_ALTERNATE;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark - IBActions
- (IBAction)closeView
{
    [self dismissViewControllerAnimated:YES completion:^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSettingsUpdated
                                                            object:nil
                                                          userInfo:nil];
    }];
}

- (IBAction)optionsDisplayType:(UIButton*)senderButton
{
    BOOL ifStandardButton = senderButton == _standardOSI;
    
    _standardOSI.selected = ifStandardButton;
    _alternateOSI.selected = !ifStandardButton;
    
//    kSetValueForBool(kOptionShowType, !ifStandard);
    kSetOptionDisplayTypeAlternate(!ifStandardButton);
}

@end
