//
//  ProjectHandler.m
//  Quodd_iPad2
//
//  Created by Nishant on 25/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "ProjectHandler.h"
#import "Reachability.h"
#import <Security/Security.h> //for saving device id in keychain access

#define kServiceName    @"com.mycompany.myAppServiceName"
#define kDEVICEID       @"DEVICEID"

@implementation ProjectHandler

+ (id)sharedHandler
{
    static ProjectHandler *sharedHandler = nil;
    static dispatch_once_t onceToken;
    
    //By using the dispatch_once method from GCD, we ensure that it’s only created once. This is thread safe & handled entirely by the OS.
    
    dispatch_once(&onceToken, ^{
        sharedHandler = [[self alloc] init];
    });
    return sharedHandler;
}

- (id)init
{
    if (self = [super init]) {
        _subscribedSymbols = [[NSMutableArray alloc] init];
        _pendingSymbols = [[NSMutableArray alloc] init];
        _nonSubscribedSymbols = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark -
+ (BOOL)checkNetworkConnection
{
    Reachability *reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    if (![reachability isReachable])
        [[[UIAlertView alloc] initWithTitle:@"Network unavailable"
                                    message:@"We could not connect to the Internet. Check your settings/connection."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    
    return [reachability isReachable];
}

+ (id)dataFromAPIWithStringURL:(NSString *)urlString
{
    if ([self checkNetworkConnection])
    {
        __autoreleasing NSError *error;
        
        NSURLRequest *request = [self urlRequestWithURL:urlString];
        
        NSHTTPURLResponse *requestResponse = [[NSHTTPURLResponse alloc]init];
        NSData *data = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&requestResponse
                                                         error:&error];
        
        if (!error)
        {
            id jsonResponse = [NSJSONSerialization JSONObjectWithData:data
                                                              options:kNilOptions
                                                                error:&error];
            return jsonResponse;
        }
        else
        {
//            [self performSelectorOnMainThread:@selector(showAlert:)
//                                   withObject:error.localizedDescription
//                                waitUntilDone:YES];
            return nil;
        }
    }
    
    return nil;
}

+ (NSMutableURLRequest*)urlRequestWithURL:(NSString*)urlString
{
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    urlString = [urlString stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [request setTimeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];

    //header fields are case sensitive.
    //[self deleteKeychainValue:kDEVICEID];
    
    NSString *deviceID;
    
    NSData *deviceIdData = [self searchKeychainCopyMatching:kDEVICEID];
    if (deviceIdData) {
        deviceID = [[NSString alloc] initWithData:deviceIdData encoding:NSUTF8StringEncoding];
    }
    else {
        deviceID = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, CFUUIDCreate(NULL)));
        [self createKeychainValue:deviceID forIdentifier:kDEVICEID];
    }
    
    [request setValue:deviceID forHTTPHeaderField:kDEVICEID];

    NSString *deviceOS = [NSString stringWithFormat:@"iOS %@", [[UIDevice currentDevice] systemVersion]];
    [request setValue:deviceOS forHTTPHeaderField:@"ENVIRONMENT"];                  //ANDROID -- to get snap with new format
    
    [request setValue:[[UIDevice currentDevice] model] forHTTPHeaderField:@"DEVICE"];
    [request setValue:@"1.0.0" forHTTPHeaderField:@"VERSION"];
    [request setValue:[NSString stringWithFormat:@"%@", currentUserID] forHTTPHeaderField:@"USERID"];
    
    //NSLog(@"Request with URL: %@", urlString);
    
    return request;
}

+ (void)showAlert:(NSString *)msg
{
    NSString *title;
    
    if (!([msg rangeOfString:@"Server"].location==NSNotFound) || !([msg rangeOfString:@"Internet"].location==NSNotFound))
    {
        title = @"Network Unavailable";
    }
    else
    {
        title = @"Time Out";
    }
    
    [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

+ (void)saveLogWithText:(NSString*)appendText
{
    //save log to APIlogfile.txt file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"APIlogfile.txt"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]){
        [[NSData data] writeToFile:path atomically:YES];
    }
    
    // append to APIlogfile.txt
    NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:path];
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[NSLocale currentLocale]];
    [df setDateStyle:NSDateFormatterFullStyle];
    [df setTimeStyle:NSDateFormatterLongStyle];
    NSString *now = [df stringFromDate:[NSDate date]];
    
    [handle writeData:[[NSString stringWithFormat:@"%@ - %@\n", now, appendText] dataUsingEncoding:NSUTF8StringEncoding]];
}

#pragma mark - Keychain Access
+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier
{
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];
    
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedIdentifier forKey:(__bridge id)kSecAttrAccount];
    [searchDictionary setObject:kServiceName forKey:(__bridge id)kSecAttrService];
    
    return searchDictionary;
}

+ (BOOL)createKeychainValue:(NSString *)password forIdentifier:(NSString *)identifier
{
    NSMutableDictionary *dictionary = [self newSearchDictionary:identifier];
    
    NSData *passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];
    [dictionary setObject:passwordData forKey:(__bridge id)kSecValueData];
    
    OSStatus status = SecItemAdd((__bridge CFDictionaryRef)dictionary, NULL);
    
    return status == errSecSuccess;
}

+ (NSData *)searchKeychainCopyMatching:(NSString *)identifier
{
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    
    // Add search attributes
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    
    // Add search return types
    [searchDictionary setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    CFDataRef result = nil;
    //OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary, (CFTypeRef *)&result);
    SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary, (CFTypeRef *)&result);
    
    return (__bridge NSData *)(result);
}

+ (void)deleteKeychainValue:(NSString *)identifier
{    
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    SecItemDelete((__bridge CFDictionaryRef)searchDictionary);
}

#pragma mark - PLists
+ (void)addDictionary:(NSDictionary*)dictionary toPListWithName:(NSString*)plistName
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",plistName]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path])
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath: path error:&error];
    }
    
    NSMutableDictionary *dataToSave = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    if (dictionary==nil) {
        [fileManager removeItemAtPath:path error:NULL];
    }
    else {
        
        [dataToSave addEntriesFromDictionary:dictionary];
        [dataToSave writeToFile:path atomically:YES];
    }
}

+ (NSDictionary*)getDictionaryFromPList:(NSString*)plistName
{
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",plistName]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path])
    {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:plistName ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath: path error:&error];
    }
    
    NSMutableDictionary *savedUserDetails = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    
    return savedUserDetails;
}

#pragma mark - CustomAlertView
+ (CustomAlertView*)showHUDWithTitle:(NSString *)text
{
    if (!text){
        text = @"Contacting Server...";
    }
    
    CustomAlertView *alertView = [[CustomAlertView alloc] initWithTitle:text message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    alertView.tag = 50;
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityIndicator.center = CGPointMake(139.5, 75.5);
    [activityIndicator startAnimating];
    
    [alertView addSubview:activityIndicator];
    [alertView show];
    
    return alertView;
}

+ (void)hideHUD:(CustomAlertView *)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

@end
