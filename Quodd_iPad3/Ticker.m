//
//  Ticker.m
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 3/20/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "Ticker.h"

@interface Ticker ()

@property (nonatomic) double stockPrice;
@property (nonatomic) double optionPrice;

@property (nonatomic, retain) ProjectHandler *sharedInstance;

@end

@implementation Ticker

- (void) loadWithTicker:(NSMutableDictionary *)tickerDict forSymbol:(NSString*)symbol
{
    _sharedInstance = [ProjectHandler sharedHandler];

    //Tick Logic change : if new last > existing last, tick up, < tick down, equal no tick; if chg >0 green, <0 red
    
    if (self.last) {
        CGFloat oldLast = self.last.floatValue;
        CGFloat newLast = [tickerDict[@"last"] floatValue];
        
        if (newLast > oldLast) {
            self.T = @"↑";
        }
        else if (newLast < oldLast) {
            self.T = @"↓";
        }
        
        //set L1, L2
        if (newLast != oldLast) {
            
            self.L2 = self.L1;
            self.L1 = self.last;
        }
    }
    else {
        self.T = @" ";
        
        self.L1 = @"";
        self.L2 = @"";
    }
    
    self.askExchg = (tickerDict[@"askExchg"] != nil) ? tickerDict[@"askExchg"] : self.askExchg;
    self.askSize = (tickerDict[@"askSize"] != nil) ? tickerDict[@"askSize"] : self.askSize;
    self.bid = (tickerDict[@"bid"] != nil) ? tickerDict[@"bid"] : self.bid;
    self.bidExchg = (tickerDict[@"bidExchg"] != nil) ? tickerDict[@"bidExchg"] : self.bidExchg;
    self.bidSize = (tickerDict[@"bidSize"] != nil) ? tickerDict[@"bidSize"] : self.bidSize;
    self.chg = (tickerDict[@"chg"] != nil) ? tickerDict[@"chg"] : self.chg;
    self.exchangeId = (tickerDict[@"exchangeId"]  != nil) ? tickerDict[@"exchangeId"] : self.exchangeId;
    self.exchg = (tickerDict[@"exchg"] != nil) ? tickerDict[@"exchg"] : self.exchg;
    self.high = (tickerDict[@"high"] != nil) ? tickerDict[@"high"] : self.high;
    self.isHalt = (tickerDict[@"isHalt"] != nil) ? tickerDict[@"isHalt"] : self.isHalt;
    self.isSHO = (tickerDict[@"isSHO"] != nil) ? tickerDict[@"isSHO"] : self.isSHO;
    self.last = (tickerDict[@"last"] != nil) ? tickerDict[@"last"] : self.last;
    self.lastTradeDate = (tickerDict[@"lastTradeDate"]  != nil) ? tickerDict[@"lastTradeDate"] : self.lastTradeDate;
    self.low = (tickerDict[@"low"] != nil) ? tickerDict[@"low"] : self.low;
    self.marketCenter = (tickerDict[@"marketCenter"] != nil) ? tickerDict[@"marketCenter"] : self.marketCenter;
    self.open = (tickerDict[@"open"] != nil) ? tickerDict[@"open"] : self.open;
    self.oRange = (tickerDict[@"oRange"] != nil) ? tickerDict[@"oRange"] : self.oRange;
    self.lastClosed = (tickerDict[@"lastClosed"] != nil) ? tickerDict[@"lastClosed"] : self.lastClosed;
    self.settlementPrice = (tickerDict[@"settlementPrice"] != nil) ? tickerDict[@"settlementPrice"] : self.settlementPrice;
    self.vol = (tickerDict[@"vol"]  != nil) ? tickerDict[@"vol"] : self.vol;
    self.vwap = (tickerDict[@"vwap"] != nil) ? tickerDict[@"vwap"] : self.vwap;
    self.perChg = (tickerDict[@"perChg"] != nil) ? tickerDict[@"perChg"] : self.perChg;
    self.ltv = (tickerDict[@"ltv"] != nil) ? tickerDict[@"ltv"] : self.ltv;
    self.ltt = (tickerDict[@"ltt"] != nil) ? tickerDict[@"ltt"] : self.ltt;
    self.ask = (tickerDict[@"ask"]  != nil) ? tickerDict[@"ask"] : self.ask;
    self.ticker = (tickerDict[@"ticker"] != nil) ? tickerDict[@"ticker"] : self.ticker;
    
    self.s = (tickerDict[@"s"] != nil) ? tickerDict[@"s"] : self.s;
    
//    self.tickerNameByUser = _strike ? [self optionTickerNameWithRootTicker:_rootTicker.tickerNameByUser] : symbol;
//    self.tickerNameByUser = [self displaySymbolForSymbol:symbol];
    
    self.subscription = [self tickerSubscription];

    self.ni = [[NSString alloc]initWithFormat:@"%@", (tickerDict[@"ni"] != nil) ? tickerDict[@"ni"] : self.ni];
    self.nd = (tickerDict[@"nd"] != nil) ? tickerDict[@"nd"] : self.nd;

    CGFloat changeVal = self.chg.floatValue;
    if (changeVal>0 && ![self.chg hasPrefix:@"+"]) {
        self.chg = [NSString stringWithFormat:@"+%@", self.chg];
        self.perChg = [NSString stringWithFormat:@"+%@", self.perChg];
    }
    
    if ([Volume_Tickers containsObject:self.ticker] && self.last) {
        self.last = [self millionFormatForValue:self.last];
    }
    
    if (self.ltt && self.lastTradeDate) {
        self.ltt = [self setLTT:self.ltt forDate:self.lastTradeDate];
    }

    //set ticker type
    NSInteger tickerLength = self.ticker.length;
    BOOL isMontage = (tickerLength > 3) && ([self.ticker characterAtIndex:tickerLength-2] == '/');
    
    BOOL isOptions = [self.ticker hasPrefix:@"O:"] && !isMontage;
    //BOOL isOptionsRegional = [self.ticker hasPrefix:@"O:"] && isMontage;
    BOOL isFutures = [self.ticker hasPrefix:@"/"];
    BOOL isBond = [self.ticker hasPrefix:@"B:"];
    
    if (isOptions) {
        self.tickerType = OPTIONS;
    }
    else if (isFutures) {
        self.tickerType = FUTURES;
    }
    else if (isBond) {
        self.tickerType = BONDS;
    }
    else self.tickerType = EQUITY;
    
    //harshit

    if(_strike)
    {
        _stockPrice = self.rootTicker.last.doubleValue;
        
        if (self.chg && self.chg.doubleValue !=0)
        {
            if (self.chg.doubleValue > 0)
                self.T = @"↑";
            else if (self.chg.doubleValue < 0)
                self.T = @"↓";
            else
                self.T = @" ";
        }
        else
            self.T = @" ";
        
        _cpp = _isPut ? @"P" : @"C";
        
        // Calculate _Days Value
        NSTimeZone *defaultTimerZone = [NSTimeZone defaultTimeZone];
        [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"EST"]];
        
        NSDate* now = [NSDate date];
        
        [NSTimeZone setDefaultTimeZone:defaultTimerZone];
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSUInteger days =  [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:_expDate] -
        [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:now];
        
        _days  = [[NSString alloc] initWithFormat:@"%i",(days + 1)]; // +1 for ongoing day
        
        // Calculate _time Value
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"yyyyMMdd"];
        
        if ([_lastTradeDate isEqualToString:@"00000000"])
        {
            _time = @"00/00/00";
        }
        else
        {
            NSDate *ltd = [df dateFromString:_lastTradeDate];
            
            if ([ltd isEqualToDate:[NSDate date]])
            {
                NSDate *temp = [NSDate dateWithTimeIntervalSince1970:(_ltt.doubleValue/1000)];
                [df setDateFormat:@"hh:mm:ss"];
                _time = [df stringFromDate:temp];
            }
            else
            {
                [df setDateFormat:@"MM/dd/yy"];
                _time = [df stringFromDate:ltd];
            }
        }
        
        double intrinsicValue;
        
        if (_isPut)
            intrinsicValue = (_strike.doubleValue - _stockPrice < 0) ? 0 : (_strike.doubleValue - _stockPrice);
        else
            intrinsicValue = (_stockPrice - _strike.doubleValue < 0) ? 0 : (_stockPrice - _strike.doubleValue);
        
        _intV  = [self formatDouble:intrinsicValue];
        
        _timeV  = [self formatDouble:_ask.doubleValue - intrinsicValue];
        
        double timeToExpiration = ([_expDate timeIntervalSinceDate:[NSDate date]]/86400000) ; 
        
        if (timeToExpiration <= 0)
            timeToExpiration = 1/(365*24);
        else
            timeToExpiration = timeToExpiration/365;
        
        if (!(_stockPrice == 0||
              _strike.doubleValue == 0 ||
              timeToExpiration == 0 ||
              _rate == 0 ||
              _volatility == 0))
        {
            double d, d1;
            
            d = (log(_stockPrice / [_strike doubleValue]) + (_rate + (_volatility * _volatility) / 2) * timeToExpiration) /
            (_volatility * sqrt(timeToExpiration));
            
            d1 = d - _volatility * sqrt(timeToExpiration);
            
            if(_isPut)
            {
                d = -d;
                d1 = -d1;
            }
            
            _optionPrice = _stockPrice * [self csnd:d] - [_strike doubleValue] *  exp(-_rate * timeToExpiration) * [self csnd:d1];
            
            if(_isPut)
                _optionPrice = -_optionPrice;
            
            _dif = [[NSString alloc] initWithFormat:@"%lf", _ask.doubleValue -_optionPrice];
            
            _delta = [self formatDouble:[self csnd:d]];
            _gamma = [self formatDouble:((exp( -(d*d)/2 ) / sqrt( 2* M_PI )) / (_stockPrice * _volatility * sqrt( timeToExpiration) ))];
            
            int phi = _isPut ? -1 : 1;
            
            double theta = ((-_stockPrice * [self snd:d] * _volatility) /
                            (2 * sqrt(timeToExpiration)) -
                            (double)phi * _rate * [_strike doubleValue] * exp(_rate * timeToExpiration) *
                            [self csnd:(double)phi * d1])/365;
            
            _theta = [self formatDouble:theta];
            
            _theoV  = [self formatDouble:_stockPrice * [self csnd:d] - _strike.doubleValue * exp(-_rate * timeToExpiration) * [self csnd:d1]];
            
            _timeV  = [self formatDouble:_ask.doubleValue - _theoV.doubleValue];
            
            _iVol  = [self formatDouble: [self iVolDouble]];
        }
        else
        {
            NSString *zString = @"0";
            
            _optionPrice = zString.doubleValue;
            _delta = [zString copy];
            _gamma = [zString copy];
            _theta = [zString copy];
            _theoV = [zString copy];
            _timeV = [zString copy];
            _iVol  = [zString copy];
        }
    }
}

- (id)valueForUndefinedKey:(NSString *)key
{
    return @"";
}

#pragma mark - Formatting Methods
- (NSString*)millionFormatForValue:(NSString*)floatString
{
    if ([floatString hasSuffix:@" M"]) {
        return floatString;
    }
    
    NSInteger num = floatString.integerValue;
    
    NSInteger millionInt = num/1000000;
    NSInteger millionDecimal = num%1000000;
    
    floatString = [NSString stringWithFormat:@"%d.%d", millionInt, millionDecimal];
    
    NSNumberFormatter *numberFormatter=[[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setCurrencySymbol:@""];    
    [numberFormatter setMaximumFractionDigits:2];
    
    NSNumber *formattedFloat = [NSNumber numberWithFloat: floatString.floatValue];
    
    NSString *milNum = [NSString stringWithFormat:@"%@ M",[numberFormatter stringFromNumber:formattedFloat]];
    
    return milNum;
}

- (NSString*)formatDouble:(double)doubleVal
{
    if (ABS(doubleVal) >= 1.0)
    {
        return [[NSString alloc] initWithFormat:@"%0.2lf", doubleVal];
    }
    
    return [[NSString alloc] initWithFormat:@"%0.4lf", doubleVal];
}

- (NSString*)setLTT:(NSString*)ltt forDate:(NSString*)lastTradeDate
{
    if ([ltt characterAtIndex:2] == ':') {
        return ltt;
    }
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyyMMdd"];
    [inputFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"America/New_York"]];
    
    NSDate *formatterDate = [inputFormatter dateFromString:lastTradeDate];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"MM/dd/yy"];
    [outputFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"America/New_York"]];
    
    NSString *newDateString = [outputFormatter stringFromDate:formatterDate];
    NSString *today = [outputFormatter stringFromDate:[NSDate date]];
    
    if ([newDateString isEqualToString:today])
    {
        NSInteger timeInt = ltt.integerValue;
        ltt = [NSString stringWithFormat:@"%02d:%02d:%02d", timeInt/10000, (timeInt/100)%100, timeInt%100];
    }
    else ltt = newDateString;
    
    return ltt;
}

- (Subscription)tickerSubscription
{
    Subscription sub = [self setSubscriptionEnum];

    @try {
        
        switch (sub) {
                
            case SUBSCRIBED:
                
                if (![_sharedInstance.subscribedSymbols containsObject:self.tickerNameByUser]) {
                    [_sharedInstance.subscribedSymbols addObject:self.tickerNameByUser];
                    [_sharedInstance.subscribedSymbols addObject:self.ticker];
                }
                
                [_sharedInstance.pendingSymbols removeObject:self.tickerNameByUser];
                [_sharedInstance.pendingSymbols removeObject:self.ticker];
                
                break;
                
            case UNSUBSCRIBED:
                
                if (![_sharedInstance.nonSubscribedSymbols containsObject:self.tickerNameByUser]) {
                    [_sharedInstance.nonSubscribedSymbols addObject:self.tickerNameByUser];
                    [_sharedInstance.nonSubscribedSymbols addObject:self.ticker];
                }
                
                [_sharedInstance.pendingSymbols removeObject:self.tickerNameByUser];
                [_sharedInstance.pendingSymbols removeObject:self.ticker];
                
                break;
                
            default:;
        }
        
    }
    @catch (NSException *exception) {
        //NSLog(@"EXCEPTION in %s (%@ - %@) - %@", __PRETTY_FUNCTION__, self.tickerNameByUser, self.ticker, exception.description);
    }

    return sub;
}

- (Subscription)setSubscriptionEnum
{
    if ([_sharedInstance.subscribedSymbols containsObject:self.tickerNameByUser])
    {
        return SUBSCRIBED;
    }
    else if ([_sharedInstance.nonSubscribedSymbols containsObject:self.tickerNameByUser])
    {
        return UNSUBSCRIBED;
    }
    else if ([_sharedInstance.pendingSymbols containsObject:self.tickerNameByUser])
    {
        NSString *exchange = self.exchangeId;
        NSString *symbol = self.ticker;
        
        if ([symbol hasPrefix:@"/"]) //FUTURES
        {
            return [self subscriptionForFuturesTicker];
        }
        else if ([symbol hasPrefix:@"O:"]) //OPTIONS
        {
            return [DICT_ENTITLEMENTS[@"optionEntitlementFlag"] boolValue] ? SUBSCRIBED : UNSUBSCRIBED;
        }
        else if ([Volume_Tickers containsObject:symbol]) //VOLUME TICKERS
        {
            return SUBSCRIBED;
        }
        else
        {
            if (exchange) //OTHERS
            {
                NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", @"[0-9]"];
                
                //check regionals
                if ([exchange isEqualToString:@"N/A"] && ![regexTest evaluateWithObject:symbol] && ([symbol hasSuffix:@"/N"] || [symbol hasSuffix:@"/P"] || [symbol hasSuffix:@"/A"]))
                {
                    return [self isEntitledForExchgs] ? SUBSCRIBED : UNSUBSCRIBED;
                }
                else
                {
                    NSDictionary *exchangeDict = DICT_ExchangeHash[exchange];
                    
                    if (exchangeDict)
                    {
                        BOOL isAuthorized = exchangeDict.count && [exchangeDict[kAuthorized] isEqualToString:kAuthorized];
                        return isAuthorized ? SUBSCRIBED : UNSUBSCRIBED;
                    }
                    else return SUBSCRIBED;
                }
            }
            else
            {
                return symbol ? UNSUBSCRIBED : PENDING;
            }
        }
    }
    else return PENDING;
}

- (BOOL)isEntitledForExchgs
{
    NSDictionary *exchangeHashDict = DICT_ExchangeHash;
    
    NSArray *exchgArr = @[@"07e", @"07n", @"11f", @"07f", @"07p", @"11g", @"07a", @"07j", @"11d"];
    
    for (int i=0; i<exchgArr.count; i++) {
        
        if ([exchangeHashDict[exchgArr[i]] count] && [exchangeHashDict[exchgArr[i]][kAuthorized] isEqualToString:kAuthorized]) {
            return YES;
        }
    }
    
    return NO;
}

- (Subscription)subscriptionForFuturesTicker
{
    if (self.subscription != SUBSCRIBED && self.subscription != UNSUBSCRIBED)
    {
        //get detailQ data for ticker
        NSString *detailUrl = [NSString stringWithFormat:DETAILED_QUOTES_URL, self.ticker];
        NSDictionary *detailDict = [NSDictionary dictionaryWithDictionary:[ProjectHandler dataFromAPIWithStringURL:detailUrl]];
        NSString *tickerExchg = detailDict[self.ticker][@"exchg"];
        
        BOOL exchgEntitled = [DICT_ExchangeHash[tickerExchg][kAuthorized] isEqualToString:kAuthorized];
        return exchgEntitled ? SUBSCRIBED : UNSUBSCRIBED;
    }
    else
        return self.subscription;
}

#pragma mark Option Display Mapping
- (NSString*)displaySymbolForSymbol:(NSString*)symbol
{
    OptionType symbolType = [self optionTypeOfSymbol:symbol];
    
    if ((symbolType == STANDARD || symbolType == ALTERNATE) && self.ticker)        //O:MSFT\13K16\21.0
    {
        NSString *primarySymbol = self.ticker;
        
        NSString *root = [primarySymbol componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":\\"]][1];
        NSString *middle = [primarySymbol componentsSeparatedByString:@"\\"][1];
        NSString *strike = [primarySymbol componentsSeparatedByString:@"\\"][2];
        
        const char *chars = middle.UTF8String;
        
        if (!IS_DISPLAY_TYPE_ALTERNATE)
        {
            //make standard MSFT 1316K21.0
            middle = [NSString stringWithFormat:@"%c%c%c%c%c", chars[0], chars[1], chars[3], chars[4], chars[2]];
        }
        else
        {
            //make alernate MSFT 131116C21.0
            int diff = chars[2] - 64;
            BOOL isPut = diff>12;
            
            if (isPut) diff-=12;
            
            NSString *monthChar = [NSString stringWithFormat:@"%02d",diff];
            
            middle = [NSString stringWithFormat:@"%c%c%@%c%c%@", chars[0], chars[1], monthChar, chars[3], chars[4], isPut?@"P":@"C"];
        }
        
        NSString *symbolToDisplay = [NSString stringWithFormat:@"%@ %@%@", root, middle, strike];
        
        return symbolToDisplay;
    }
    else
        return symbol;
}

- (OptionType)optionTypeOfSymbol:(NSString*)symbol
{
    //PRIMARY  O:MSFT\13K16\21.0
    //standard APV 1020B130 //GE 1321I15.0
    //alernate APV 100220C130
    
    NSString *primaryFormat = @"O:[\\S]+\\\\[0-9]{2}[A-Z]{1}[0-9]{2}\\\\[0-9]+\\.[0-9]*";
    NSString *standardFormat = @"[\\S]+[ ][0-9]{4}[A-Z]{1}[0-9]+[\\.]*[0-9]*";
    NSString *alternateFormat = @"[\\S]+[ ][0-9]{6}[C|P][0-9]+\\.[0-9]*";
    
    NSPredicate *primaryRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", primaryFormat];
    NSPredicate *standardRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", standardFormat];
    NSPredicate *alternateRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alternateFormat];
    
    if ([primaryRegex evaluateWithObject:symbol]) {
        return PRIMARY;
    }
    else if ([standardRegex evaluateWithObject:symbol]) {
        return STANDARD;
    }
    else if ([alternateRegex evaluateWithObject:symbol]) {
        return ALTERNATE;
    }
    else return INVALID;
}

#pragma mark - Options

- (id) initWithOptionArray: (NSArray *) optionArray
            parseKeyFormat:(NSArray *) parseKeyArray
{
    self = [super init];
    
    if (self)
    {
        // O:MSFT\\13G20\\16.0
        
        //Setting Ticker
        NSUInteger indexOfSymbol = [parseKeyArray indexOfObject:@"symbol"];
        _ticker = optionArray[indexOfSymbol];
        
        //Setting _strike
        NSArray *temp = [_ticker componentsSeparatedByString:@"\\"];
 
        _strike =  [[NSString alloc] initWithFormat:@"%0.2f",[temp.lastObject floatValue]];
        
        //Setting openInterest
        NSUInteger indexOfOI = [parseKeyArray indexOfObject:@"openInterest"];
        _oI = [[NSString alloc] initWithFormat:@"%@", optionArray[indexOfOI]];
        
        //Setting put/call status
        char ch = [temp[1] characterAtIndex:2]; //13G20 - G tells if it is call or put.
        
        if ((ch - 64) >12)
            _isPut = TRUE;
        else
            _isPut = FALSE;
        
        
        //Setting expiryDate 08/17/2013 07:59:00:000
        
        NSUInteger indexOfExpDate = [parseKeyArray indexOfObject:@"expDate"];
        NSString *strDate = optionArray[indexOfExpDate];
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM/dd/yyyy HH:mm:ss:SSS"];
        [df setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
        
        _expDate = [df dateFromString:strDate];
        
        _tickerNameByUser = [self optionTickerNameWithRootTicker:[temp[0] substringFromIndex:2]];
        
        NSDate *ym = [df dateFromString:strDate];
        [df setDateFormat:@"yyyy, MMM"];
        _yms = [[NSString alloc] initWithFormat:@"%@, %@", [df stringFromDate:ym], _strike ];
    }
    return  self;
}

- (NSString *) optionTickerNameWithRootTicker: (NSString *) ticker //returns ticker name as per user setting
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSString *strike = ([_strike rangeOfString:@".00"].location == NSNotFound) ? [[NSString alloc] initWithFormat:@"%0.1f",_strike.floatValue] : [_strike componentsSeparatedByString:@"."][0];

    if (strike.floatValue == strike.integerValue) {
        strike = [NSString stringWithFormat:@"%d.0", strike.integerValue];
    }
    
    NSString *tickerName;
    //if (kGetValueForBool(kOptionShowType))
    if (IS_DISPLAY_TYPE_ALTERNATE)
    {
        [df setDateFormat:@"yyMMdd"];
        
        tickerName =  [[NSString alloc] initWithFormat:@"%@ %@%@%@",
                       ticker,
                       [df stringFromDate:_expDate],
                       _isPut?@"P":@"C",
                       strike];
    }
    else
    {
        [df setDateFormat:@"yydd"];
        
        tickerName =  [[NSString alloc] initWithFormat:@"%@ %@%@%@",
                       ticker,
                       [df stringFromDate:_expDate],
                       [self charforMonth:_expDate],
                       strike];
    }

    return tickerName;
}

- (NSString *) charforMonth:(NSDate *) date
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSMonthCalendarUnit
                                                                   fromDate:date];
    NSInteger month = [components month];
    
    char ch = 64+month;

    if (_isPut)
        ch += 12;
    
    return  [NSString stringWithFormat:@"%c", ch];
}

- (NSString*) monthForChar: (char) ch
{
    int diff = ch - 64;
    
    if (diff >12)
    {
        _isPut = TRUE;
        diff-=12;
    }
    else
    {
        _isPut = FALSE;
    }
    
    return [NSString stringWithFormat:@"%02d",diff];
}

- (double) csnd:(double) x
{
    double a[] = { 0.31938153000000002, -0.356563782, 1.781477937, -1.8212559779999999, 1.3302744289999999 };
    double gamma = 0.23164190000000001;
    double k[5];
    
    if(x >= 0.0)
    {
        k[0] = 1.0 / (1.0 + gamma  * x);
        double s = a[0] * k[0];
        
        for(short i = 1; i < 5; i++)
        {
            k[i] = k[i - 1] * k[0];
            s += a[i] * k[i];
        }
        return 1.0 - [self snd:x] * s;
    }
    else
    {
        return 1.0 - [self csnd:-x];
    }
}

- (double) snd:(double) x
{
    return exp((-x * x) / 2) / sqrt(M_PI + M_PI);
}

- (double) iVolDouble //as it is from EQ+
{
    double sigma_low=0.0001;
    
    double timeToExpiration = ([_expDate timeIntervalSinceDate:[NSDate date]]*1000) ;
    
    double d, d1;
    
    d = (log(_stockPrice / [_strike doubleValue]) + (_rate + (sigma_low * sigma_low) / 2) * timeToExpiration) /
    (sigma_low * sqrt(timeToExpiration));
    
    d1 = d - sigma_low * sqrt(timeToExpiration);
    
    if(_isPut)
    {
        d = -d;
        d1 = -d1;
    }
    
    double price = _stockPrice * [self csnd:d] - [_strike doubleValue] *  exp(-_rate * timeToExpiration) * [self csnd:d1];
    
    if (price>_last.doubleValue)
        return 0.0;
    
    // simple binomial search for the implied volatility.
    // relies on the value of the option increasing in volatility
    
    double ACCURACY = 1.0e-5; // make this smaller for higher accuracy
    int MAX_ITERATIONS = 100;
    double HIGH_VALUE = 1e10;
    double ERROR = -1e40;
    
    // want to bracket sigma. first find a maximum sigma by finding a sigma
    // with an estimated price higher than the actual price.
    double sigma_high=0.15;
    
    do
    {
        sigma_high = 2.0 * sigma_high; // keep doubling.
        
        d = (log(_stockPrice / [_strike doubleValue]) + (_rate + (sigma_high * sigma_high) / 2) * timeToExpiration) /
        (sigma_high * sqrt(timeToExpiration));
        
        d1 = d - sigma_high * sqrt(timeToExpiration);
        
        if(_isPut)
        {
            d = -d;
            d1 = -d1;
        }
        
        price = _stockPrice * [self csnd:d] - [_strike doubleValue] *  exp(-_rate * timeToExpiration) * [self csnd:d1];
        
        if (sigma_high>HIGH_VALUE)
            return ERROR;
    } while (price < _last.doubleValue);
    
    for (int i=0; i<MAX_ITERATIONS; i++)
    {
        
        double sigma = (sigma_low+sigma_high)*0.5;
        
        d = (log(_stockPrice / [_strike doubleValue]) + (_rate + (sigma * sigma) / 2) * timeToExpiration) /
        (sigma * sqrt(timeToExpiration));
        
        d1 = d - sigma * sqrt(timeToExpiration);
        
        if(_isPut)
        {
            d = -d;
            d1 = -d1;
        }
        
        price = _stockPrice * [self csnd:d] - [_strike doubleValue] *  exp(-_rate * timeToExpiration) * [self csnd:d1];
        
        double test =  (price-_last.doubleValue);
        
        if (abs(test)<ACCURACY)
        {
            return sigma;
        }
        
        if (test < 0.0) {
            sigma_low = sigma;
        }
        else {
            sigma_high = sigma;
        }
    }
    
    return ERROR;
}

@end
