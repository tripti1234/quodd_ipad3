//
//  WallTable.h
//  Quodd_iPad
//
//  Created by Nishant on 29/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WallTable : NSObject

@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, strong) NSMutableArray *tickerSymbols;
@property (nonatomic, assign) NSUInteger selectedTickerIndex;

@end
