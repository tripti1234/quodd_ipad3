//
//  AlertsViewController.m
//  Quodd_iPad2
//
//  Created by Nishant on 01/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "AlertsViewController.h"
#import "AlertSummaryVC.h"
#import <QuartzCore/QuartzCore.h>

#define TABLE_ROW_HEIGHT 40
#define SELECTALERT_TABLE_TAG 13245
#define VOLUMES_TABLE_TAG 23124

#define COMP_ACTIVITY_CHECKBOX_TAG 5

#define kVolumeText         @"Volume  ⬇"
#define kSelectAlertText    @"Select Alert    ⬇"

@interface AlertsViewController ()

@property (nonatomic, strong) NSArray *volumes;
@property (nonatomic, strong) NSMutableArray *hours;
@property (nonatomic, strong) NSMutableArray *minutes;

@property (nonatomic, strong) NSArray *userAlerts;
@property (nonatomic, strong) UIPopoverController *popOverController;

@property (nonatomic, strong) NSMutableDictionary *parameters;

@property (nonatomic, strong) AlertSummaryVC *alertSummaryVC;

@property (nonatomic, strong) CustomAlertView *customAlert;

@end

@implementation AlertsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = VIEW_BACKGROUND_IMAGE;
    
    [Flurry logEvent:@"Alerts View"];

    _volumes = [[NSArray alloc] initWithObjects:
                @"Clear",
                @">1 Million",
                @">5 Million",
                @">10 Million",
                @">20 Million",
                @">2X AVG",
                @">3X AVG",
                @">5X AVG",
                nil];
    
    _hours = [[NSMutableArray alloc] initWithCapacity:12];
    _minutes = [[NSMutableArray alloc] initWithCapacity:60];

    for (int i=1; i<=12; i++) {
        [_hours addObject:[NSString stringWithFormat:@"%02d",i]];
    }
    
    for (int i=0; i<=59; i++) {
        [_minutes addObject:[NSString stringWithFormat:@"%02d",i]];
    }
    
    _parameters = [[NSMutableDictionary alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadAlertToEdit:)
                                                 name:kLoadAlertToEdit
                                               object:nil];
    
    for (UITextField *subview in self.view.subviews) {
        if ([subview isKindOfClass:[UITextField class]])
        {
            subview.layer.borderWidth = 1.0;
            subview.layer.borderColor = [[UIColor colorWithRed:19.0/255.0 green:28.0/255.0 blue:38.0/255.0 alpha:1.0] CGColor];
        }
    }
    
    [self clearAll];
    
    [self fetchUserAlerts];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (void)loadAlertToEdit:(NSNotification*)notif
{
    NSString *alertName = notif.userInfo[@"alertName"];
    [_selectAlert setTitle:alertName forState:UIControlStateNormal];
    
    for (NSDictionary *dict in _userAlerts)
    {
        if ([dict[@"name"] isEqualToString:alertName])
        {
            [self setUIForDict:dict];
            break;
        }
    }
}

- (void)fetchUserAlerts
{
    [_loadAlerts startAnimating];
    [_selectAlert setTitle:@"No Alerts Available" forState:UIControlStateNormal];
    
    _userAlerts = [[NSArray alloc] init];

    NSString *loadUserAlertsURL = [NSString stringWithFormat:LOAD_USER_ALERTS_URL, currentSessionID];
    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:loadUserAlertsURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             _userAlerts = [[NSArray alloc] initWithArray:JSON];
                                             
                                             if (_userAlerts.count) {
                                                 [_selectAlert setTitle:kSelectAlertText forState:UIControlStateNormal];
                                             }
                                             
                                             [_loadAlerts stopAnimating];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [_loadAlerts stopAnimating];
                                         }];
    [operation start];
    
}

#pragma mark - IBActions
- (IBAction)backPressed
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self clearAll];
    }];
}

- (IBAction)backgroundTouched
{
    if ([_popOverController isPopoverVisible]) {
        [_popOverController dismissPopoverAnimated:YES];
    }
    
    for (UITextField *subview in self.view.subviews) {
        [subview resignFirstResponder];
    }
}

- (IBAction)typeSelected:(UIButton*)sender
{
    sender.selected = !sender.selected;
    
    if (sender == _alertTypeWindow) {
        
        _alertTypeAudible.enabled = sender.selected;

        if (!sender.selected) {
            _alertTypeAudible.selected = NO;
        }
    }
}

- (IBAction)showAlertSummaryVC
{
    if (!_alertSummaryVC) {
        _alertSummaryVC = [[AlertSummaryVC alloc] initWithNibName:@"AlertSummaryVC" bundle:nil];
    }
    
    [self presentViewController:_alertSummaryVC animated:YES completion:nil];
}

- (IBAction)clearAll
{
    for (UIButton *button in self.view.subviews)
    {
        if (button.tag && [button respondsToSelector:@selector(setSelected:)])
        {
            button.selected = NO;
        }
    }
    
    _alertTypeAudible.enabled = NO;
    _charsLeft.text = @"100";
    [_selectTime setTitle:@"Select Time" forState:UIControlStateNormal];

    for (UITextField *textfield in self.view.subviews)
    {
        if ([textfield isKindOfClass:[UITextField class]] && [textfield respondsToSelector:@selector(setText:)])
        {
            textfield.text = @"";
        }
    }
    
    [_selectVolume setTitle:kVolumeText forState:UIControlStateNormal];

    if (_userAlerts.count) {
        [_selectAlert setTitle:kSelectAlertText forState:UIControlStateNormal];
    }
    else [_selectAlert setTitle:@"No Alerts Available" forState:UIControlStateNormal];
    
    _deleteAlertButton.enabled = NO;
}

- (IBAction)showPopOver:(UIButton*)senderButton
{
    NSInteger count = senderButton==_selectAlert ? _userAlerts.count : _volumes.count;
    
    if (count==0) {
        return; //no popover needed
    }
    
    [self backgroundTouched];
    
    //create a popover (table) here
    UITableViewController *alertsTable = alertsTable = [[UITableViewController alloc] init];
    alertsTable.tableView.dataSource = self;
    alertsTable.tableView.delegate = self;
    
    alertsTable.tableView.tag = senderButton==_selectAlert ? SELECTALERT_TABLE_TAG : VOLUMES_TABLE_TAG;
    
    [alertsTable.tableView reloadData];
    
    //set popover content size
    CGFloat maxHeight = TABLE_ROW_HEIGHT * 8.0;
    CGFloat desiredHeight = TABLE_ROW_HEIGHT * count;
    
    desiredHeight = (desiredHeight > maxHeight) ? maxHeight : desiredHeight;
    
    [alertsTable setContentSizeForViewInPopover:CGSizeMake(300, desiredHeight)];
    
    _popOverController = [[UIPopoverController alloc] initWithContentViewController:alertsTable];
    
    [_popOverController presentPopoverFromRect:senderButton.frame
                                        inView:self.view
                      permittedArrowDirections:UIPopoverArrowDirectionUp
                                      animated:YES];
}

- (IBAction)showTimePicker:(UIButton*)senderButton
{
    [self backgroundTouched];
    
    _popOverController = [[UIPopoverController alloc] initWithContentViewController:_timePicker];

    [_popOverController presentPopoverFromRect:senderButton.frame
                                        inView:self.view
                      permittedArrowDirections:UIPopoverArrowDirectionUp
                                      animated:YES];
}

- (IBAction)clearTime
{
    [_selectTime setTitle:@"Select Time" forState:UIControlStateNormal];
    [_popOverController dismissPopoverAnimated:YES];
}

- (IBAction)dismissTimePicker
{
    [_popOverController dismissPopoverAnimated:YES];
    
    [_selectTime setTitle:_timeLabel.text forState:UIControlStateNormal];
    
    _selectTime.titleLabel.textAlignment = NSTextAlignmentCenter;

    _tickerName.text = @".TIME";
}

- (IBAction)saveAlert
{
    NSString *title = @"";

    if (_tickerName.text.length == 0)
    {
        title = @"Please specify a ticker";
    }
    else if (_alertName.text.length == 0)
    {
        title = @"Please specify Alert Name";
    }
    else if (!_alertTypeWindow.selected && !_alertTypeEmail.selected)
    {
        title = @"Please select either Window or Email alert";
    }
    else if ([_comments.text rangeOfString:@"##"].location != NSNotFound)
    {
        title = @"## should not be part of comments";
    }
    else
    {
        BOOL flag = NO;
        
        for (UITextField *textField in self.view.subviews)
        {
            if ([textField isKindOfClass:[UITextField class]] && textField != _alertName && textField != _tickerName && textField != _comments && !flag) {
                
                flag = textField.text.length;
                if (flag) break;
            }
        }
        
        for (UIButton *button in self.view.subviews) {
            if ([button isKindOfClass:[UIButton class]] && button.tag==COMP_ACTIVITY_CHECKBOX_TAG && !flag) {
                
                flag = button.selected;
                if (flag) break;
            }
        }
        
        if (!flag) flag = [_selectVolume.titleLabel.text hasPrefix:@">"];
        if (!flag) flag = [_selectTime.titleLabel.text hasSuffix:@"M"];
        
        if (!flag) title = @"Please select at least one alert condition";
    }
    
    if (![_alertName.text isEqualToString:_selectAlert.titleLabel.text])
    {
        for (NSDictionary *alertData in _userAlerts) {
            if ([_alertName.text isEqualToString:alertData[@"name"]]) {
                title = @"This alert name already exists!";
            }
        }
    }

    if (title.length>0) {
        [[[UIAlertView alloc] initWithTitle:title message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }

    //form params string to add to url
    NSString *params = @"";

    for (UITextField *textField in self.view.subviews) {
        if ([textField isKindOfClass:[UITextField class]] && textField.text.length>0) {
            params = [params stringByAppendingFormat:@"&%@=%@", textField.placeholder, textField==_comments ? textField.text : textField.text.uppercaseString];
        }
    }
    
    for (UIButton *button in self.view.subviews) {
        if ([button isKindOfClass:[UIButton class]] && button.tag==COMP_ACTIVITY_CHECKBOX_TAG && button.selected) {
            params = [params stringByAppendingFormat:@"&%@=%d", button.titleLabel.text, button.selected];
        }
    }
    
    NSString *webFlag = @"";
    
    if (_alertTypeWindow.selected)      webFlag = [webFlag stringByAppendingString:@"W"];
    if (_alertTypeEmail.selected)       webFlag = [webFlag stringByAppendingString:@"E"];
    if (_alertTypeAudible.selected)     webFlag = [webFlag stringByAppendingString:@"A"];

    if (webFlag.length>0)
        params = [params stringByAppendingFormat:@"&webFlag=%@", webFlag];
    
    if ([_selectVolume.titleLabel.text hasPrefix:@">"])
        params = [params stringByAppendingFormat:@"&volume=%@", _selectVolume.titleLabel.text];
    
    if ([_selectTime.titleLabel.text hasSuffix:@"M"]) {
        NSString *timeSelected = _selectTime.titleLabel.text;
        timeSelected = [timeSelected stringByReplacingOccurrencesOfString:@" " withString:@","];
        params = [params stringByAppendingFormat:@"&alarmTime=%@", timeSelected];
    }

    
    _customAlert = [ProjectHandler showHUDWithTitle:@"Saving Alert.."];

    NSString *saveAlertURL = [NSString stringWithFormat:ALERT_SAVE_URL, currentSessionID, params];
    [self manageUserAlertsResponseForURL:saveAlertURL];
}

- (IBAction)deleteAlert
{
    if (_alertName.text.length>0) {
        
        _customAlert = [ProjectHandler showHUDWithTitle:@"Deleting Alert.."];

        NSString *deleteAlertURL = [NSString stringWithFormat:ALERT_DELETE_URL, currentSessionID, _alertName.text];
        [self manageUserAlertsResponseForURL:deleteAlertURL];
    }
}

- (void)manageUserAlertsResponseForURL:(NSString*)url
{    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSDictionary *status = [NSDictionary dictionaryWithDictionary:JSON];
                                             
                                             BOOL success = [status[@"status"] boolValue];
                                             
                                             NSString *title   = success ? @"Update Success!" : @"Update Failed!";
                                             NSString *message = success ? nil : @"Please try again.";
                                                                                          
                                             [[[UIAlertView alloc] initWithTitle:title
                                                                         message:message
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                             
                                             if (success) {
                                                 [self clearAll];
                                                 [self fetchUserAlerts];
                                             }
                                             
                                             [ProjectHandler hideHUD:_customAlert];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [ProjectHandler hideHUD:_customAlert];

                                             [[[UIAlertView alloc] initWithTitle:@"Operation Failed."
                                                                         message:error.localizedDescription.capitalizedString
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                         }];
    [operation start];
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (tableView.tag) {
        case SELECTALERT_TABLE_TAG: return _userAlerts.count;
            break;
            
        default: return _volumes.count;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TABLE_ROW_HEIGHT;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    switch (tableView.tag) {
        case SELECTALERT_TABLE_TAG:
            cell.textLabel.text = _userAlerts[indexPath.row][@"name"];
            break;
            
        case VOLUMES_TABLE_TAG:
            cell.textLabel.text = _volumes[indexPath.row];
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self backgroundTouched];
    
    switch (tableView.tag) {
        case SELECTALERT_TABLE_TAG:
        {
            [self setUIForDict:_userAlerts[indexPath.row]];
            break;
        }
        case VOLUMES_TABLE_TAG:

            if ([_volumes[indexPath.row] isEqualToString:@"Clear"]) {
                [_selectVolume setTitle:kVolumeText forState:UIControlStateNormal];
            }
            else
            {
                for (UITextField *subview in self.view.subviews) {
                    if ([subview isKindOfClass:[UITextField class]] && [subview.placeholder isEqualToString:@"volume"]) {
                        
                        [subview setText:@""];
                    }
                }
                
                [_selectVolume setTitle:_volumes[indexPath.row] forState:UIControlStateNormal];
            }

            break;
    }
}

- (void)setUIForDict:(NSDictionary*)dict
{    
    [self clearAll];
    
    for (UITextField *subview in self.view.subviews) {
        if ([subview isKindOfClass:[UITextField class]] && subview != _volume) {
            
            [subview setText:dict[subview.placeholder]];
        }
    }
    
    NSInteger charsLeft = 100 - _comments.text.length;
    _charsLeft.text = [NSString stringWithFormat:@"%d", charsLeft];

    
    _alertTypeWindow.selected = [dict[@"webFlag"] rangeOfString:@"W"].location != NSNotFound;
    _alertTypeAudible.selected = [dict[@"webFlag"] rangeOfString:@"A"].location != NSNotFound;
    _alertTypeEmail.selected = [dict[@"webFlag"] rangeOfString:@"E"].location != NSNotFound;
    
    _alertTypeAudible.enabled = _alertTypeWindow.selected;
    
    for (UIButton *button in self.view.subviews)
    {
        if (button.tag==COMP_ACTIVITY_CHECKBOX_TAG)
        {
            button.selected = [dict[button.titleLabel.text] boolValue];
        }
    }
    
    [_selectAlert setTitle:dict[@"name"] forState:UIControlStateNormal];
    
    if (dict[@"alarmTime"]) {
        NSString *alarmTime = [dict[@"alarmTime"] stringByReplacingOccurrencesOfString:@"," withString:@" "];
        [_selectTime setTitle:alarmTime forState:UIControlStateNormal];
    }
    else [_selectTime setTitle:@"Select Time" forState:UIControlStateNormal];
    
    if (dict[@"volume"] && [dict[@"volume"] hasPrefix:@">"]) {
        [_selectVolume setTitle:dict[@"volume"] forState:UIControlStateNormal];
        _volume.text = @"";
    }
    else _volume.text = dict[@"volume"];
    
    _deleteAlertButton.enabled = YES;
}

#pragma mark - UIPickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    switch (component)
    {
        case 0: return _hours.count; break;
            
        case 1: return _minutes.count; break;
            
        default: return 2; break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    switch (component)
    {
        case 0: return _hours[row]; break;
            
        case 1: return _minutes[row]; break;
            
        default: return row==0 ? @"AM" : @"PM";
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    static NSString *hour = @"01";
    static NSString *minute = @"00";
    static NSString *AMPM = @"AM";

    switch (component) {
        case 0: hour = _hours[row]; break;
            
        case 1: minute = _minutes[row]; break;

        default: AMPM = row==0 ? @"AM" : @"PM";
    }
    
    _timeLabel.text = [NSString stringWithFormat:@"%@:%@ %@", hour, minute, AMPM];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField // called when 'return' key pressed. return NO to ignore.
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //implement REGEX check instead
    
    if (string.length==0) return YES;

    if (textField == _tickerName)
    {
        return ![string isEqualToString:@" "];
    }
    else if (textField == _alertName)
    {
        NSCharacterSet *notAllowedChar = [NSCharacterSet characterSetWithCharactersInString:@" (;/\\#!)"];

        BOOL isAllowedChar = [string rangeOfCharacterFromSet:notAllowedChar].location == NSNotFound;
        BOOL isWithinSizeLimit = textField.text.length <= 20;
        
        return isAllowedChar && isWithinSizeLimit;
        
    }
    else if (textField == _comments)
    {
        return textField.text.length<100;
    }
    else if (textField == _volume) {
        [_selectVolume setTitle:kVolumeText forState:UIControlStateNormal];
        
        if ([string isEqualToString:@"."]) {
            BOOL doesNotHaveDot = [textField.text rangeOfString:@"."].location == NSNotFound;
            return doesNotHaveDot;
        }

        BOOL isWithinLimit = textField.text.length<13;
        NSCharacterSet *allowedChar = [NSCharacterSet characterSetWithCharactersInString:@"1234567890."];
        BOOL isAllowedChar = [string rangeOfCharacterFromSet:allowedChar].location != NSNotFound;

        return isWithinLimit && isAllowedChar;
    }
    else
    {                
        if ([string isEqualToString:@"."]) {
            BOOL doesNotHaveDot = [textField.text rangeOfString:@"."].location == NSNotFound;
            return doesNotHaveDot;
        }
        
        NSCharacterSet *allowedChar = [NSCharacterSet characterSetWithCharactersInString:@"1234567890."];
        BOOL isAllowedChar = [string rangeOfCharacterFromSet:allowedChar].location != NSNotFound;
        
        BOOL isWithinLimit = textField.text.length<10;
        
        return isAllowedChar && isWithinLimit;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _comments)
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            [self shiftViewTo:20.0 animated:YES];
        }];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@"."]) {
        textField.text = @"";
    }
    else if (textField == _comments)
    {
        [UIView animateWithDuration:0.5 animations:^{
            
            [self shiftViewTo:-20.0 animated:YES];
        }];
    }
}

- (void)shiftViewTo:(CGFloat)yCoordinate animated:(BOOL)animated
{    
    [UIView animateWithDuration:animated ? 0.2 : 0.0
                     animations:^{
                         
                         if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
                             [self.view setFrame:CGRectMake(-yCoordinate, 0, self.view.frame.size.width, self.view.frame.size.height)];
                         }
                         else
                             [self.view setFrame:CGRectMake(yCoordinate, 0, self.view.frame.size.width, self.view.frame.size.height)];
                     }];
}

- (IBAction)textDidChange:(UITextField *)textField
{
    if (textField == _comments)
    {
        NSInteger charsLeft = 100 - textField.text.length;
        _charsLeft.text = [NSString stringWithFormat:@"%d", charsLeft];
    }
}

@end
