//
//  AlertSummaryVC.h
//  Quodd_iPad2
//
//  Created by Nishant on 12/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertSummaryCell.h"

@interface AlertSummaryVC : UIViewController<UITableViewDataSource, UITableViewDelegate, AlertSummaryCellDelegate>

@property (nonatomic, retain) IBOutlet UISegmentedControl *summarySegment;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *summaryLoading;
@property (nonatomic, retain) IBOutlet UILabel *summaryTypeLabel;
@property (nonatomic, retain) IBOutlet UITableView *summaryTable;

@property (nonatomic, retain) IBOutlet UIButton *first;
@property (nonatomic, retain) IBOutlet UIButton *previous;
@property (nonatomic, retain) IBOutlet UIButton *next;
@property (nonatomic, retain) IBOutlet UIButton *last;

- (IBAction)backPressed;
- (IBAction)alertSummaryTypeChanged:(UISegmentedControl*)senderSegment;


- (IBAction)selectAllAlerts;
- (IBAction)clearSelectedAlerts;
- (IBAction)deleteSelectedAlerts;

- (IBAction)getMoreHistorical:(UIButton*)senderButton;

@end
