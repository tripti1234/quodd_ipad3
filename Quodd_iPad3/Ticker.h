//
//  Ticker.h
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 3/20/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    EQUITY,
    OPTIONS,
    FUTURES,
    BONDS,
} TickerType;

typedef enum {
    PENDING,
    SUBSCRIBED,
    UNSUBSCRIBED
} Subscription;

typedef enum {
    INVALID,
    PRIMARY,
    STANDARD,
    ALTERNATE
} OptionType;


@interface Ticker : NSObject

@property (nonatomic, assign) TickerType tickerType;
@property (nonatomic, retain) id detailQuote;

@property (nonatomic, retain) NSString * ask;
@property (nonatomic, retain) NSString * askExchg;
@property (nonatomic, retain) NSString * askSize;
@property (nonatomic, retain) NSString * bid;
@property (nonatomic, retain) NSString * bidExchg;
@property (nonatomic, retain) NSString * bidSize;
@property (nonatomic, retain) NSString * chg;
@property (nonatomic, retain) NSString * exchangeId;
@property (nonatomic, retain) NSString * exchg;
@property (nonatomic, retain) NSString * high;
@property (nonatomic, retain) NSString * isHalt;
@property (nonatomic, retain) NSString * isSHO;
@property (nonatomic, retain) NSString * last;
//@property (nonatomic, retain) NSString * lastClosePriceRange1;
//@property (nonatomic, retain) NSString * lastClosePriceRange2;
@property (nonatomic, retain) NSString * lastTradeDate;
@property (nonatomic, retain) NSString * low;
@property (nonatomic, retain) NSString * ltt;
@property (nonatomic, retain) NSString * ltv;
@property (nonatomic, retain) NSString * marketCenter;
@property (nonatomic, retain) NSString * open;
@property (nonatomic, retain) NSString * oRange;
//@property (nonatomic, retain) NSString * openPriceRange2;
@property (nonatomic, retain) NSString * perChg;
@property (nonatomic, retain) NSString * lastClosed;
@property (nonatomic, retain) NSString * settlementPrice;
@property (nonatomic, retain) NSString * ticker;
@property (nonatomic, retain) NSString * vol;
@property (nonatomic, retain) NSString * vwap;
@property (nonatomic, retain) NSString * s;
@property (nonatomic, retain) NSString * T;


@property (nonatomic, retain) NSString * tickerNameByUser;
@property (nonatomic, retain) NSString * ni;
@property (nonatomic, retain) NSString * nd;
@property (nonatomic, assign) Subscription subscription;

@property (nonatomic, retain) NSString * L1;
@property (nonatomic, retain) NSString * L2;


- (void) loadWithTicker:(NSDictionary *) ticker forSymbol:(NSString*)symbol;

//Option By Harshit
@property (strong, nonatomic) NSString * strike;
@property (strong, nonatomic) NSDate * expDate;

@property (nonatomic) BOOL isPut;

@property (strong, nonatomic) NSString * yms;
@property (strong, nonatomic) NSString * time;
@property (strong, nonatomic) NSString * oI;
@property (strong, nonatomic) NSString * delta;
@property (strong, nonatomic) NSString * gamma;
@property (strong, nonatomic) NSString * theta;
@property (strong, nonatomic) NSString * intV;
@property (strong, nonatomic) NSString * timeV;
@property (strong, nonatomic) NSString * theoV;
@property (strong, nonatomic) NSString * dif;
@property (strong, nonatomic) NSString * iVol;
@property (strong, nonatomic) NSString * exp;
@property (strong, nonatomic) NSString * cpp;

@property (strong, nonatomic) NSString * days;

@property (nonatomic) double volatility;
@property (nonatomic) double rate;


@property (strong, nonatomic) Ticker *rootTicker;


- (id) initWithOptionArray: (NSArray *) optionArray
            parseKeyFormat: (NSArray *) parseKeyArray;


@end
