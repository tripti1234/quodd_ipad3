//
//  NewsDetailWebViewController.h
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 3/21/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsHeadlineView.h"

@interface NewsDetailWebViewController : UIViewController<UIWebViewDelegate, UITableViewDataSource, UITableViewDelegate, NewsHeadlineViewDelegate>

@property(nonatomic) BOOL dowJonesFlag;
@property(nonatomic, retain) NSArray *newsList;
@property(nonatomic, retain) IBOutlet UITableView *newsTable;
@property(nonatomic, retain) NSMutableArray *isNewsRead;
@property (nonatomic, assign) NSInteger openSectionIndex;

- (IBAction)backPressed;

@end
