//
//  LoginViewController.h
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 2/22/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"

@interface LoginViewController : UIViewController
{
    IBOutlet UITextField *usernameField;
    IBOutlet UITextField *passwordField;
    IBOutlet UIButton *rememberMeButton;
   
    IBOutlet UIButton *loginButton;
    
    BOOL ifRemember;
    
    NSMutableDictionary *loginResponse;
}

@property (nonatomic, strong) MasterViewController *masterVC;


- (IBAction)backgroundTouched;
- (IBAction)rememberMeCheck;
- (IBAction)loginButtonPressed;

- (IBAction)forgotPassword;

@end
