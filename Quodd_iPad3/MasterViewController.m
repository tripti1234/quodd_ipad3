//
//  MasterViewController.m
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 2/20/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "MasterViewController.h"
#import "TickerTableViewCell.h"
#import "Ticker.h"
#import <QuartzCore/QuartzCore.h>

#import "MarketMonitor.h"
#import "GreatWall.h"
#import "WallTable.h"

#import "DetailQuoteView.h"
#import "ModifyColumnsVC.h"

#import "NALabelAnimation.h"
#import "NANotification.h"

#import "AlertsViewController.h"
#import "ChartsViewController.h"
#import <AudioToolbox/AudioToolbox.h>

#import "SingleNewsVC.h"
#import "SettingsViewController.h"

#define KEYBOARD_HEIGHT         180
#define TICKER_TABLE_ROW_HEIGHT 39

#define TICKER_TABLE_WIDTH 754.0

//random tags
#define NEWS_TABLE_TAG                  9992
#define POPOVER_TABLE_TAG               8399
#define LOGOUT_ALERTVIEW_TAG            7148
#define RENAME_GREATWALL_ALERTVIEW_TAG  6387
#define REMOVE_GW_ALERTVIEW_TAG         5344
#define REMOVE_ST_ALERTVIEW_TAG         4248
#define SYNC_ALERTVIEW_TAG              6877

#define MARKETMONITOR_MENU_TAG          7987

#define TICKER_LOOKUP_TABLE_TAG         3432
#define TICKER_COUNT    @"6"
#define TICKER_TYPE     @"A"
#define TICKER_ORDERBY  @"Symbol"


@interface MasterViewController ()
@property (strong, nonatomic) NSMutableArray *array;


@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (strong, nonatomic) IBOutlet UIPageControl *greatWallPageControl;

@property (strong, nonatomic) IBOutlet UISegmentedControl *greatWallSegment;

@property (strong, nonatomic) IBOutlet UIView *marketMonitorMenu;
@property (strong, nonatomic) IBOutlet UITableView *marketMonitorsTable;

@property (nonatomic, strong) IBOutlet UIScrollView *scrollDetailQuotes;

@property (nonatomic, strong) NSMutableDictionary *marketMonitorResponseComplete;
@property (nonatomic, strong) NSMutableDictionary *columnLayoutDict;
@property (nonatomic, strong) NSArray *defaultColumns;
@property (nonatomic, strong) NSArray *optionsColumns;

@property (nonatomic, strong) NSMutableArray *currentLayoutTickers;

@property (nonatomic, assign) NSUInteger currentMMIndex;
@property (nonatomic, assign) NSUInteger currentGWIndex;

@property (nonatomic, strong) NSMutableArray *tickerLookupResponse;

@property (nonatomic)        BOOL dowJonesNewsFlag;

@property (nonatomic, strong) NSMutableArray *marketMonitors;

@property (nonatomic, strong) NSMutableDictionary *selectedGWIndices;
@property (nonatomic, strong) NSMutableDictionary *visibleSubTable;

@property (nonatomic, strong) DetailQuoteView *detailQuoteView;

@property (nonatomic, strong) NSTimer *timerSessionCheck;

@property (nonatomic, strong) NSTimer *timerAlerts;
@property (strong) AVAudioPlayer *audioPlayer;

@property (nonatomic, strong) NSMutableArray *readNewsIDs;

@property (nonatomic, retain) ProjectHandler *sharedInstance;

@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _array = [[NSMutableArray alloc] init];
        _currentMMIndex = 0;
        _currentGWIndex = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = VIEW_BACKGROUND_IMAGE;

    _readNewsIDs = [[NSMutableArray alloc] init];

    NSDictionary *entitlements = DICT_ENTITLEMENTS;
    _dowJonesNewsFlag = [entitlements[@"dowJonesNewsFlag"] boolValue];
    
    
    //set global parameters
    isDowJonesEntitled = [entitlements[@"dowJonesNewsFlag"] boolValue];
    isNASDAQBasicEntitled = [entitlements[@"exchangeHash"][@"tsnasdaqbasic_access"] count] || [entitlements[@"exchangeHash"][@"tsqnb_access"] count];
    
    
    quotesMenuItems = [[NSMutableArray alloc] initWithObjects:@"Great Wall (All fields)", @"Major Market Indices", @"Major Market Indicators", nil];
    
    if (_dowJonesNewsFlag) {
        newsMenuItems = [[NSMutableArray alloc] initWithObjects:
                         @"DJ Breaking News",
                         @"DJ News Search, Queries & Codes",
                         @"DJ News Plus",
                         nil];
    }
    else newsMenuItems = [[NSMutableArray alloc] initWithObjects:
                          @"Breaking News", nil];

    
    companyInfoMenuItems = [[NSMutableArray alloc] initWithObjects:
                            @"Company Snapshot",
                            @"Company News",
                            @"MarketGrader Equity Research",
                            @"PIR Corporate Profile",
                            @"Analyst Coverage",
                            @"Earnings",
                            @"Key Stats & Financials",
                            @"Insider Activity",
                            @"Institutional Ownership",
                            @"SEC Filings",
                            @"Financial Highlights",
                            @"Balance Sheet (Annual)",
                            @"Income Statement (Annual)",
                            @"Cash Flow Statement (Annual)",
                            @"Income Statement (Quarterly)",
                            @"Balance Sheet (Quarterly)", nil];

    if (!isDowJonesEntitled) {
        [companyInfoMenuItems removeObject:@"PIR Corporate Profile"];
    }
    
    if ([entitlements[@"exchangeHash"][@"OResearch_access"] count] == 0) {
        [companyInfoMenuItems removeObject:@"MarketGrader Equity Research"];
    }
    
    miscMenuItems = [[NSMutableArray alloc] initWithObjects:
                     @"Treasury Page",
                     @"Splits & Buybacks",
                     @"Movers & Shakers",
                     @"Historical Prices",
                     @"Stock Screener",
                     @"Mutual Fund Screener",
                     @"Earnings Calendar",
                     @"Economic Calendar Plus", nil];
    
    if ([entitlements[@"exchangeHash"][@"earningscal_access"] count] == 0) {
        [miscMenuItems removeObject:@"Earnings Calendar"];
    }
    
    if ([entitlements[@"exchangeHash"][@"Econo_access"] count] == 0) {
        [miscMenuItems removeObject:@"Economic Calendar Plus"];
    }
    
    allMenuItems = [[NSDictionary alloc] initWithObjectsAndKeys:
                    quotesMenuItems, @"Quotes",
                    newsMenuItems, @"News",
                    companyInfoMenuItems, @"Company Info",
                    miscMenuItems, @"Misc.", nil];
    
    
    selectorForString = [[NSDictionary alloc] initWithObjectsAndKeys:
                         @"greatWallActionSheet", @"Great Wall (All fields)",
                         @"showMajorIndicatorsView", @"Major Market Indicators",
                         @"showMajorIndicesView", @"Major Market Indices",
                         @"showBreakingNews", @"Breaking News",
                         @"showBreakingNews", @"DJ Breaking News",
                         
                         @"upcoming", @"DJ News Search, Queries & Codes",
                         @"upcoming", @"DJ News Plus",
                         nil];
    
    lastTickerSelected = @"GOOG";
    
    newsTableView.layer.cornerRadius = 10;
    newsTableView.tag = NEWS_TABLE_TAG;
    
    _sharedInstance = [ProjectHandler sharedHandler];

    [_greatWallSegment setBackgroundImage:[UIImage imageNamed:@"segmentBg.png"]  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_greatWallSegment setBackgroundImage:[UIImage imageNamed:@"activeBg.png"]  forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    //get market monitors at launch
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];

    
    NSString *marketMonitorURL = [NSString stringWithFormat:LAYOUT_GET_MONITORS_URL,currentUserID];
    [self performSelectorOnMainThread:@selector(getMarketMonitorResponseFromURL:)
                           withObject:marketMonitorURL
                        waitUntilDone:YES];
    
    
    _timerAlerts = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                    target:self
                                                  selector:@selector(getUserAlerts)
                                                  userInfo:nil
                                                   repeats:YES];
    
    _timerSessionCheck = [NSTimer scheduledTimerWithTimeInterval:15*60.0
                                                    target:self
                                                  selector:@selector(checkValidSession)
                                                  userInfo:nil
                                                   repeats:YES];
    
    NSURL *audibleAlertURL = [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                                     pathForResource:@"audibleAlert"
                                                     ofType:@"aiff"]];
    
    NSError *error;
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:audibleAlertURL error:&error];
    
    if (!error) {
        [_audioPlayer prepareToPlay];
    }
    //else //NSLog(@"_audioPlayer %@", error.localizedDescription);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [self performSelectorInBackground:@selector(getComstockNewsList) withObject:nil];
    timerComstockNewsList = [NSTimer scheduledTimerWithTimeInterval:60.0
                                                             target:self
                                                           selector:@selector(getComstockNewsList)
                                                           userInfo:nil
                                                            repeats:YES];

    [self updateTimer];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kReloadDataInTable
                                                  object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [timerComstockNewsList invalidate];
    [timerSnapQuote invalidate];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark
- (void)updateTimer
{
    NSString *newsSource = _dowJonesNewsFlag ? @"DOWJONES" : @"COMTEX";
    
    [timerSnapQuote invalidate];
    timerSnapQuote = [NSTimer scheduledTimerWithTimeInterval:20.0
                                                      target:[self visibleTable]
                                                    selector:@selector(getSnapAPI:)
                                                    userInfo:@{kNewsSource: newsSource}
                                                     repeats:YES];
    [timerSnapQuote fire];
}

- (void)configureGreatWalls
{
    //remove all segments
    [_greatWallSegment removeAllSegments];
    
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
   
    if (!_visibleSubTable) {
        _visibleSubTable = [[NSMutableDictionary alloc] init];
    }
    
    if (mm.greatWalls.count>0)
    {
        //add new segments
        for (int i=0; i<mm.greatWalls.count; i++)
        {
            GreatWall *gw = mm.greatWalls[i];
            
            [_greatWallSegment insertSegmentWithTitle:gw.name atIndex:i animated:YES];
                        
            [_greatWallSegment setTitleTextAttributes:
             [NSDictionary dictionaryWithObjectsAndKeys:
              [UIFont fontWithName:@"Helvetica Neue" size:14], UITextAttributeFont,
              [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0], UITextAttributeTextColor,
              nil]
                                             forState:UIControlStateNormal];
            
            if (!_visibleSubTable[gw.index]) {
                [_visibleSubTable setValue:@"0" forKey:gw.index];
            }
        }
        
        //show previously select
        _currentGWIndex = [_selectedGWIndices[mm.index] integerValue];
        [_greatWallSegment setSelectedSegmentIndex:_currentGWIndex];
        [self greatWallSelected:_greatWallSegment];
    }
    
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

- (void)configureSubTablesForGreatWall
{
    for (UIView *subview in greatWallScroll.subviews)
        [subview removeFromSuperview];
    
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    
    _greatWallPageControl.numberOfPages = gw.tickerLists.count;
    _greatWallPageControl.currentPage = 0;

    [greatWallScroll setContentSize:CGSizeMake(greatWallScroll.frame.size.width*gw.tickerLists.count, greatWallScroll.frame.size.height)];
    
    CGFloat width = tickerTableView.frame.size.width;
    CGFloat height = tickerTableView.frame.size.height;
    
    for (int i=0; i<gw.tickerLists.count; i++) {

        TickerTableView *newTable;
        
        if (newTable == nil)
        {
            NSArray *topObjects = [[NSBundle mainBundle] loadNibNamed:@"TickerTableView" owner:self options:nil];
            for (id currentObject in topObjects)
            {
                if ([currentObject isKindOfClass:[TickerTableView class]])
                {
                    newTable = (TickerTableView *)currentObject;
                    break;
                }
            }
        }
        
        [newTable setFrame:CGRectMake(width*i, 0, width, height)];
        newTable.delegate = self;

        newTable.readNewsArray = _readNewsIDs;
        newTable.wallTable = gw.tickerLists[i];
        [newTable configureWallTableWithColumns:gw.columns];
        [greatWallScroll addSubview:newTable];
    }
    
    //scroll to desired table
    _greatWallPageControl.currentPage = [_visibleSubTable[gw.index] integerValue];
    [self scrollToTableIndex:_greatWallPageControl.currentPage];
}

- (void)addNewGreatWall
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];

    if (mm.greatWalls.count==8)
    {
        [[[UIAlertView alloc] initWithTitle:@"Cannot add great wall" message:@"A market monitor can have only 8 great walls." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }

    //add new great wall empty data
    NSInteger largestID = 1000 + 100*mm.index.integerValue;
    for (GreatWall *gw in mm.greatWalls)
    {
        if (gw.index.integerValue > largestID) {
            largestID = gw.index.integerValue;
        }
    }

    NSString *newGWID = [NSString stringWithFormat:@"%d",largestID+1];
    NSString *newGWName = [NSString stringWithFormat:@"GreatWall-%d",(largestID+1)%100];
    
    NSString *addGreatWallURL = [NSString stringWithFormat:LAYOUT_ADD_GREATWALL_URL, currentUserID, newGWName, mm.index, newGWID];

    if ([self isLayoutUpdateSuccessForURL:addGreatWallURL])
    {
        GreatWall *newGW = [[GreatWall alloc] init];
        newGW.index = newGWID;
        newGW.name = newGWName;
        newGW.tickerLists = [[NSMutableArray alloc] init];        
        newGW.columns = [[NSMutableArray alloc] initWithArray:_defaultColumns];
        
        WallTable *newTable = [[WallTable alloc] init];
        newTable.index = 0;     //no significance
        newTable.tickerSymbols = [[NSMutableArray alloc] init];
        newTable.selectedTickerIndex = 0;
        [newGW.tickerLists addObject:newTable];
        
        NSMutableArray *temp = mm.greatWalls.mutableCopy;
        [temp addObject:newGW];
        mm.greatWalls = temp;
        
        //add new great wall segment
        [_greatWallSegment insertSegmentWithTitle:newGW.name
                                          atIndex:_greatWallSegment.numberOfSegments
                                         animated:YES];     //index increased here
        
        [_greatWallSegment setSelectedSegmentIndex:_greatWallSegment.numberOfSegments-1];
        [self greatWallSelected:_greatWallSegment];
        
        //show notification
        NANotification *notif = [[NANotification alloc] initWithText:@"New Great Wall Added."];
        [self.view addSubview:notif];
    }
}

- (void)removeSubtable
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];

    NSInteger deleteIndex = _greatWallPageControl.currentPage;
    
    NSString *removeSubtableURL = [[NSString alloc] initWithFormat:LAYOUT_DELETE_SUBTABLE_URL, currentUserID, deleteIndex, mm.index, gw.index];

    if ([self isLayoutUpdateSuccessForURL:removeSubtableURL])
    {
        //Update UI
        NSMutableArray *temp = gw.tickerLists.mutableCopy;
        [temp removeObjectAtIndex:deleteIndex];
        gw.tickerLists = temp;
        
        
        if (deleteIndex == _greatWallPageControl.numberOfPages-1) {         //if last table
            _greatWallPageControl.currentPage = deleteIndex-1;
            [self scrollToTableIndex:deleteIndex];
        }
        
        _greatWallPageControl.numberOfPages = gw.tickerLists.count;
        
        CGFloat x = greatWallScroll.contentOffset.x;
        
        for (UIView *subview in greatWallScroll.subviews) {
            
            if ([subview isKindOfClass:[TickerTableView class]] && subview.frame.origin.x > x)
            {
                [UIView animateWithDuration:0.5
                                 animations:^{
                                     
                    subview.frame = CGRectMake(subview.frame.origin.x - greatWallScroll.frame.size.width, subview.frame.origin.y, subview.frame.size.width, subview.frame.size.height);
                }];
            }
            else if (subview.frame.origin.x == x)
                [subview removeFromSuperview];
            
        }
        
        [greatWallScroll setContentSize:CGSizeMake(greatWallScroll.frame.size.width*gw.tickerLists.count,greatWallScroll.frame.size.height)];
        
        //show notification
        NANotification *notif = [[NANotification alloc] initWithText:@"Table removed."];
        [self.view addSubview:notif];
    }
}

- (void)getUserAlerts
{
    NSString *alertPollingURL = [NSString stringWithFormat:ALERTS_POLLING_URL, currentUserID];

    NSURLRequest *request = [ProjectHandler urlRequestWithURL:alertPollingURL];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             
                                             NSArray *fetchedAlerts = [[NSArray alloc] initWithArray:JSON];

                                             for (NSDictionary *dict in fetchedAlerts)
                                             {
                                                 if ([dict[@"webFlag"] rangeOfString:@"W"].location != NSNotFound)
                                                 {
                                                     [[[UIAlertView alloc] initWithTitle:dict[@"message"]
                                                                                 message:dict[@"comment"]
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil] show];
                                                 }
                                                 
                                                 if ([dict[@"webFlag"] rangeOfString:@"A"].location != NSNotFound)
                                                 {
                                                     [_audioPlayer play];
                                                 }
                                             }
                                             
                                             fetchedAlerts = nil;
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Unable to retrieve alerts. %@", error.localizedDescription);
                                         }];
    [operation start];
    
}

- (void)checkValidSession
{
    NSString *sessionCheckURL = [NSString stringWithFormat:SESSION_CHECK_URL, currentUserID, currentSessionID];
    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:sessionCheckURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             //{"loginStatus":"SUCCESS"}
                                             
                                             NSDictionary *sessionResponse = [NSDictionary dictionaryWithDictionary:JSON];
                                             
                                             if (![sessionResponse[@"loginStatus"] isEqualToString:@"SUCCESS"])
                                             {
                                                 UIAlertView *alert =  [[UIAlertView alloc] initWithTitle:@"Single User Login Exception"
                                                                                                  message:@"You will not receive real-time quote updates until you LOGOUT and then re-login."
                                                                                                 delegate:self
                                                                                        cancelButtonTitle:@"OK"
                                                                                        otherButtonTitles:nil];
                                                 
                                                 alert.tag = LOGOUT_ALERTVIEW_TAG;
                                                 [alert show];
                                                 
                                                 [timerComstockNewsList invalidate];
                                                 [timerSnapQuote invalidate];
                                                 [_timerAlerts invalidate];
                                                 [_timerSessionCheck invalidate];
                                             }
                                             
                                             sessionResponse = nil;
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Session Check Failed. %@", error.localizedDescription);
                                         }];
    [operation start];
}

#pragma mark - UIScrollView
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;     //this delegate method is not for tables. only scroll view
    }
    
    @try {
        
        static NSInteger previousPage = 0;
        
        CGFloat pageWidth = scrollView.frame.size.width;
        _greatWallPageControl.currentPage = floor((scrollView.contentOffset.x - pageWidth/2) / pageWidth) + 1;
        
        BOOL isNewPage = _greatWallPageControl.currentPage != previousPage;
        
        MarketMonitor *mm = _marketMonitors[_currentMMIndex];
        GreatWall *gw = mm.greatWalls[_currentGWIndex];
        [_visibleSubTable setValue:[NSString stringWithFormat:@"%d",_greatWallPageControl.currentPage] forKey:gw.index];
        
        if (isNewPage)
        {
            previousPage = _greatWallPageControl.currentPage;
            
            [[self visibleTable] updateTickersFormat];

            if ([[self visibleTable] selectedTickerSymbol])
            {
                lastTickerSelected = [[self visibleTable] selectedTickerSymbol];
            }
            
            [[self visibleTable] setEditing:NO];
            _editButton.title = @"Edit";
            
            [self updateTimer];
        }
    }
    @catch (NSException *exception) {
        
        [[[UIAlertView alloc] initWithTitle:@"EXCEPTION"
                                    message:exception.description
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

- (void)scrollToTableIndex:(NSUInteger)tableIndex
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];

    [greatWallScroll setContentOffset:CGPointMake(tableIndex*greatWallScroll.frame.size.width, 0) animated:NO];
        
    [_visibleSubTable setValue:[NSString stringWithFormat:@"%d", tableIndex] forKey:gw.index];

    [[self visibleTable] setEditing:NO];
    _editButton.title = @"Edit";

    lastTickerSelected = [[self visibleTable] selectedTickerSymbol];
    [self updateTimer];
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    switch (tableView.tag) {
        case NEWS_TABLE_TAG:            return comstockNewsList.count;
            break;
            
        case POPOVER_TABLE_TAG:         return popoverMenuItems.count;
            break;
            
        case MARKETMONITOR_MENU_TAG:    return _marketMonitors.count;
            break;
            
        case TICKER_LOOKUP_TABLE_TAG:   return _tickerLookupResponse.count;
            break;

        default: //NSLog(@"UNKNOWN TABLE %d", tableView.tag);
            return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (tableView.tag) {
        case POPOVER_TABLE_TAG:         
        case MARKETMONITOR_MENU_TAG:
        case TICKER_LOOKUP_TABLE_TAG:   return 0; break;

        default:                        return TICKER_TABLE_ROW_HEIGHT*0.666; break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == NEWS_TABLE_TAG) {
        
        static UILabel *headerLabel;
        
        if (!headerLabel) {
            headerLabel = [[UILabel alloc] init];
            headerLabel.backgroundColor = [UIColor colorWithRed:9.0/255.0 green:11.0/255.0 blue:13.0/255.0 alpha:1.0];
            headerLabel.textColor = [UIColor whiteColor];
            headerLabel.text = @"Breaking News";
            headerLabel.font = [UIFont italicSystemFontOfSize:16];
            headerLabel.textAlignment = NSTextAlignmentCenter;
        }
        
        return headerLabel;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section;
{
    switch (tableView.tag) {
        case POPOVER_TABLE_TAG:
        case MARKETMONITOR_MENU_TAG:
        case TICKER_LOOKUP_TABLE_TAG:   return 0; break;

        default:                        return TICKER_TABLE_ROW_HEIGHT/2; break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (tableView.tag==NEWS_TABLE_TAG) {
        
        UIButton *footer = [UIButton buttonWithType:UIButtonTypeCustom];
        footer.backgroundColor = [UIColor colorWithRed:9.0/255.0 green:11.0/255.0 blue:13.0/255.0 alpha:1.0];
        [footer addTarget:self action:@selector(getComstockNewsList) forControlEvents:UIControlEventTouchUpInside];
        
        return footer;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (tableView.tag == NEWS_TABLE_TAG) ? TICKER_TABLE_ROW_HEIGHT*1.4 : TICKER_TABLE_ROW_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];    

    switch (tableView.tag) {

        case NEWS_TABLE_TAG:
            cell.textLabel.numberOfLines=2;
            
            cell.textLabel.text = comstockNewsList[indexPath.row][@"headline"];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ - %@", comstockNewsList[indexPath.row][@"source"], comstockNewsList[indexPath.row][@"date"]];
            
            //if (isNewsReadDict[comstockNewsList[indexPath.row][@"mLastNewsID"]]) {
            if ([_readNewsIDs containsObject:[NSString stringWithFormat:@"%@", comstockNewsList[indexPath.row][@"id"]]]) {
                cell.textLabel.font = [UIFont systemFontOfSize:13];
                cell.detailTextLabel.font = [UIFont systemFontOfSize:11];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            else {
                cell.textLabel.font = [UIFont boldSystemFontOfSize:13];
                cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:11];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            
            break;
            
        case POPOVER_TABLE_TAG:
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.text = popoverMenuItems[indexPath.row];
            cell.contentView.backgroundColor = [UIColor whiteColor];
            break;
            
        case MARKETMONITOR_MENU_TAG:
            cell.textLabel.text = [_marketMonitors[indexPath.row] name];
            cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fundaBg-active"]];
            break;
            
        case TICKER_LOOKUP_TABLE_TAG:
            cell.textLabel.textColor = [UIColor blackColor];
            cell.textLabel.text = _tickerLookupResponse[indexPath.row][@"tickerName"];
            cell.detailTextLabel.text = _tickerLookupResponse[indexPath.row][@"companyName"];
            cell.contentView.backgroundColor = [UIColor whiteColor];

            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIColor *oddBgColor = [UIColor colorWithRed:31.0/255.0 green:41.0/255.0 blue:53.0/255.0 alpha:1.0];
    UIColor *evenBgColor = [UIColor colorWithRed:10.0/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1.0];
    cell.backgroundColor = (indexPath.row%2) ? evenBgColor : oddBgColor;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self backgroundTouched];
    
    @try {
        if (tableView.tag == NEWS_TABLE_TAG)
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            //mark news as read
            if (![_readNewsIDs containsObject:[NSString stringWithFormat:@"%@", comstockNewsList[indexPath.row][@"id"]]]) {
                [_readNewsIDs addObject:[[NSString alloc] initWithFormat:@"%@", comstockNewsList[indexPath.row][@"id"]]];
            }
            
            if (!newsDetailVC) {
                newsDetailVC = [[NewsDetailWebViewController alloc] initWithNibName:@"NewsDetailWebViewController" bundle:nil];
            }
            
            newsDetailVC.newsList = comstockNewsList;
            newsDetailVC.dowJonesFlag = _dowJonesNewsFlag;
            newsDetailVC.openSectionIndex = indexPath.row;
            newsDetailVC.isNewsRead = _readNewsIDs;
            
            [self presentViewController:newsDetailVC animated:YES completion:nil];
            
            [newsTableView reloadData];
        }
        else if (tableView.tag == POPOVER_TABLE_TAG)
        {
            if ([companyInfoMenuItems containsObject:popoverMenuItems[indexPath.row]])  //if company info
            {
                if (!compFundaVC) {
                    compFundaVC = [[CompanyFundamentalsViewController alloc] initWithNibName:@"CompanyFundamentalsViewController" bundle:nil];
                }
                
                compFundaVC.tickerSymbol = lastTickerSelected;
                compFundaVC.companyFundaTitles = companyInfoMenuItems;
                compFundaVC.selectedTitle = companyInfoMenuItems[indexPath.row];
                
                [self presentViewController:compFundaVC animated:YES completion:nil];
            }
            else if ([miscMenuItems containsObject:popoverMenuItems[indexPath.row]])    //if misc URL
            {
                if (!miscVC) {
                    miscVC = [[MiscViewController alloc] initWithNibName:@"MiscViewController" bundle:nil];
                }
                
                miscVC.miscIndex = indexPath.row;
                miscVC.miscTitles = miscMenuItems;
                [self presentViewController:miscVC animated:YES completion:nil];
                
            }
            else
            {
                @try {
                    
                    SEL methodName = NSSelectorFromString(selectorForString[popoverMenuItems[indexPath.row]]);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                    [self performSelector:methodName];
#pragma clang diagnostic pop
                    
                }
                @catch (NSException *exception) {
                    //NSLog(@"EXCEPTION CAUGHT in %s\n%@",__PRETTY_FUNCTION__,exception.description);
                }
            }
        }
        else if (tableView.tag == MARKETMONITOR_MENU_TAG)
        {
            [self showHideMarketMonitorMenu:FALSE];
            
            if (_currentMMIndex != indexPath.row)
            {
                _currentMMIndex = indexPath.row;
                _currentGWIndex = 0;
                
                _greatWallPageControl.currentPage = 0;
                
                [self performSelectorOnMainThread:@selector(configureGreatWalls) withObject:nil waitUntilDone:YES];
            }
        }
        else if (tableView.tag == TICKER_LOOKUP_TABLE_TAG)
        {
            [self addTickerToSubTable:_tickerLookupResponse[indexPath.row][@"tickerName"]];
        }
    }
    @catch (NSException *exception) {
        //NSLog(@"%@", exception.description);
    }
}

#pragma mark - TickerTableViewDelegate
- (NSString*)marketMonitorIndex
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    return mm.index;
}

- (NSString*)greatWallIndex
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    
    return gw.index;
}

- (NSString*)tableIndex
{
    return [NSString stringWithFormat:@"%d",_greatWallPageControl.currentPage];
}

-(void)didSelectTicker:(Ticker *)ticker
{
    [self toggleMonitorMenu:NO];

    if (ticker==nil) {
        //NSLog(@"ticker==nil");
        ticker = [[Ticker alloc] init];
        ticker.ticker = lastTickerSelected;
    }
    
    lastTickerSelected = ticker.ticker;
    
    [self getDetailedQuoteForTicker:ticker];
}

- (void)deleteTickerAtIndexPath:(NSIndexPath *)indexPath
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    WallTable *wTable = gw.tickerLists[_greatWallPageControl.currentPage];
    
    NSString *deleteTickerURL = [NSString stringWithFormat:LAYOUT_DELETE_TICKER_URL, currentUserID, mm.index, gw.index, indexPath.row,_greatWallPageControl.currentPage];

    if ([self isLayoutUpdateSuccessForURL:deleteTickerURL])
    {
        NSMutableArray *temp = wTable.tickerSymbols.mutableCopy;
        [temp removeObjectAtIndex:indexPath.row];
        wTable.tickerSymbols = temp;
        
        [[self visibleTable] deleteRowAtIndexPath:indexPath];
    }
}

- (NSArray*)columnArray
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    
    return gw.columns;
}

- (void)modifyColumns
{

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setColumnsToGreatWall:)
                                                 name:kReloadDataInTable
                                               object:nil];
    
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    
    static NSDictionary* formattedKeys ;
    
    if (!formattedKeys)
    {
        formattedKeys = [[NSDictionary alloc] initWithObjectsAndKeys:
                         @"Ask", @"ask",
                         @"Ask Exchg", @"askExchg",
                         @"Size", @"s",
                         @"Bid ", @"bid",
                         @"Bid Exchg", @"bidExchg",
                         @"Chg", @"chg",
                         @"Exchg", @"exchg",
                         @"High", @"high",
                         @"Last", @"last",
                         @"Low", @"low",
                         @"LTT", @"ltt",
                         @"LTV", @"ltv",
                         @"Open", @"open",
                         @"%Chg", @"perChg",
                         @"PC", @"lastClosed",
                         @"Vol", @"vol",
                         @"VWAP", @"vwap",
                         @"Tick", @"T",
                         @"News", @"ni",
                         @"Halt", @"isHalt",
                         nil];
    }
    
    ModifyColumnsVC *modifyColVC = [[ModifyColumnsVC alloc] initWithNibName:@"ModifyColumnsVC" bundle:nil];
    modifyColVC.selectedColumns = [[NSMutableArray alloc] initWithArray:gw.columns];
    modifyColVC.formattedKeys = formattedKeys;
    
    modifyColVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:modifyColVC animated:YES completion:nil];
}

- (void)setColumnsToGreatWall:(NSNotification*)notif
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kReloadDataInTable
                                                  object:nil];

    NSArray *newColumns = [NSArray arrayWithArray:notif.userInfo[@"newColumns"]];
    
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];

    NSString *columnsUpdateURL = [NSString stringWithFormat:COLUMNS_UPDATE_URL, currentUserID, gw.index, [newColumns componentsJoinedByString:@","]];
    
    if ([self isLayoutUpdateSuccessForURL:columnsUpdateURL])
    {
        gw.columns = [[NSMutableArray alloc] initWithArray:newColumns];
        [[self visibleTable] reloadTable];
    }
}

#pragma mark - TickerTableCellDelegate
- (void)loadNewsForNewsID:(NSString*)newsID
{
    NSString *newsSource = _dowJonesNewsFlag ? @"DOWJONES" : @"COMTEX";
    NSString *newsURL = [NSString stringWithFormat:COMSTOCK_NEWS_DETAIL_URL, newsID, newsSource];
    
    if (![_readNewsIDs containsObject:newsID])
        [_readNewsIDs addObject:newsID];
    
    SingleNewsVC *newsView = [[SingleNewsVC alloc] initWithNibName:@"SingleNewsVC" bundle:nil];
    newsView.newsURL = newsURL;
    
    newsView.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:newsView animated:YES completion:^{
        [[self visibleTable] reloadTable];
    }];
}

#pragma mark - DetailQuoteViewDelegate
- (void)loadCompanyNewsForTicker:(NSString *)symbol
{
    if (!compFundaVC) {
        compFundaVC = [[CompanyFundamentalsViewController alloc] initWithNibName:@"CompanyFundamentalsViewController" bundle:nil];
    }
    
    compFundaVC.tickerSymbol = symbol;
    compFundaVC.companyFundaTitles = companyInfoMenuItems;
    compFundaVC.selectedTitle = companyInfoMenuItems[1];
    
    [self presentViewController:compFundaVC animated:YES completion:nil];
}

#pragma mark - UISearchBar
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    return YES; //no conditions for now
    
    BOOL isBackspace = text.length==0;
    BOOL isSearchButton = [text isEqualToString:@"\n"];
    
    if (isBackspace || isSearchButton)
        return YES;

    
    NSCharacterSet *letters = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    
    BOOL isFirstLetter = searchBar.text.length==0;
    BOOL isLetter = [text rangeOfCharacterFromSet:letters].location != NSNotFound;

    if (isFirstLetter)
        return isLetter;
  
        
    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~#$%^*-+=\\/:."] invertedSet];
    
    BOOL isValidCharacter = ([text rangeOfCharacterFromSet:notAllowedChars].location == NSNotFound);
    BOOL hasValidSize = searchBar.text.length<10;
    
    
    if (isValidCharacter && hasValidSize) {
        return YES;
    }
    else return NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self addTickerToSubTable:searchBar.text];
    searchBar.text = @"";
}

- (void)addTickerToSubTable:(NSString*)tickerSymbol
{
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    }
    
    tickerSymbol = [tickerSymbol stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    tickerSymbol = tickerSymbol.uppercaseString;
    
    //save updated ticker to layout
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    WallTable *wTable = gw.tickerLists[_greatWallPageControl.currentPage];

    NSString *addTickerToWallURL = [NSString stringWithFormat:
                                    LAYOUT_ADD_TICKER_URL,
                                    currentUserID,
                                    tickerSymbol,
                                    _greatWallPageControl.currentPage,
                                    mm.index,
                                    gw.index];

    
    if ([self isLayoutUpdateSuccessForURL:addTickerToWallURL])
    {
        Ticker *newTickerOb = [[Ticker alloc] init];
        newTickerOb.tickerNameByUser = tickerSymbol;
        
        //check ticker updater
        NSString *tickerUpdaterURL = [NSString stringWithFormat:TICKER_UPDATER_URL, tickerSymbol, currentUserName];
        NSMutableArray *updatedTicker = [ProjectHandler dataFromAPIWithStringURL:tickerUpdaterURL];
        
        tickerSymbol = updatedTicker.count>0 ? updatedTicker.lastObject : tickerSymbol;
        
        newTickerOb.ticker = tickerSymbol;

        NSMutableArray *temp = wTable.tickerSymbols.mutableCopy;
        [temp addObject:tickerSymbol];
        wTable.tickerSymbols = temp;
        
        //insert ticker row in table
        [[self visibleTable] insertTicker:newTickerOb atIndexPath:[NSIndexPath indexPathForRow:wTable.tickerSymbols.count-1 inSection:0]];
        
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self toggleMonitorMenu:NO];

    BOOL isBackspace = string.length==0;
    BOOL isSearchButton = [string isEqualToString:@"\n"];
    
    if (isBackspace || isSearchButton)
        return YES;
    
    
    NSCharacterSet *notAllowed = [NSCharacterSet characterSetWithCharactersInString:@"#%€¥$[]{}^*+=_|~<>£"];
    if ([string rangeOfCharacterFromSet:notAllowed].location != NSNotFound) {
        return NO;
    }

    return (textField.text.length<15);
}

#pragma mark - UI IBActions
- (IBAction)backgroundTouched
{
    [tickerLookupSearch resignFirstResponder];
    
    if ([popoverController isPopoverVisible]) {
        [popoverController dismissPopoverAnimated:YES];
    }
    
    [self toggleMonitorMenu:NO];
}

- (IBAction)logoutButton
{
    UIAlertView *logoutAlert = [[UIAlertView alloc] initWithTitle:@"LOGOUT" message:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    
    logoutAlert.tag = LOGOUT_ALERTVIEW_TAG;
    
    [logoutAlert show];
    
    logoutAlert = nil;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    
    if (buttonIndex==1)
    {
        switch (alertView.tag) {
                
            case RENAME_GREATWALL_ALERTVIEW_TAG:
            {
                NSString *newName = [[alertView textFieldAtIndex:0] text];
                NSString *renameGWURL = [[NSString alloc] initWithFormat:LAYOUT_UPDATE_GREATWALL_RENAME_URL, currentUserID, mm.index, gw.index,  newName];
                
                if ([self isLayoutUpdateSuccessForURL:renameGWURL])
                {
                    //update UI
                    gw.name = newName;
                    [_greatWallSegment setTitle:gw.name forSegmentAtIndex:_greatWallSegment.selectedSegmentIndex];
                }
                
                renameGWURL = nil;
            }
                break;
                
            case REMOVE_GW_ALERTVIEW_TAG:
            {
                
                //hit delete great wall api
                NSString *removeGWURL = [[NSString alloc] initWithFormat:LAYOUT_DELETE_GREATWALL_URL, currentUserID, mm.index, gw.index];
                
                if ([self isLayoutUpdateSuccessForURL:removeGWURL])
                {
                    //change selected segment
                    NSInteger deleteGW = _greatWallSegment.selectedSegmentIndex;
                    
                    [_greatWallSegment removeSegmentAtIndex:deleteGW animated:YES];
                    
                    [_greatWallSegment setSelectedSegmentIndex:(deleteGW>0 ? deleteGW-1 : 0)];
                    
                    //remove from local
                    NSMutableArray *temp = mm.greatWalls.mutableCopy;
                    [temp removeObjectAtIndex:_currentGWIndex];
                    mm.greatWalls = temp;
                    
                    //show notification
                    NANotification *notif = [[NANotification alloc] initWithText:[NSString stringWithFormat:@"%@ removed.",gw.name]];
                    [self.view addSubview:notif];
                    
                    //set UI
                    [self greatWallSelected:_greatWallSegment];
                }
                
                removeGWURL = nil;
            }
                break;
                
            case REMOVE_ST_ALERTVIEW_TAG: [self removeSubtable]; break;
                
            case LOGOUT_ALERTVIEW_TAG:
            {
                [self backgroundTouched];
                
                NSString *logoutURL = [NSString stringWithFormat:LOGOUT_URL,currentUserID,currentSessionID];
                NSDictionary *jsonResponseDict = [[NSDictionary alloc] initWithDictionary:[ProjectHandler dataFromAPIWithStringURL:logoutURL]];
                
                //if ([[jsonResponseDict valueForKey:@"isLogout"] boolValue]) {
                if (jsonResponseDict.count) {
                    
                    static BOOL flag = TRUE;
                    
                    while (flag) {
                        
                        flag = FALSE;
                        
                        [self dismissViewControllerAnimated:YES completion:^{
                        
                            flag = TRUE;
                            [timerComstockNewsList invalidate];
                            [timerSnapQuote invalidate];
                            [_timerAlerts invalidate];
                            [_timerSessionCheck invalidate];
                            
                            [comstockNewsList removeAllObjects];
                            [_readNewsIDs removeAllObjects];
                            
                            [_sharedInstance.pendingSymbols removeAllObjects];
                            [_sharedInstance.subscribedSymbols removeAllObjects];
                            [_sharedInstance.nonSubscribedSymbols removeAllObjects];
                        }];
                    }
                }
                else
                {
                    [[[UIAlertView alloc] initWithTitle:@"Logout Failure" message:@"You could not be logged out." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
                
                jsonResponseDict = nil;
                
                break;
            }
                
            case SYNC_ALERTVIEW_TAG:
                
                [self backgroundTouched];
                
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                
                //get market monitors at launch
                NSString *syncURL = [NSString stringWithFormat:LAYOUT_SYNC_URL,currentUserID];
                [self performSelector:@selector(getMarketMonitorResponseFromURL:) onThread:[NSThread mainThread] withObject:syncURL waitUntilDone:YES];
                
                break;
        }
    }
}

- (TickerTableView *)visibleTable
{
    NSInteger x = greatWallScroll.contentOffset.x;
    
    for (TickerTableView *thisTable in greatWallScroll.subviews) {
        if ([thisTable isKindOfClass:[TickerTableView class]] && thisTable.frame.origin.x==x) {
            return thisTable;
        }
    }
    
    return nil;
}

- (IBAction)toggleEditingState:(UIBarButtonItem*)sender
{
    BOOL isAlreadyEditing = [[self visibleTable] isEditing];
    
    sender.title = isAlreadyEditing ? @"Edit" : @"Done";

    [[self visibleTable] setEditing:!isAlreadyEditing];
}

- (IBAction)showPopover:(UIButton*)senderButton
{    
    popoverMenuItems = [NSMutableArray arrayWithArray:allMenuItems[senderButton.titleLabel.text]];
    
    if (popoverMenuItems.count) {
        
        [self backgroundTouched];

        //create a popover (table) here
        UITableViewController *tableMenu = [[UITableViewController alloc] init];
        tableMenu.tableView.dataSource = self;
        tableMenu.tableView.delegate = self;
        tableMenu.tableView.tag = POPOVER_TABLE_TAG;
        
        //set popover content size
        CGFloat maxHeight = TICKER_TABLE_ROW_HEIGHT * 8;
        CGFloat actualHeight = TICKER_TABLE_ROW_HEIGHT * popoverMenuItems.count;

        actualHeight = (actualHeight > maxHeight) ? maxHeight : actualHeight;

        [tableMenu setContentSizeForViewInPopover:CGSizeMake(300, actualHeight)];
        
        //show popover
        if ([popoverController isPopoverVisible]) {
            [popoverController dismissPopoverAnimated:YES];
        }
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:tableMenu];
        [popoverController presentPopoverFromRect:senderButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
    else if ([senderButton.titleLabel.text isEqualToString:@"Options"])
    {
        if ([DICT_ENTITLEMENTS[@"optionEntitlementFlag"] boolValue]) {
            
            optionsPlusVC = [[OptionsPlusViewController alloc] initWithNibName:@"OptionsPlusViewController" bundle:nil];
            
            optionsPlusVC.root = lastTickerSelected;
            optionsPlusVC.columnArray = _optionsColumns;
            
            optionsPlusVC.modalPresentationStyle = UIModalPresentationFullScreen;
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(updateOptionsColumn:)
                                                         name:kReloadDataInTable
                                                       object:nil];

            [self presentViewController:optionsPlusVC animated:YES completion:nil];
        }
        else {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"You are not entitled for Options+.\nPlease contact Quodd Support for details."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];

        }
        
    }
    else if ([senderButton.titleLabel.text isEqualToString:@"Charts"])
    {
        
        NSDictionary *entitlements = DICT_ENTITLEMENTS;
        if ([entitlements[@"exchangeHash"][@"barchart_access"] count] == 0) {
            
            [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                        message:@"You are not entitled for stock charts.\nPlease contact Quodd Support for details."
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
            
        }
        else {
            
            [[[UIAlertView alloc] initWithTitle:@"Stock Charts will be available in the next version."
                                        message:nil
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
        
        //ChartsViewController *chartsVC = [[ChartsViewController alloc] initWithNibName:@"ChartsViewController" bundle:nil];
        //chartsVC.tickerName = lastTickerSelected;
        //[self presentViewController:chartsVC animated:YES completion:nil];
    }
}

- (IBAction)showTickerLookupView
{
    if (!tickerLookupVC) {
        tickerLookupVC = [[TickerLookupViewController alloc] initWithNibName:@"TickerLookupViewController" bundle:nil];
    }
    [self presentModalViewController:tickerLookupVC animated:YES];
}

- (void)toggleMonitorMenu:(BOOL)flag
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         
                         if (flag) {
                             
                             //_marketMonitorsTable.tag = MARKETMONITOR_MENU_TAG;
                             [_marketMonitorsTable reloadData];
                             
                             NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_currentMMIndex inSection:0];
                             [_marketMonitorsTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];
                             
                             [_marketMonitorMenu setFrame:CGRectMake(0, 100, _marketMonitorMenu.frame.size.width, _marketMonitorMenu.frame.size.height)];
                         }
                         else {
                             
                             [_marketMonitorMenu setFrame:CGRectMake(-300, 100, _marketMonitorMenu.frame.size.width, _marketMonitorMenu.frame.size.height)];
                         }                         
                     }];
}

- (IBAction)showHideMarketMonitorMenu:(BOOL)flag
{
    static BOOL menuHidden = YES;
    
    [self toggleMonitorMenu:menuHidden];

    menuHidden = !menuHidden;
}

- (IBAction)greatWallSelected:(UISegmentedControl*)senderSegment
{
    [self toggleMonitorMenu:NO];
    
    if (senderSegment.selectedSegmentIndex > -1) {
        
        _currentGWIndex = senderSegment.selectedSegmentIndex;
        
        MarketMonitor *mm = _marketMonitors[_currentMMIndex];
        [_selectedGWIndices setValue:[NSString stringWithFormat:@"%d",_currentGWIndex] forKey:mm.index];
        
        [self configureSubTablesForGreatWall];
    }
}

- (void)showMajorIndicatorsView
{
    if (!majorVC) {
        majorVC = [[MajorIndicatorsIndicesVC alloc] initWithNibName:@"MajorIndicatorsIndicesVC" bundle:nil];
    }
    
    majorVC.viewTitle = @"Major Market Indicators";
    
    majorVC.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:majorVC animated:YES completion:nil];
}

- (void)showMajorIndicesView
{
    if (!majorVC) {
        majorVC = [[MajorIndicatorsIndicesVC alloc] initWithNibName:@"MajorIndicatorsIndicesVC" bundle:nil];
    }
    
    majorVC.viewTitle = @"Major Market Indices";

    majorVC.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:majorVC animated:YES completion:nil];
}

- (void)showBreakingNews
{
    if (comstockNewsList.count) {
        
        if (!newsDetailVC) {
            newsDetailVC = [[NewsDetailWebViewController alloc] initWithNibName:@"NewsDetailWebViewController" bundle:nil];
        }

        newsDetailVC.newsList = comstockNewsList;
        newsDetailVC.dowJonesFlag = _dowJonesNewsFlag;
        newsDetailVC.openSectionIndex = NSNotFound;
        newsDetailVC.isNewsRead = _readNewsIDs;

        [self presentViewController:newsDetailVC animated:YES completion:nil];
    }
}

- (IBAction)showAlertVC
{    
    AlertsViewController *alertVC = [[AlertsViewController alloc] initWithNibName:@"AlertsViewController" bundle:nil];

    [self presentViewController:alertVC
                       animated:YES
                     completion:^{
                         alertVC.tickerName.text = lastTickerSelected;
    }];
}

- (IBAction)showSettingsVC
{
    SettingsViewController *settingsVC = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];

    settingsVC.modalPresentationStyle = UIModalPresentationFormSheet;
    
    [self presentViewController:settingsVC animated:YES completion:^{
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(settingsUpdate)
                                                     name:kSettingsUpdated
                                                   object:nil];
    }];
}

- (void)settingsUpdate
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kSettingsUpdated
                                                  object:nil];
    
    [[self visibleTable] updateTickersFormat];

    [tickerLookupSearch resignFirstResponder];

    [timerSnapQuote fire];
}

- (void)updateOptionsColumn:(NSNotification*)notif
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kReloadDataInTable
                                                  object:nil];
    
    _optionsColumns = [[NSArray alloc] initWithArray:notif.userInfo[@"newColumns"]];
}

- (void)upcoming
{
    [[[UIAlertView alloc] initWithTitle:@"Not Available"
                                message:@"This feature will be available in upcoming builds." delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

#pragma mark - UIActionSheet
-(void) greatWallActionSheet
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Great Wall options"
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"Add New Great Wall", nil];

    if (mm.greatWalls.count>1) {
        [popupQuery addButtonWithTitle:@"Remove Great Wall"];
    }

    if (mm.greatWalls.count>0) {
        [popupQuery addButtonWithTitle:@"Rename Great Wall"];
        
        if (gw.tickerLists.count<15) {
            [popupQuery addButtonWithTitle:@"Add new table"];
        }
    }
    
    if (gw.tickerLists.count>1) {
        [popupQuery addButtonWithTitle:@"Remove current table"];
    }
    
    popupQuery.tag = 1;
    
    [popupQuery showInView:self.view];
    
    popupQuery = nil;
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag==1 && buttonIndex >= 0) {
        
        MarketMonitor *mm = _marketMonitors[_currentMMIndex];
        GreatWall *gw = mm.greatWalls[_currentGWIndex];

        NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
        
        BOOL isAddNewGW = [buttonTitle isEqualToString:@"Add New Great Wall"];
        BOOL isRenameGW = [buttonTitle isEqualToString:@"Rename Great Wall"];
        BOOL isRemoveGW = [buttonTitle isEqualToString:@"Remove Great Wall"];
        BOOL isAddNewST = [buttonTitle isEqualToString:@"Add new table"];
        BOOL isRemoveST = [buttonTitle isEqualToString:@"Remove current table"];
        

        if (isAddNewGW) {
            
            [self addNewGreatWall];
        }
        else if (isRenameGW)
        {
            //show alert to rename greatwall
            NSString *oldName = gw.name;
            
            UIAlertView *renameAlert = [[UIAlertView alloc] initWithTitle:@"Rename Great Wall"
                                                                  message:[NSString stringWithFormat:@"Enter new name for '%@'",oldName]
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                        otherButtonTitles:@"OK", nil];

            renameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
            renameAlert.tag = RENAME_GREATWALL_ALERTVIEW_TAG;
            
            [[renameAlert textFieldAtIndex:0] setText:oldName];
            [[renameAlert textFieldAtIndex:0] setDelegate:self];
            
            [renameAlert show];
        }
        else if (isRemoveGW)
        {
            //show alert to confirm remove great wall
            UIAlertView *removeGWAlert = [[UIAlertView alloc] initWithTitle:@"Remove Great Wall" message:@"Are you sure you want to delete this great wall?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
            
            removeGWAlert.tag = REMOVE_GW_ALERTVIEW_TAG;
            
            [removeGWAlert show];
        }
        else if (isAddNewST)            //add new sub table
        {

            NSString *addTableURL = [NSString stringWithFormat:LAYOUT_ADD_SUBTABLE_URL, currentUserID, mm.index, gw.index];
            
            if ([self isLayoutUpdateSuccessForURL:addTableURL])
            {
                //add empty table to local great wall data
                WallTable *newTable = [[WallTable alloc] init];
                newTable.index = gw.tickerLists.count+1;
                newTable.tickerSymbols = [[NSMutableArray alloc] init];
                newTable.selectedTickerIndex = 0;
                [gw.tickerLists addObject:newTable];

                [self configureSubTablesForGreatWall];
                
                _greatWallPageControl.numberOfPages = gw.tickerLists.count;
                _greatWallPageControl.currentPage = gw.tickerLists.count-1;
                [self scrollToTableIndex:_greatWallPageControl.currentPage];
                

                //show notification
                NANotification *notif = [[NANotification alloc] initWithText:@"New Table Added."];
                [self.view addSubview:notif];
            }
        }
        else if (isRemoveST)
        {
            //show alert to remove sub table
            UIAlertView *removeSTAlert = [[UIAlertView alloc] initWithTitle:@"Remove Current Sub Table" message:@"Are you sure you want to delete this table from great wall?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            
            removeSTAlert.tag = REMOVE_ST_ALERTVIEW_TAG;
            
            [removeSTAlert show];
            removeSTAlert = nil;
        }
    }
}

#pragma mark - API calls
- (BOOL)isLayoutUpdateSuccessForURL:(NSString*)url
{
    NSDictionary *response = [ProjectHandler dataFromAPIWithStringURL:url];
    
    if (response!=nil && [response[@"Status"] isEqualToString:@"SUCCESS"])
    {
        return YES;
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Update Failure!" message:@"Could not update layout. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return NO;
    }
}

- (IBAction)reloadGreatWallData
{
//    [self sendMail];
//    return;
    
    //show alert to sync
    UIAlertView *syncAlert = [[UIAlertView alloc] initWithTitle:@"Sync with EQ+" message:@"Are you sure you want to sync with the EQ+ server layout? The current layout will be removed. This cannot be undone." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    syncAlert.tag = SYNC_ALERTVIEW_TAG;
    
    [syncAlert show];
    syncAlert = nil;
}

- (id) objectAtLevel: (NSUInteger) level
{
    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    if (level == 0) return mm;
    
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    if (level == 1) return gw;
    
    WallTable *wTable = gw.tickerLists[_greatWallPageControl.currentPage];
    return wTable;
}

- (void)getDetailedQuoteForTicker:(Ticker*)ticker
{

    MarketMonitor *mm = _marketMonitors[_currentMMIndex];
    GreatWall *gw = mm.greatWalls[_currentGWIndex];
    WallTable *wTable = gw.tickerLists[_greatWallPageControl.currentPage];

    if (wTable.tickerSymbols.count==0) {
        return;
    }
    
    if (!_detailQuoteView)
    {
        NSArray *topObjects = [[NSBundle mainBundle] loadNibNamed:@"DetailQuoteView" owner:self options:nil];
        for (id currentObject in topObjects)
        {
            if ([currentObject isKindOfClass:[DetailQuoteView class]])
            {
                _detailQuoteView = (DetailQuoteView *)currentObject;
                break;
            }
        }
        
        [_scrollDetailQuotes addSubview:_detailQuoteView];
        
        _detailQuoteView.delegate = self;

        //added for animated-flipping of detail quote view
        
//        [UIView animateWithDuration:0.5 animations:<#^(void)animations#>]
        
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.5];
//        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_scrollDetailQuotes cache:YES];
//        [UIView commitAnimations];
    }
    
    [_detailQuoteView configureDetailQuoteViewForTicker:ticker];
}

- (void) getComstockNewsList
{
    NSString *newsSource = _dowJonesNewsFlag ? @"DOWJONES" : @"COMTEX";
    NSString *newsURL = [NSString stringWithFormat:COMSTOCK_NEWS_LIST_URL,newsSource];

    NSURLRequest *request = [ProjectHandler urlRequestWithURL:newsURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSDictionary *jsonResponseNewsList = [NSDictionary dictionaryWithDictionary:JSON];
                                             
                                             NSMutableArray *newNewsIDs = [[jsonResponseNewsList allKeys] mutableCopy];
                                             NSMutableArray *oldNewsIDs = [[NSMutableArray alloc]init];
                                             
                                             if (!comstockNewsList) {
                                                 comstockNewsList = [[NSMutableArray alloc]init];
                                             }
                                             
                                             for (int i=0; i<comstockNewsList.count; i++) {
                                                 [oldNewsIDs addObject:comstockNewsList[i][@"id"]];
                                             }
                                             
                                             for (int i=0; i<newNewsIDs.count; i++) {
                                                 
                                                 if (![oldNewsIDs containsObject:newNewsIDs[i]]) {
                                                     
                                                     [comstockNewsList insertObject:jsonResponseNewsList[newNewsIDs[i]] atIndex:0];
                                                     
                                                     //[isNewsReadDict setValue:[NSNumber numberWithBool:NO] forKey:newNewsIDs[i]];
                                                 }
                                             }
                                                                                          
                                             NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
                                             [comstockNewsList sortUsingDescriptors:@[sort]];
                                             
                                             while (comstockNewsList.count > 50) {
                                                 [comstockNewsList removeLastObject];
                                             }
                                             
                                             [newsTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
                                             
                                             [self configureScrollViewTickers];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Unable to retrieve News. %@", error.localizedDescription);
                                         }];
    [operation start];
}

- (void)getMarketMonitorResponseFromURL:(NSString*)urlString
{
    _marketMonitorResponseComplete = [[NSMutableDictionary alloc]initWithDictionary:[ProjectHandler dataFromAPIWithStringURL:urlString]];

    NSString *columnsURL = [NSString stringWithFormat:COLUMNS_GET_URL, currentUserID];
    _columnLayoutDict = [[NSMutableDictionary alloc] initWithDictionary:[ProjectHandler dataFromAPIWithStringURL:columnsURL]];
    
    _defaultColumns = [[NSArray alloc] initWithArray:[self validColumns:_columnLayoutDict[@"$DEFAULT"]]];
    _optionsColumns = [[NSArray alloc] initWithArray:[self validColumns:_columnLayoutDict[@"OPTIONS"]]];

    [[UIApplication sharedApplication] endIgnoringInteractionEvents];

    //new architecture
    NSMutableArray *marketMonitors = [[NSMutableArray alloc] init];
    NSMutableArray *mmIndices = [[NSMutableArray alloc] initWithArray:_marketMonitorResponseComplete[@"$MONITORINDEX"][@"$MONITORINDEX"]];
    
    if (mmIndices.count>0) {
        
        _selectedGWIndices = [[NSMutableDictionary alloc] init];
        
        for (int i=0; i<mmIndices.count; i++) {
            
            MarketMonitor *monitor = [[MarketMonitor alloc] init];
            monitor.index = mmIndices[i];
            monitor.name = _marketMonitorResponseComplete[@"$MONITORINDEX"][@"$MONITORNAME"][i];
            
            monitor.greatWalls = [[NSMutableArray alloc] initWithArray:[self arrayOfGWsForMonitorResponse:_marketMonitorResponseComplete[monitor.index]]];
            
            [marketMonitors addObject:monitor];
            
            [_selectedGWIndices setValue:@"0" forKey:monitor.index];
        }
        
        _marketMonitors = [[NSMutableArray alloc] initWithArray:marketMonitors];
        
        _currentMMIndex = 0;
        _currentGWIndex = 0;
        
        [self configureGreatWalls];
    }
    else
        [[[UIAlertView alloc] initWithTitle:@"Server Error!" message:@"No market monitors found." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}

- (NSMutableArray*)arrayOfGWsForMonitorResponse:(NSDictionary*)dict
{
    NSMutableArray *gwIndexes = dict[@"$WALLINDEX"];
    NSMutableArray *gwNames = dict[@"$WALLNAME"];
    NSMutableArray *greatWalls = [[NSMutableArray alloc] initWithCapacity:gwIndexes.count];
    
    for (int i=0; i<gwIndexes.count; i++) {
        
        GreatWall *greatwall = [[GreatWall alloc] init];
        greatwall.index = gwIndexes[i];
        greatwall.name = gwNames[i];
        greatwall.columns = [[NSMutableArray alloc] initWithArray:[self validColumns:_columnLayoutDict[greatwall.index]]];
        
                
        NSMutableArray *tickerArrays = dict[gwIndexes[i]];;

        NSMutableArray *temp = [[NSMutableArray alloc] initWithCapacity:tickerArrays.count];
        
        for (NSUInteger j=0; j<tickerArrays.count; j++)
        {
            WallTable *table = [[WallTable alloc] init];
            table.index = j;
            table.tickerSymbols = tickerArrays[j];
            table.selectedTickerIndex = 0;
            
            [temp addObject:table];
        }
        
        greatwall.tickerLists = temp;

        [greatWalls addObject:greatwall];
    }
    
    return greatWalls;
}

- (NSArray*)validColumns:(NSArray*)columns
{
    return columns;
    NSMutableArray *finalColumns = [[NSMutableArray alloc] initWithArray:columns];
    
    NSMutableDictionary *widthDict = [[NSMutableDictionary alloc] initWithDictionary:[(AppDelegate *)[[UIApplication sharedApplication] delegate] columnWidth]];
    
    [widthDict setValue:@"40" forKey:@"askExchg"];
    [widthDict setValue:@"40" forKey:@"bidExchg"];
    [widthDict setValue:@"40" forKey:@"marketCenter"];
    
    NSDictionary *columnWidth = [[NSDictionary alloc] initWithDictionary:widthDict];
    
    CGFloat buffer = 10;
    CGFloat desiredTotalWidth = TICKER_TABLE_WIDTH - 2*buffer;
    CGFloat multiplicationFactor;
    
    do {
        CGFloat expectedTotalWidth = 0.0;
        for (int i=0; i<finalColumns.count; i++) {
            expectedTotalWidth += [columnWidth[finalColumns[i]] floatValue];
        }
        
        multiplicationFactor = desiredTotalWidth / expectedTotalWidth;
        
        if (multiplicationFactor > 1.0)
            break;
        else [finalColumns removeLastObject];
        
    } while (multiplicationFactor < 1.0);
    
    return finalColumns;
}

#pragma mark - Ticker Scroll
- (void)configureScrollViewTickers
{
    bottomNewsScroll.layer.cornerRadius = 4;

    NSArray *scrollIndicesTickers = [NSArray arrayWithObjects:@"I:DJI", @"I:COMP", @"I:SPX", @"I:RUT", @"I:NDX", @"I:DJT", @"I:DJU", nil];
    
    NALabelAnimation *animatedLabel = [[NALabelAnimation alloc]
                                       initWithFrame:CGRectMake(0, 0,
                                                                bottomNewsScroll.frame.size.width,
                                                                bottomNewsScroll.frame.size.height)
                                       
                                       andTickerArray:scrollIndicesTickers];
    
    for (UIView *subview in bottomNewsScroll.subviews) {
        [subview removeFromSuperview];
    }

    [bottomNewsScroll addSubview:animatedLabel];
    
}

#pragma mark - Open MFMailComposeController delegate
-(void)sendMail
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        mailer.navigationBar.tintColor = [UIColor blackColor];
        
        [mailer setSubject:@"SNAP API LOG"];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"ankit.saria@paxcel.net", nil];
        [mailer setToRecipients:toRecipients];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"SNAPlogfile.txt"];

        [mailer addAttachmentData:[NSData dataWithContentsOfFile:path] mimeType:@"text" fileName:@"SNAP Log"];

        [self presentModalViewController:mailer animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];

    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    //added to send api log
	switch (result)
	{
		case MFMailComposeResultCancelled:
			//NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			//NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
        {
			//NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:@"SNAPlogfile.txt"];

            NSFileManager *fileManager = [NSFileManager defaultManager];
            [fileManager removeItemAtPath:path error:NULL];
            
        }
			break;
		case MFMailComposeResultFailed:
			//NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			//NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}

@end
