//
//  TickerTableView.m
//  Quodd_iPad
//
//  Created by Nishant on 03/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "TickerTableView.h"
#import "TickerTableHeader.h"
#import "TickerTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

#define TICKER_TABLE_ROW_HEIGHT 39
#define SECTION_HEADER_HEIGHT 30

@interface TickerTableView ()

@property (nonatomic, retain) NSMutableArray *tickerObjects;
@property (nonatomic, retain) NSArray *columns;
@property (nonatomic, retain) TickerTableHeader *tableHeader;

@property (nonatomic, retain) NSString *nb;

@property (nonatomic, retain) ProjectHandler *sharedInstance;

@end

@implementation TickerTableView

- (void)configureWallTableWithColumns:(NSArray*)columns
{
    self.tickerTable.layer.cornerRadius = 10;
    _tickerList = _wallTable.tickerSymbols.mutableCopy;
    
    [self reloadTable];
    
    _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    _longPress.minimumPressDuration = 0.8;
    [self addGestureRecognizer:_longPress];
    
    _nb = isNASDAQBasicEntitled ? @"1" : @"0";
    
    _sharedInstance = [ProjectHandler sharedHandler];
    
    [self updateTickersFormat];
}

- (void)updateTickersFormat
{
    for (int i=0; i<_tickerList.count; i++)
    {
        OptionType optionType = [self optionTypeOfSymbol:_tickerList[i]];
        
        BOOL shouldConvert = (IS_DISPLAY_TYPE_ALTERNATE && optionType == STANDARD) || (!IS_DISPLAY_TYPE_ALTERNATE && optionType == ALTERNATE);
        
        if (shouldConvert)
        {
            NSString *primarySymbol = [self primarySymbolForOptions:_tickerList[i]]; //convert to PRIMARY symbol first
            NSString *newSymbol = [self displaySymbolForPrimaryOption:primarySymbol];
            
            Ticker *ticker = [self returnTickerForSymbol:_tickerList[i]];
            ticker.tickerNameByUser = newSymbol;
            
            [_tickerList replaceObjectAtIndex:i withObject:newSymbol];
        }
    }
    
    [self addToPendingTickers];
}

- (void)addToPendingTickers
{
    for (NSString *symbol in _tickerList)
    {
        if (![_sharedInstance.subscribedSymbols containsObject:symbol] && ![_sharedInstance.pendingSymbols containsObject:symbol] && ![_sharedInstance.nonSubscribedSymbols containsObject:symbol])
        {
            [_sharedInstance.pendingSymbols addObject:symbol];
        }
    }
    
    [self reloadTable];
}

- (NSString*)selectedTickerSymbol
{
    if (_tickerList.count>0)
    {
        NSInteger selectedRow = self.tickerTable.indexPathForSelectedRow.row;
        return _tickerList[selectedRow];
    }
    else return nil;
}

- (void)getSnapAPI:(NSTimer*)snapTimer      //snap polling for all tickers
{
    if (_tickerTable.isEditing) { //no polling on editing mode.
        return;
    }
    
    [self reloadTable];
    
    NSMutableArray *mapped = [NSMutableArray arrayWithArray:_tickerList];
    NSArray *futuresKeys = DICT_FUTURES.allKeys;
    
    //map for futures and options tickers
    for (int i=0; i<mapped.count; i++) {
        if ([futuresKeys containsObject:_tickerList[i]]) {        
            [mapped replaceObjectAtIndex:i withObject:DICT_FUTURES[mapped[i]]];
        }
        else
        {
            //if standard or alternate options ticker
            OptionType opType = [self optionTypeOfSymbol:_tickerList[i]];
            if (opType == STANDARD || opType == ALTERNATE) {
                [mapped replaceObjectAtIndex:i withObject:[self primarySymbolForOptions:_tickerList[i]]];
            }
        }
    }
    
    //check subscription
    NSMutableArray *subscribedTickers = [[NSMutableArray alloc] init];
    for (NSString *symbol in mapped)
    {
        if (![_sharedInstance.nonSubscribedSymbols containsObject:symbol] && ![subscribedTickers containsObject:symbol])
        {
            [subscribedTickers addObject:symbol];
        }
    }
    
    if (subscribedTickers.count==0) {
        [self reloadTable];
        return;
    }
    //else //NSLog(@"SUBSCRIBED %d/%d Symbols: %@", subscribedTickers.count, _tickerList.count, [subscribedTickers componentsJoinedByString:@", "]);

    NSMutableURLRequest *request = [ProjectHandler urlRequestWithURL:@"http://199.96.250.198/mobile/snapQuote.jsp?"];

    NSString *tickersParams = [NSString stringWithFormat:@"ticker=%@&source=%@&nb=%@", [subscribedTickers componentsJoinedByString:@","], snapTimer.userInfo[kNewsSource], _nb];
        
    tickersParams = [tickersParams stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    tickersParams = [tickersParams stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSData *postData = [tickersParams dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [request setHTTPBody:postData];
    
//    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    [df setLocale:[NSLocale currentLocale]];
//    [df setDateStyle:NSDateFormatterFullStyle];
//    [df setTimeStyle:NSDateFormatterLongStyle];
//    NSString *apiTimestamp = [df stringFromDate:[NSDate date]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:JSON];

                                             //NSString *responseTimestamp = [df stringFromDate:[NSDate date]];
                                             //NSString *log = [NSString stringWithFormat:@"API STARTED at %@ \n%@\n\nAPI FINISHED at %@ - %@", apiTimestamp, request.URL, responseTimestamp, dict.allKeys];
                                             //[self saveLogWithText:log];

                                             
                                             //reverse mapping of futures & options data
                                             NSDictionary *futures = DICT_FUTURES;
                                             for (NSString *symbol in _tickerList)
                                             {
                                                 BOOL isFutureSymbol = [futures.allKeys containsObject:symbol];
                                                 if (isFutureSymbol)
                                                 {
                                                     NSString *mappingSymbol = futures[symbol];
                                                     [dict setValue:dict[mappingSymbol] forKey:symbol];
                                                 }
                                                 else
                                                 {
                                                     OptionType opType = [self optionTypeOfSymbol:symbol];
                                                     if (opType == STANDARD || opType == ALTERNATE)
                                                     {
                                                         NSString *primarySymbol = [self primarySymbolForOptions:symbol];
                                                         [dict setValue:dict[primarySymbol] forKey:symbol];
                                                     }
                                                 }
                                             }
                                             
                                             NSMutableArray *tempTickerObjects = [[NSMutableArray alloc] initWithCapacity:dict.allKeys.count];
                                             
                                             //make TICKER Objects
                                             for (NSString *symbol in _tickerList) {
                                                 
                                                 Ticker *ticker = [self returnTickerForSymbol:symbol];
                                                 [ticker loadWithTicker:dict[symbol] forSymbol:symbol];
                                                 [tempTickerObjects addObject:ticker];
                                             }
                                             
                                             _tickerObjects = [[NSMutableArray alloc] initWithArray:tempTickerObjects];

                                             [self reloadTable];
                                             
                                             [self didSelectRow:_wallTable.selectedTickerIndex];
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Error Retrieving SNAP Data. %@", error.localizedDescription);
                                         }];
    [operation start];
}

- (void)hitSnapQuoteForSymbol:(NSString*)symbol     //snap for a specific ticker
{
    if (_tickerTable.isEditing) { //no polling on editing mode.
        return;
    }

    Ticker *ticker = [self returnTickerForSymbol:symbol];
    if (ticker.subscription == UNSUBSCRIBED) {
        return;
    }
    
    if ([symbol rangeOfString:@" "].location != NSNotFound) {
        symbol = [self primarySymbolForOptions:symbol];
    }

    NSString *urlString = [NSString stringWithFormat:SNAP_QUOTE_URL, _nb, symbol];
    urlString = [urlString stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];

    NSURLRequest *request = [ProjectHandler urlRequestWithURL:urlString];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:JSON];
                                             
                                             NSDictionary *futures = DICT_FUTURES;
                                             for (NSString *symbol in _tickerList)
                                             {
                                                 BOOL isFutureSymbol = [futures.allKeys containsObject:symbol];
                                                 if (isFutureSymbol)
                                                 {
                                                     NSString *mappingSymbol = futures[symbol];
                                                     [dict setValue:dict[mappingSymbol] forKey:symbol];
                                                 }
                                                 else
                                                 {
                                                     OptionType opType = [self optionTypeOfSymbol:symbol];
                                                     if (opType == STANDARD || opType == ALTERNATE)
                                                     {
                                                         NSString *primarySymbol = [self primarySymbolForOptions:symbol];
                                                         [dict setValue:dict[primarySymbol] forKey:symbol];
                                                     }
                                                 }
                                             }
                                             
                                             NSMutableArray *tempTickerObjects = [[NSMutableArray alloc] initWithCapacity:dict.allKeys.count];
                                             
                                             //make TICKER Objects
                                             for (NSString *symbol in _tickerList) {
                                                 
                                                 Ticker *ticker = [self returnTickerForSymbol:symbol];
                                                 [ticker loadWithTicker:dict[symbol] forSymbol:symbol];
                                                 [tempTickerObjects addObject:ticker];
                                             }
                                             
                                             _tickerObjects = [[NSMutableArray alloc] initWithArray:tempTickerObjects];
                                             
                                             [self reloadTable];
                                             
                                             [self didSelectRow:_wallTable.tickerSymbols.count-1]; //select last ticker
                                             
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Error Retrieving SNAP Data for %@. %@", symbol, error.localizedDescription.capitalizedString);
                                         }];
    [operation start];    
}

- (Subscription)isSubscribedFuturesTicker:(Ticker*)tickerFutures
{
    return PENDING;
    
    if (tickerFutures.subscription != SUBSCRIBED || tickerFutures.subscription != UNSUBSCRIBED)
    {
        //get detailQ data for ticker
        NSString *detailUrl = [NSString stringWithFormat:DETAILED_QUOTES_URL, tickerFutures.ticker];
        NSDictionary *detailDict = [NSDictionary dictionaryWithDictionary:[ProjectHandler dataFromAPIWithStringURL:detailUrl]];
        NSString *tickerExchg = detailDict[tickerFutures.ticker][@"exchg"];
        
        BOOL exchgEntitled = [DICT_ExchangeHash[tickerExchg][kAuthorized] isEqualToString:kAuthorized];
        return exchgEntitled ? SUBSCRIBED : UNSUBSCRIBED;
    }
    else
        return tickerFutures.subscription;
}

- (Ticker *) returnTickerForSymbol: (NSString *) tickerSymbol
{
    for (Ticker *existingTicker in _tickerObjects) {
        if ([existingTicker.tickerNameByUser isEqualToString:tickerSymbol]) {
            return existingTicker;
        }
    }
    
    Ticker *newTicker = [[Ticker alloc]init];
    newTicker.tickerNameByUser = tickerSymbol;
    return newTicker;
}

-(void)onLongPress:(UILongPressGestureRecognizer*)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        CGPoint point = [gesture locationInView:_tickerTable];
        NSIndexPath *indexPath = [_tickerTable indexPathForRowAtPoint:point];
        
        if (indexPath != nil) {
            
            UIAlertView *replaceTicker = [[UIAlertView alloc] initWithTitle:@"Edit Ticker" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
            
            replaceTicker.alertViewStyle = UIAlertViewStylePlainTextInput;
            replaceTicker.tag = indexPath.row;
            [[replaceTicker textFieldAtIndex:0] setText:_tickerList[indexPath.row]];
            [[replaceTicker textFieldAtIndex:0] setDelegate:self];
            
            [replaceTicker show];

        }
        //else //NSLog(@"long press on table but not on a row");
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1) {
        
        NSString *newTickerSymbol = [[[alertView textFieldAtIndex:0] text] uppercaseString];
        newTickerSymbol = [newTickerSymbol stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
        if (newTickerSymbol.length==0 || [[_tickerList[alertView.tag] uppercaseString] isEqualToString:newTickerSymbol]) {
            return;
        }
        
        //inform server about table update
        NSString *mmIndex;
        if (_delegate != nil && [_delegate respondsToSelector:@selector(marketMonitorIndex)])
        {
            mmIndex = [_delegate marketMonitorIndex];
        }
        
        NSString *gwIndex;
        if (_delegate != nil && [_delegate respondsToSelector:@selector(greatWallIndex)])
        {
            gwIndex = [_delegate greatWallIndex];
        }
        
        NSString *tableIndex;
        if (_delegate != nil && [_delegate respondsToSelector:@selector(tableIndex)])
        {
            tableIndex = [_delegate tableIndex];
        }
        
        NSString *replaceTickerURL = [NSString stringWithFormat:LAYOUT_UPDATE_TICKER_RENAME_URL, currentUserID, mmIndex, gwIndex, tableIndex, alertView.tag, newTickerSymbol];
        NSDictionary *response = [ProjectHandler dataFromAPIWithStringURL:replaceTickerURL];
        
        if (response && [response[@"Status"] isEqualToString:@"SUCCESS"])
        {
            Ticker *newTickerOb = [[Ticker alloc] init];
            newTickerOb.tickerNameByUser = newTickerSymbol;
            
            //check ticker updater
            NSString *tickerUpdaterURL = [NSString stringWithFormat:TICKER_UPDATER_URL, newTickerSymbol, currentUserName];
            NSMutableArray *updatedTicker = [ProjectHandler dataFromAPIWithStringURL:tickerUpdaterURL];
            
            newTickerSymbol = updatedTicker.count>0 ? updatedTicker.lastObject : newTickerSymbol;
            
            newTickerOb.ticker = newTickerSymbol;
            
            //replace in layout
            [_tickerList replaceObjectAtIndex:alertView.tag withObject:newTickerSymbol];
            _wallTable.tickerSymbols = _tickerList;
            
            [_tickerObjects replaceObjectAtIndex:alertView.tag withObject:newTickerOb];
            
            [self.tickerTable reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:alertView.tag inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            //get data for new ticker
            [self hitSnapQuoteForSymbol:newTickerSymbol];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Update Failure" message:@"Failed to update ticker. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }
}

#pragma mark Options Mapping
- (NSString*)primarySymbolForOptions:(NSString*)symbol
{
    OptionType type = [self optionTypeOfSymbol:symbol];
    
    if (type == STANDARD || type == ALTERNATE)
    {
        NSString *root = [symbol componentsSeparatedByString:@" "][0];
        
        NSString *middle = [symbol componentsSeparatedByString:@" "][1];
        NSString *date;
        NSString *strike;
                
        if (type == ALTERNATE)
        {
            //alernate MSFT 130119C24.0
            date = [middle substringToIndex:6];
            strike = [middle substringFromIndex:7];
            
            BOOL _isPut = [middle characterAtIndex:6] == 'P';
            
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            [df setDateFormat:@"yyMMdd"];
            
            NSString *monthChar = [self charforMonth:[df dateFromString:date] isPut:_isPut];
            date = [NSString stringWithFormat:@"%@%@%@", [date substringToIndex:2], monthChar, [date substringWithRange:NSMakeRange(date.length-2, 2)]];
        }
        else
        {            
            //standard MSFT 1319A24.0
            date = [middle substringToIndex:5];
            strike = [middle substringFromIndex:5];
            
            NSString *lastChar = [date substringWithRange:NSMakeRange(date.length-1, 1)];
            date = [NSString stringWithFormat:@"%@%@%@", [date substringToIndex:2], lastChar, [date substringWithRange:NSMakeRange(date.length-3, 2)]];
        }
        
        if (strike.floatValue == strike.integerValue) {
            strike = [NSString stringWithFormat:@"%d.0", strike.integerValue];
        }
        
        NSString *primarySymbol = [NSString stringWithFormat:@"O:%@\\%@\\%@", root, date, strike];
        
        return primarySymbol;
    }
    else return symbol;
}

- (NSString *)charforMonth:(NSDate *)date isPut:(BOOL)_isPut
{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSMonthCalendarUnit fromDate:date];
    NSInteger month = [components month];
    
    char ch = 64+month;
    
    if (_isPut)
        ch += 12;

    return  [NSString stringWithFormat:@"%c", ch];
}

- (NSString*)displaySymbolForPrimaryOption:(NSString*)symbol
{
    if ([self optionTypeOfSymbol:symbol] == PRIMARY)        //O:MSFT\13K16\21.0
    {        
        NSString *root = [symbol componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":\\"]][1];
        NSString *middle = [symbol componentsSeparatedByString:@"\\"][1];
        NSString *strike = [symbol componentsSeparatedByString:@"\\"][2];
        
        const char *chars = middle.UTF8String;

        if (!IS_DISPLAY_TYPE_ALTERNATE)
        {
            //make standard MSFT 1316K21.0
            middle = [NSString stringWithFormat:@"%c%c%c%c%c", chars[0], chars[1], chars[3], chars[4], chars[2]];
        }
        else
        {
            //make alernate MSFT 131116C21.0
            int diff = chars[2] - 64;
            BOOL _isPut = diff>12;

            if (_isPut) diff-=12;

            NSString *monthChar = [NSString stringWithFormat:@"%02d",diff];
            
            middle = [NSString stringWithFormat:@"%c%c%@%c%c%@", chars[0], chars[1], monthChar, chars[3], chars[4], _isPut?@"P":@"C"];
        }
        
        NSString *symbolToDisplay = [NSString stringWithFormat:@"%@ %@%@", root, middle, strike];

        return symbolToDisplay;
    }
    else return symbol;
}

- (OptionType)optionTypeOfSymbol:(NSString*)symbol
{
    //PRIMARY  O:MSFT\13K16\21.0
    //standard APV 1320B130.0     //GE 1321I15.0
    //alernate APV 130220C130.0
    
    NSString *primaryFormat = @"O:[\\S]+\\\\[0-9]{2}[A-X][0-3][0-9]\\\\[0-9]+\\.[0-9]*";
    NSString *standardFormat = @"[\\S]+[ ][0-9]{2}[0-3][0-9][A-X][0-9]+[\\.]*[0-9]*";
    NSString *alternateFormat = @"[\\S]+[ ][0-9]{2}[0-1][0-9][0-3][0-9][C|P][0-9]+\\.[0-9]*";
    
    NSPredicate *primaryRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", primaryFormat];
    NSPredicate *standardRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", standardFormat];
    NSPredicate *alternateRegex = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alternateFormat];
    
    if ([primaryRegex evaluateWithObject:symbol]) {
        return PRIMARY;
    }
    else if ([standardRegex evaluateWithObject:symbol]) {
        return STANDARD;
    }
    else if ([alternateRegex evaluateWithObject:symbol]) {
        return ALTERNATE;
    }
    else return INVALID;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UIAlertView *alert = (UIAlertView *)textField.superview;
    
    [self alertView:alert clickedButtonAtIndex:1];
    [alert dismissWithClickedButtonIndex:1 animated:YES];
    
    return YES;
}

#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tickerList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return SECTION_HEADER_HEIGHT;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (!_tableHeader)
    {
        _tableHeader = [[TickerTableHeader alloc] initWithFrame:CGRectMake(0, 0,
                                                                           tableView.frame.size.width, SECTION_HEADER_HEIGHT)];
        _tableHeader.delegate = self;
    }
    
    static NSDictionary *_formattedKeys;
    
    if (!_formattedKeys)
    {
        _formattedKeys = [[NSDictionary alloc] initWithObjectsAndKeys:
                          @"Ask", @"ask",
                          @"Ask Ex", @"askExchg",
                          @"Ask Size", @"askSize",
                          @"Bid", @"bid",
                          @"Bid Ex", @"bidExchg",
                          @"Bid Size", @"bidSize",
                          @"Chg", @"chg",
                          @"Exchg", @"exchg",
                          @"High", @"high",
                          @"Last", @"last",
                          @"Low", @"low",
                          @"LTT", @"ltt",
                          @"LTV", @"ltv",
                          @"Open", @"open",
                          @"%Chg", @"perChg",
                          @"PC", @"lastClosed",
                          @"Settlement Price", @"settlementPrice",
                          @"Symbol", @"ticker",
                          @"Symbol", @"tickerNameByUser",
                          @"Vol ", @"vol",
                          @"VWAP", @"vwap",
                          @"N", @"ni",
                          @"H", @"isHalt",
                          @"S", @"s",
                          nil];
    }
    _tableHeader.formattedKeys = _formattedKeys;
    [_tableHeader headerViewWithColumns:_columns];
    
    return _tableHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TICKER_TABLE_ROW_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    TickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        CGRect cellFrame = CGRectMake(0, 0, tableView.frame.size.width, TICKER_TABLE_ROW_HEIGHT);
        cell = [[TickerTableViewCell alloc] initWithFrame:cellFrame];
        cell.delegate = self;
    }
    
    UIImageView *bgColorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"activeBg.png"]];
    cell.selectedBackgroundView = bgColorView;

    Ticker *ticker = [self returnTickerForSymbol:_tickerList[indexPath.row]];
//    cell.textLabel.font = [UIFont fontWithName:@"Times New Roman" size:14];

    cell.isRead = [_readNewsArray containsObject:ticker.ni];
    [cell loadCellWithTicker:ticker andColumns:_columns];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIColor *oddBgColor = [UIColor colorWithRed:31.0/255.0 green:41.0/255.0 blue:53.0/255.0 alpha:1.0];
    UIColor *evenBgColor = [UIColor colorWithRed:10.0/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1.0];
    cell.backgroundColor = (indexPath.row%2) ? evenBgColor : oddBgColor;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark UITableView Delegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView isEditing];
}

- (NSString*)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [NSString stringWithFormat:@"Delete %@",_tickerList[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (_delegate != nil && [_delegate respondsToSelector:@selector(deleteTickerAtIndexPath:)])
        {
            [_delegate deleteTickerAtIndexPath:indexPath];
        }
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    
    NSString *mmIndex;
    if (_delegate != nil && [_delegate respondsToSelector:@selector(marketMonitorIndex)])
    {
        mmIndex = [_delegate marketMonitorIndex];
    }
    
    NSString *gwIndex;
    if (_delegate != nil && [_delegate respondsToSelector:@selector(greatWallIndex)])
    {
        gwIndex = [_delegate greatWallIndex];
    }
    
    NSString *tableIndex;
    if (_delegate != nil && [_delegate respondsToSelector:@selector(tableIndex)])
    {
        tableIndex = [_delegate tableIndex];
    }
    
    NSString *moveURL = [[NSString alloc] initWithFormat:
                         LAYOUT_UPDATE_TICKER_MOVE_URL,
                         currentUserID,
                         mmIndex,
                         gwIndex,
                         tableIndex,
                         sourceIndexPath.row,
                         destinationIndexPath.row];
    
    NSDictionary *response = [ProjectHandler dataFromAPIWithStringURL:moveURL];
    
    if (response && [response[@"Status"] isEqualToString:@"SUCCESS"])
    {
        //update local data, after row relocation
        NSString *stringToMove = _tickerList[sourceIndexPath.row];
        [_tickerList removeObjectAtIndex:sourceIndexPath.row];
        [_tickerList insertObject:stringToMove atIndex:destinationIndexPath.row];
        
        Ticker *tickerToMove = _tickerObjects[sourceIndexPath.row];
        [_tickerObjects removeObjectAtIndex:sourceIndexPath.row];
        [_tickerObjects insertObject:tickerToMove atIndex:destinationIndexPath.row];
        
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Update Failure" message:@"Could not move ticker. Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    
    [self reloadTable];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _wallTable.selectedTickerIndex = indexPath.row;
    
    if (_delegate != nil && [_delegate respondsToSelector:@selector(didSelectTicker:)])
    {
        [_delegate didSelectTicker:_tickerObjects[indexPath.row]];
    }
}

#pragma mark - TickerTableViewCell
- (void)companyNewsForNewsID:(NSString*)newsID
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(loadNewsForNewsID:)])
    {
        [_delegate loadNewsForNewsID:[NSString stringWithFormat:@"%@", newsID]];
    }
}

#pragma mark - User Methods
- (void)modifyHeaderColumns
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(modifyColumns)])
    {
        [_delegate modifyColumns];
    }
}

- (void)reloadTable
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(columnArray)])
    {
        _columns = [_delegate columnArray];
    }
    
    [self.tickerTable reloadData];
    [self selectRow:_wallTable.selectedTickerIndex];
}

- (void)selectRow:(NSInteger)row
{
    if (row<_tickerList.count) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [self.tickerTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

- (void)didSelectRow:(NSInteger)row
{
    if (row<_tickerList.count) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        [self tableView:self.tickerTable didSelectRowAtIndexPath:indexPath];
        [self.tickerTable selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

- (BOOL)isEditing
{
    return [self.tickerTable isEditing];
}

- (void)setEditing:(BOOL)editing
{
    [self.tickerTable setEditing:editing animated:YES];
}

- (void)deleteRowAtIndexPath:(NSIndexPath*)indexPath;
{
    [_tickerList removeObjectAtIndex:indexPath.row];
    [self.tickerTable beginUpdates];
    [self.tickerTable deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tickerTable endUpdates];
}

- (void)insertTicker:(Ticker*)ticker atIndexPath:(NSIndexPath*)indexPath
{
    [_tickerList addObject:ticker.ticker];
    
    [_tickerObjects addObject:ticker];
    
    [self.tickerTable beginUpdates];
    [self.tickerTable insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tickerTable endUpdates];
    
    [self.tickerTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [self updateTickersFormat];

    [self hitSnapQuoteForSymbol:ticker.ticker];
}

- (void)saveLogWithText:(NSString*)appendText //keep logs of hitting SNAP QUOTE API
{
    //save log to APIlogfile.txt file
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"SNAPlogfile.txt"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]){
        [[NSData data] writeToFile:path atomically:YES];
    }

    // append to APIlogfile.txt
    NSFileHandle *handle = [NSFileHandle fileHandleForWritingAtPath:path];
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    
//    NSDateFormatter *df = [[NSDateFormatter alloc] init];
//    [df setLocale:[NSLocale currentLocale]];
//    [df setDateStyle:NSDateFormatterFullStyle];
//    [df setTimeStyle:NSDateFormatterLongStyle];
//    NSString *now = [df stringFromDate:[NSDate date]];
    
    [handle writeData:[[NSString stringWithFormat:@"%@\n\n**********************************************************************************************************\n\n", appendText]
                       dataUsingEncoding:NSUTF8StringEncoding]];
}

@end
