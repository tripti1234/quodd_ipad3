//
//  LoginViewController.m
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 2/22/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "LoginViewController.h"
#import "ForgotPasswordVC.h"

#define KEYBOARD_HEIGHT 180

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) //1

@interface LoginViewController ()

@property (nonatomic, retain) CustomAlertView *loginHUD;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [Flurry logEvent:@"Login View"];

    loginButton.userInteractionEnabled = YES;
    
    NSDictionary *savedData = [ProjectHandler getDictionaryFromPList:@"userDetails"];
    
    if ([savedData valueForKey:@"username"]) {
        usernameField.text = [savedData valueForKey:@"username"];
        passwordField.text = [savedData valueForKey:@"password"];
        
        ifRemember=NO;
        [self rememberMeCheck];
    }
    else
    {
        usernameField.text = @"";
        passwordField.text = @"";
    }
}

#pragma mark - UITextField
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    if (textField == usernameField) {
        [passwordField becomeFirstResponder];
    }
    else if (textField == passwordField)
        [self loginButtonPressed];
    
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self shiftViewTo:KEYBOARD_HEIGHT-textField.frame.origin.y animated:YES];
}

- (void)shiftViewTo:(CGFloat)yCoordinate animated:(BOOL)animated
{
    return;
    
    [UIView animateWithDuration:animated ? 0.3 : 0.0
                     animations:^{
                         
                         if (self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
                             [self.view setFrame:CGRectMake(-yCoordinate, 0, self.view.frame.size.width, self.view.frame.size.height)];
                         }
                         else [self.view setFrame:CGRectMake(yCoordinate, 0, self.view.frame.size.width, self.view.frame.size.height)];
                     }];
}

#pragma mark - User Interactions
- (IBAction)backgroundTouched
{
    [usernameField resignFirstResponder];
    [passwordField resignFirstResponder];

    [self shiftViewTo:20 animated:YES];
}

- (IBAction)rememberMeCheck
{
    ifRemember = !ifRemember;
    
    NSString *imageName = ifRemember ? @"CheckedBox.png" : @"UnchekedBox.png";
    
    [rememberMeButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
}

- (IBAction)forgotPassword
{
    ForgotPasswordVC *fvc = [[ForgotPasswordVC alloc] initWithNibName:@"ForgotPasswordVC" bundle:nil];
    fvc.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:fvc animated:YES completion:nil];
}

//-(NSString*)sha256HashFor:(NSString*)input
//{
//    const char* str = [input UTF8String];
//    unsigned char result[CC_SHA256_DIGEST_LENGTH];
//    CC_SHA256(str, strlen(str), result);
//
//    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
//    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
//    {
//        [ret appendFormat:@"%02x",result[i]];
//    }
//    return ret;
//}

- (IBAction)loginButtonPressed
{
    if (usernameField.text.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Please Enter Username" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else if (passwordField.text.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Please Enter Password" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else
    {
        [self startAPI];
        [self getLoginResponse];
    }
}

- (void)startAPI
{
    loginButton.userInteractionEnabled = NO;
    _loginHUD = [ProjectHandler showHUDWithTitle:@"Logging in.."];
    [self backgroundTouched];
}

- (void)getLoginResponse
{
    NSString *loginURL = [NSString stringWithFormat:LOGIN_URL, usernameField.text, passwordField.text];    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:loginURL];

    AFJSONRequestOperation *loginOperation = [AFJSONRequestOperation
                                              JSONRequestOperationWithRequest: request
                                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                              {
                                                  loginResponse = [[NSMutableDictionary alloc] initWithDictionary:JSON];
                                                  
                                                  if (loginResponse.count>0)
                                                  {
                                                      if ([loginResponse[@"isLogin"] boolValue])
                                                      {
                                                          [ProjectHandler addDictionary:nil toPListWithName:@"userDetails"]; //delete saved entries
                                                          
                                                          if (ifRemember)
                                                          {
                                                              [loginResponse setValue:usernameField.text forKey:@"username"];
                                                              [loginResponse setValue:passwordField.text forKey:@"password"];
                                                              
                                                              [ProjectHandler addDictionary:loginResponse toPListWithName:@"userDetails"];
                                                          }
                                                          
                                                          currentUserID = [loginResponse valueForKey:@"userID"];
                                                          currentSessionID = [loginResponse valueForKey:@"sessionID"];
                                                          currentUserName = usernameField.text;
                                                          
                                                          [self fetchUserEntitlements];
                                                      }
                                                      else
                                                      {
                                                          [[[UIAlertView alloc] initWithTitle:@"Login Failure"
                                                                                      message:@"Incorrent username or password"
                                                                                     delegate:nil
                                                                            cancelButtonTitle:@"OK"
                                                                            otherButtonTitles:nil] show];
                                                      }
                                                  }
                                                  
                                                  [ProjectHandler hideHUD:_loginHUD];
                                                  loginButton.userInteractionEnabled = YES;
                                              }
                                              failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                              {
                                                  [ProjectHandler hideHUD:_loginHUD];
                                                  loginButton.userInteractionEnabled = YES;

                                                  [[[UIAlertView alloc] initWithTitle:@"Login Failure"
                                                                              message:@"Please check your network connection and try again."
                                                                             delegate:nil
                                                                    cancelButtonTitle:@"OK"
                                                                    otherButtonTitles:nil] show];
                                              }];
    [loginOperation start];
}

- (void)fetchUserEntitlements
{
    NSString *entitlementsURL = [NSString stringWithFormat:ENTITLEMENTS_URL,usernameField.text];
    NSDictionary *entitlementsResponse = [ProjectHandler dataFromAPIWithStringURL:entitlementsURL];
    
    [ProjectHandler addDictionary:entitlementsResponse toPListWithName:@"entitlements"];

    if (entitlementsResponse.count>0)
    {
        BOOL iPadEntitled = [entitlementsResponse[@"exchangeHash"][@"ipad_access"][kAuthorized] isEqualToString:kAuthorized];
        
        if (iPadEntitled) {
            [NSThread detachNewThreadSelector:@selector(fetchFuturesMappingAndVolumeTickers) toTarget:self withObject:nil];
            [self performSelectorOnMainThread:@selector(switchToMasterView) withObject:nil waitUntilDone:YES];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"Quodd is not accessible on iPad for this user. Please contact Quodd support for details." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else
        [[[UIAlertView alloc] initWithTitle:@"Could not get User Entitlements" message:@"Please try again." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)switchToMasterView
{
    _masterVC = [[MasterViewController alloc] initWithNibName:@"MasterViewController" bundle:nil];
    [self presentViewController:_masterVC animated:YES completion:nil];
}

- (void)fetchFuturesMappingAndVolumeTickers
{
    NSDictionary *futures = [ProjectHandler dataFromAPIWithStringURL:FUTURES_MAPPING_URL];
    [ProjectHandler addDictionary:futures toPListWithName:@"futuresMapping"];

    
    NSArray *volumeTickers = [ProjectHandler dataFromAPIWithStringURL:VOLUME_TICKERS_LIST_URL];
    NSDictionary *volDict = [NSDictionary dictionaryWithObjectsAndKeys:volumeTickers, @"volumeTickerList", nil];
    [ProjectHandler addDictionary:volDict toPListWithName:@"volumeTickerList"];
}

@end