//
//  TickerTableHeader.h
//  Quodd_iPad2
//
//  Created by Nishant on 09/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TickerTableHeaderDelegate <NSObject>

- (void)modifyHeaderColumns;

@end

@interface TickerTableHeader : UIView

@property(unsafe_unretained, nonatomic) id<TickerTableHeaderDelegate> delegate;
@property (strong, nonatomic) NSDictionary *formattedKeys;

- (void)headerViewWithColumns:(NSArray*)columns;

@end
