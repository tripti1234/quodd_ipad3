//
//  OptionsPlusViewController.m
//  Quodd_iPad
//
//  Created by Nishant Anthwal on 2/21/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "OptionsPlusViewController.h"

#import "ModifyColumnsVC.h"
#import "TickerTableViewCell.h"

#import "Option.h"
#import "Ticker.h"

#import "SettingsViewController.h"

#define TICKER_TABLE_ROW_HEIGHT 39
#define SECTION_HEADER_HEIGHT 30

#define checkedImageName @"CheckedBox.png"
#define unCheckedImageName @"UnchekedBox.png"

@interface OptionsPlusViewController ()

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;

@property (nonatomic, retain) TickerTableHeader *tableHeader;

@property (strong, nonatomic) Option *callOption;
@property (strong, nonatomic) Option *putOption;
@property (strong, nonatomic) NSArray *expiryDateList;

@property (strong, nonatomic) NSTimer *snapQuoteAPIHitTimer;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *monthLabel;

@property (strong, nonatomic) IBOutlet UIView *menuView;
@property (strong, nonatomic) IBOutlet UITextField *tickerTextField;
@property (strong, nonatomic) IBOutlet UIPickerView *monthPicker;
@property (strong, nonatomic) IBOutlet UIButton *putCheckBoxButton;
@property (strong, nonatomic) IBOutlet UIButton *callCheckBoxButton;

@property (strong, nonatomic) Ticker *rootTicker;

@end

@implementation OptionsPlusViewController

static NSDictionary* formattedKeys ;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"MainBg.png"]];

    _rootTicker = [[Ticker alloc] init];

    if (!formattedKeys)
    {
        formattedKeys = [[NSDictionary alloc] initWithObjectsAndKeys:
                         @"Symbol", @"tickerNameByUser",
                         @"T", @"T",
                         @"Last", @"last",
                         @"L", @"marketCenter",
                         @"Chg", @"chg",
                         @"Bid", @"bid",
                         @"B", @"bidExchg",
                         @"Ask", @"ask",
                         @"A", @"askExchg",
                         @"Y/M/S", @"yms",
                         @"%Chg", @"perChg",
                         @"Open", @"open",
                         @"High", @"high",
                         @"Low", @"low",
                         @"LTV", @"ltv",
                         @"Vol", @"vol",
                         @"S", @"s",
                         @"Time", @"time",
                         @"Strike", @"strike",
                         @"OI", @"oI",
                         @"Delta", @"delta",
                         @"Gamma", @"gamma",
                         @"Theta", @"theta",
                         @"Int.V", @"intV",
                         @"Time.V", @"timeV",
                         @"Theo.V", @"theoV",
                         @"Dif", @"dif",
                         @"I.Vol", @"iVol",
                         @"Exp.", @"exp",
                         @"Days", @"days",
                         @"*C/P", @"cpp",
                         nil];
    }
    
    _expiryDateList = @[];
    
    [_putCheckBoxButton setImage:[UIImage imageNamed: unCheckedImageName]
                        forState:UIControlStateNormal];
    [_putCheckBoxButton setImage:[UIImage imageNamed: checkedImageName]
                        forState:UIControlStateSelected];
    
    [_callCheckBoxButton setImage:[UIImage imageNamed: unCheckedImageName]
                         forState:UIControlStateNormal];
    [_callCheckBoxButton setImage:[UIImage imageNamed: checkedImageName]
                         forState:UIControlStateSelected];
    
    
    [_segmentControl setBackgroundImage:[UIImage imageNamed:@"ControlBg.png"]  forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [_segmentControl setBackgroundImage:[UIImage imageNamed:@"ControlBg-ActiveLeft.png"]  forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _monthPicker.hidden = YES;

    if (_root) {
        BOOL isOptionSymbol = [_root hasPrefix:@"O:"];

        if (isOptionSymbol) {
            _root = [_root componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@":\\"]][1];
        }
        
        _tickerTextField.text = _root;
        [self goMenuButtonPressed:nil];
        
        [Flurry logEvent:@"Options View" withParameters:@{@"Root Ticker": _root}];

    }
    else {
        
        _tickerTextField.text = @"";
    }
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setSegmentControl:nil];
    [self setMenuView:nil];
    [self setTickerTextField:nil];
    [self setPutCheckBoxButton:nil];
    [self setCallCheckBoxButton:nil];
    [self setMonthPicker:nil];
    [self setMonthLabel:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (UIInterfaceOrientationIsLandscape(toInterfaceOrientation));
}

#pragma mark - IBActions

- (IBAction)backPressed
{
    [self stopTimer];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backgroundTouched
{    
    [_tickerTextField resignFirstResponder];
}

- (IBAction)menuButtonPressed
{
    [_tickerTextField resignFirstResponder];

    CGRect currentFrame = _menuView.frame;
    CGFloat finalX;
    
    if (_menuView.frame.origin.x <0)
    {        
        [_menuToggleButton setImage:[UIImage imageNamed:@"ArowTop-Left.png"] forState:UIControlStateNormal];
        
        finalX = 0;
        [_monthPicker reloadAllComponents];
        
        if (!(_putCheckBoxButton.isSelected || _callCheckBoxButton.isSelected))
        {
            _putCheckBoxButton.selected = YES;
            _callCheckBoxButton.selected = YES;
        }
    }
    else
    {
        [_menuToggleButton setImage:[UIImage imageNamed:@"ArowTop-Right.png"] forState:UIControlStateNormal];

        finalX = -currentFrame.size.width;
    }
    
    CGRect finalFrame = CGRectMake(finalX,
                                   currentFrame.origin.y,
                                   currentFrame.size.width,
                                   currentFrame.size.height);
    
    [UIView beginAnimations:@"MenuAnimation" context:NULL];
    [UIView setAnimationDuration:0.3];
    _menuView.frame = finalFrame;
    [UIView commitAnimations];
}

- (IBAction)segmentControlChanged:(id)sender
{
    NSString *segmentImage = [NSString stringWithFormat:@"ControlBg-Active%@.png", _segmentControl.selectedSegmentIndex ? @"Right" : @"Left"];
    
    [_segmentControl setBackgroundImage:[UIImage imageNamed:segmentImage]  forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];

    [_tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  _segmentControl.selectedSegmentIndex ?
            _putOption.optionItemArray.count :
            _callOption.optionItemArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        static NSString *CellIdentifier = @"Cell";
                
        TickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            CGRect cellFrame = CGRectMake(0, 0, tableView.frame.size.width, TICKER_TABLE_ROW_HEIGHT);
            cell = [[TickerTableViewCell alloc] initWithFrame:cellFrame];
        }
        
        Ticker *ticker = _segmentControl.selectedSegmentIndex ?
        (Ticker *)_putOption.optionItemArray[indexPath.row]:
        (Ticker *)_callOption.optionItemArray[indexPath.row];
                
        [cell loadCellWithTicker:ticker andColumns:_columnArray];
        
        UIColor *oddBgColor = [UIColor colorWithRed:31.0/255.0 green:41.0/255.0 blue:53.0/255.0 alpha:1.0];
        UIColor *evenBgColor = [UIColor colorWithRed:10.0/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1.0];
        cell.contentView.backgroundColor = (indexPath.row%2) ? evenBgColor : oddBgColor;

        UIImageView *bgColorView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fundaBg-active"]];
        cell.selectedBackgroundView = bgColorView;
        
        return cell;
    }
    @catch (NSException *exception)
    {
        //NSLog(@"EXCEPTION CAUGHT in %s\n%@",__PRETTY_FUNCTION__,exception.description);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return SECTION_HEADER_HEIGHT;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (!_tableHeader)
    {
        _tableHeader = [[TickerTableHeader alloc] initWithFrame:CGRectMake(0,
                                                                           0,
                                                                           tableView.frame.size.width,
                                                                           SECTION_HEADER_HEIGHT)];
        _tableHeader.delegate = self;
    }
    
    _tableHeader.formattedKeys = formattedKeys;
    [_tableHeader headerViewWithColumns:_columnArray];
    
    return _tableHeader;
}

#pragma mark - ------------ Header View Delegate Methods ------------

- (void) modifyHeaderColumns
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setOptionsColumns:)
                                                 name:kReloadDataInTable
                                               object:nil];
    
    ModifyColumnsVC *_modifyColVC = [[ModifyColumnsVC alloc] initWithNibName:@"ModifyColumnsVC" bundle:nil];
    _modifyColVC.formattedKeys = formattedKeys;
    _modifyColVC.selectedColumns = [[NSMutableArray alloc] initWithArray:_columnArray];

    _modifyColVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:_modifyColVC
                       animated:YES
                     completion:nil];
}

- (void)setOptionsColumns:(NSNotification*)notif
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kReloadDataInTable
                                                  object:nil];
    
    NSArray *newColumns = [NSArray arrayWithArray:notif.userInfo[@"newColumns"]];

    NSString *columnsUpdateURL = [NSString stringWithFormat:COLUMNS_UPDATE_URL, currentUserID, @"OPTIONS", [newColumns componentsJoinedByString:@","]];
    
    if ([self isLayoutUpdateSuccessForURL:columnsUpdateURL])
    {
        _columnArray = [[NSArray alloc] initWithArray:newColumns];
        [_tableView reloadData];
    }
}

- (BOOL)isLayoutUpdateSuccessForURL:(NSString*)url
{
    NSDictionary *response = [ProjectHandler dataFromAPIWithStringURL:url];
    
    if (response!=nil && [response[@"Status"] isEqualToString:@"SUCCESS"])
    {
        return YES;
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Update Failure!" message:@"Could not columns layout. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return NO;
    }
}


#pragma mark - ------------ Menu View Buttons ------------

- (IBAction)cancelMenuButtonPressed:(id)sender
{
    [self menuButtonPressed];
}

- (IBAction)goMenuButtonPressed:(id)sender
{
    if (!_tickerTextField.text || _tickerTextField.text.length <1)
    {
        [self showAlertWithTitle:@"Error" message:@"No ticker specified"];
        return;
    }
    _root = [_tickerTextField.text uppercaseString];
    
    [self hitRootSnapQuoteAPI:_root];
    
    [self hitOptionAPI:_root];
    
    if (_menuView.frame.origin.x == 0)
    {
        [self menuButtonPressed];
    }

    if (_callCheckBoxButton.selected || _callCheckBoxButton.selected == _putCheckBoxButton.selected)
        _segmentControl.selectedSegmentIndex = 0;
    else
        _segmentControl.selectedSegmentIndex = 1;
    
    [_tableView reloadData];
}

- (IBAction)callPutCBPressed:(id)sender
{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    
    if ([button.titleLabel.text isEqualToString:@"C"] &&
        !_putCheckBoxButton.selected)
    {
        _putCheckBoxButton.selected = YES;
    }
    else if ([button.titleLabel.text isEqualToString:@"P"] &&
             !_callCheckBoxButton.selected)
    {
        _callCheckBoxButton.selected = YES;
    }
}

- (NSUInteger) callPutStatus
{
    NSUInteger status;
    
    if (_callCheckBoxButton.selected == _putCheckBoxButton.selected)
        status = 0;
    else if (_callCheckBoxButton.selected)
        status = 1;
    else
        status=2;
    
    return status;
}


#pragma mark - ------------ Custom Methods ------------

- (void) hitOptionAPI: (NSString *) root
{
    NSUInteger selectedMonth = [_monthPicker selectedRowInComponent:0];
    NSString *date = @"";
    
    if (_expiryDateList.count >= (selectedMonth + 1))
    {
        date = _expiryDateList[selectedMonth];
        _monthLabel.title = date;
    }
    
    NSString *optionsURL = [NSString stringWithFormat:OPTIONS_FULL_URL, root, [self callPutStatus], date];
    
    DLog(@"++++++++++++++++++++++++++");
    DLog(@"Hitting API for Option %@", optionsURL);
    DLog(@"++++++++++++++++++++++++++");
    
    _monthPicker.hidden = YES;
    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:optionsURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSDictionary *result = (NSDictionary *) JSON;

                                             if (result.count)
                                             {
                                                 _expiryDateList = result[@"expiryDateList"];
                                                 
                                                 NSUInteger selectedMonth = [_monthPicker selectedRowInComponent:0];
                                                 if (_expiryDateList.count >= (selectedMonth + 1))
                                                 {
                                                     _monthLabel.title = _expiryDateList[selectedMonth];
                                                 }
                                                 
                                                 _callOption = [[Option alloc] initWithDictionary:result[@"call"]
                                                                                       volatility:result[@"volatility"]];
                                                 
                                                 _putOption = [[Option alloc] initWithDictionary:result[@"put"]
                                                                                      volatility:result[@"volatility"]];
                                                 
                                                 [self hitSnapQuoteAPI];
                                                 
                                                 [self startTimer];
                                                 
                                                 [_tableView reloadData];
                                                 
                                                 [_monthPicker reloadAllComponents];
                                                 _monthPicker.hidden = NO;
                                             }
                                             else [[[UIAlertView alloc] initWithTitle:@"Sorry"
                                                                         message:@"Options data not available for this ticker"
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [[[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"OptionList API for %@",_root]
                                                                         message:error.localizedDescription
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                             
                                             DLog(@"%s %@", __PRETTY_FUNCTION__, [error localizedDescription]);
                                         }];
    [operation start];
}

- (void) hitSnapQuoteAPI
{
    NSMutableArray *symbolArray = [[NSMutableArray alloc] initWithCapacity:(_putOption.optionItemArray.count +
                                                                            _callOption.optionItemArray.count)];
    
    //operation for PUT
    for (Ticker *ticker in _callOption.optionItemArray)
    {
        [symbolArray addObject:ticker.ticker];
    }
    
//    for (Ticker *ticker in _putOption.optionItemArray)
//    {
//        [symbolArray addObject:ticker.ticker];
//    }
    

    NSMutableURLRequest *request = [ProjectHandler urlRequestWithURL:@"http://199.96.250.198/mobile/snapQuote.jsp?"];
    
    NSString *post = [NSString stringWithFormat:@"ticker=%@", [symbolArray componentsJoinedByString:@","]];
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [request setHTTPBody:postData];
    
    AFJSONRequestOperation *callOperation = [AFJSONRequestOperation
                                             JSONRequestOperationWithRequest: request
                                             success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                             {
                                                 [_callOption updateOptionItemArrayWithDictionary:JSON
                                                                                       rootTicker:_rootTicker];
//                                             [_putOption updateOptionItemArrayWithDictionary:JSON
//                                                                                  rootTicker:_rootTicker];
                                                 
                                                 [_tableView reloadData];
                                             }
                                             failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 DLog(@"%s %@",__PRETTY_FUNCTION__, [error localizedDescription]);
                                             }];
    [callOperation start];
    
    
    
    //seperate operation for PUT
    [symbolArray removeAllObjects];
    for (Ticker *ticker in _putOption.optionItemArray)
    {
        [symbolArray addObject:ticker.ticker];
    }
    
    NSMutableURLRequest *putRequest = [ProjectHandler urlRequestWithURL:@"http://199.96.250.198/mobile/snapQuote.jsp?"];

    NSString *post2 = [NSString stringWithFormat:@"ticker=%@", [symbolArray componentsJoinedByString:@","]];
    NSData *putPostData2 = [post2 dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [putRequest setHTTPBody:putPostData2];
    
    AFJSONRequestOperation *putOperation = [AFJSONRequestOperation
                                            JSONRequestOperationWithRequest: putRequest
                                            success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                            {
//                                              [_callOption updateOptionItemArrayWithDictionary:JSON
//                                                                                    rootTicker:_rootTicker];
                                                [_putOption updateOptionItemArrayWithDictionary:JSON
                                                                                     rootTicker:_rootTicker];
                                                
                                                [_tableView reloadData];
                                            }
                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                            {
                                                DLog(@"%s %@",__PRETTY_FUNCTION__, [error localizedDescription]);
                                            }];
    
    [putOperation start];
    
}

- (void) hitRootSnapQuoteAPI: (NSString *) root   //SNAP Data for root ticker
{
    NSString *url = [NSString stringWithFormat:SNAP_QUOTE_URL, isNASDAQBasicEntitled ? @"1" : @"0", root];
    
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:url];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSDictionary *dict = (NSDictionary *) JSON;
                                             [_rootTicker loadWithTicker:dict[root]
                                                               forSymbol:root];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [[[UIAlertView alloc] initWithTitle:@"Error Retrieving Options root"
                                                                         message:error.localizedDescription.capitalizedString
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                             
                                             DLog(@"[%@ %@] %@ (%@)",
                                                  NSStringFromClass([self class]),
                                                  NSStringFromSelector(_cmd),
                                                  [error localizedDescription],
                                                  [error localizedFailureReason]);
                                         }];
    [operation start];
}

- (void) startTimer
{
    [_snapQuoteAPIHitTimer invalidate];
    
    _snapQuoteAPIHitTimer = [NSTimer scheduledTimerWithTimeInterval:30.0
                                                             target:self
                                                           selector:@selector(updateData)
                                                           userInfo:nil
                                                            repeats:YES];
    
    [[NSRunLoop mainRunLoop] addTimer:_snapQuoteAPIHitTimer
                              forMode:NSRunLoopCommonModes];
    
}

- (void) stopTimer
{
    [_snapQuoteAPIHitTimer invalidate];
}

- (void) updateData
{
    [self hitRootSnapQuoteAPI:_root];
    [self hitSnapQuoteAPI];
}


#pragma mark - ------------ Utility Methods ------------

- (void) showAlertWithTitle: (NSString *) title message: (NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
}


#pragma mark - UIPickerView Delegate Methods

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _expiryDateList[row];
}

#pragma mark - UIPickerView Data Source Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _expiryDateList.count;
}

#pragma mark - UITextField Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self goMenuButtonPressed:nil];
    return YES;
}

@end
