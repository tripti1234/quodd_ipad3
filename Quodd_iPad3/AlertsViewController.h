//
//  AlertsViewController.h
//  Quodd_iPad2
//
//  Created by Nishant on 01/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *loadAlerts;
 
@property (nonatomic, retain) IBOutlet UIButton *selectAlert;
@property (nonatomic, retain) IBOutlet UIButton *gotoAlertSummary;
@property (nonatomic, retain) IBOutlet UIButton *selectVolume;
@property (nonatomic, retain) IBOutlet UIButton *selectTime;
@property (nonatomic, retain) IBOutlet UIButton *alertTypeWindow;
@property (nonatomic, retain) IBOutlet UIButton *alertTypeAudible;
@property (nonatomic, retain) IBOutlet UIButton *alertTypeEmail;

@property (nonatomic, retain) IBOutlet UIButton *deleteAlertButton;

@property (nonatomic, retain) IBOutlet UILabel *charsLeft;

@property (nonatomic, retain) IBOutlet UITextField *tickerName;
@property (nonatomic, retain) IBOutlet UITextField *alertName;
@property (nonatomic, retain) IBOutlet UITextField *comments;

@property (nonatomic, retain) IBOutlet UITextField *volume;

@property (nonatomic, retain) IBOutlet UIViewController *timePicker;
@property (nonatomic, retain) IBOutlet UILabel *timeLabel;


- (IBAction)backPressed;
- (IBAction)backgroundTouched;
- (IBAction)showAlertSummaryVC;

- (IBAction)typeSelected:(UIButton*)sender;
- (IBAction)clearAll;
- (IBAction)showPopOver:(UIButton*)senderButton;
- (IBAction)showTimePicker:(UIButton*)senderButton;
- (IBAction)clearTime;
- (IBAction)dismissTimePicker;

- (IBAction)saveAlert;
- (IBAction)deleteAlert;

- (IBAction)textDidChange:(UITextField *)textField;

@end
