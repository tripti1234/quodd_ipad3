//
//  NewsHeadlineView.h
//  Quood_iPad
//
//  Created by Nishant on 16/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewsHeadlineViewDelegate <NSObject>

- (NSInteger) openSectionIndex;
- (void) sectionClosed : (NSInteger) section;
- (void) sectionOpened : (NSInteger) section;

@end

@interface NewsHeadlineView : UIView

@property (nonatomic, retain) UIButton *discButton;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, weak) id<NewsHeadlineViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame inSection:(NSInteger)sectionNumber WithDictionary:(NSDictionary *)dict andStatus:(BOOL)isRead;

- (void) discButtonPressed : (id) sender;
- (void) toggleButtonPressed : (BOOL) flag isSelected: (BOOL) flag2;

@end