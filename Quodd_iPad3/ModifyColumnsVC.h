//
//  ModifyColumnsVC.h
//  Quodd_iPad2
//
//  Created by Nishant on 27/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModifyColumnsVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableAllColumns;
@property (nonatomic, retain) IBOutlet UITableView *tableSelectedColumns;

@property (nonatomic, retain) NSMutableArray *selectedColumns;
@property (nonatomic, retain) NSDictionary *formattedKeys;

- (IBAction)closeView;
- (IBAction)done;

@end
