//
//  SingleNewsVC.h
//  Quodd_iPad3
//
//  Created by Nishant on 02/08/13.
//  Copyright (c) 2013 Paxcel Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol SingleNewsVCDelegate <NSObject>
//
//- (void)reloadMasterTable;
//
//@end

@interface SingleNewsVC : UIViewController<UIWebViewDelegate>

@property (nonatomic, retain) IBOutlet UIWebView *webViewSingleNews;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *newsIndicator;

@property (nonatomic, retain) NSString *newsURL;

- (IBAction)close;

@end
