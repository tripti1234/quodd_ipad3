//
//  HGMessageHUD.h
//  HGMessageHUD
//
//  Created by Paxcel Technologies on 27/12/12.
//  Copyright (c) 2012 Paxcel Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HGMessageHUD : UILabel

+ (void) showInView : (UIView *) view withMessage: (NSString *) message;

@end
