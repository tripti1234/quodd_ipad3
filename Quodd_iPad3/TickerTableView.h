//
//  TickerTableView.h
//  Quodd_iPad
//
//  Created by Nishant on 03/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WallTable.h"
#import "Ticker.h"
#import "TickerTableHeader.h"
#import "TickerTableViewCell.h"
 
@protocol TickerTableViewDelegate <NSObject>

- (void)didSelectTicker:(Ticker*)ticker;
- (void)deleteTickerAtIndexPath:(NSIndexPath*)indexPath;
- (void)modifyColumns;
- (void)loadCompanyNewsForTicker:(NSString*)symbol;
- (void)loadNewsForNewsID:(NSString*)newsID;

- (NSArray*)columnArray;

- (NSString*)marketMonitorIndex;
- (NSString*)greatWallIndex;
- (NSString*)tableIndex;

@end

@interface TickerTableView : UIView <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UITextFieldDelegate, TickerTableHeaderDelegate, TickerTableViewCellDelegate>

@property(unsafe_unretained, nonatomic) id<TickerTableViewDelegate> delegate;

@property (nonatomic, retain) IBOutlet UITableView *tickerTable;
@property (nonatomic, retain) NSMutableArray *tickerList;
@property (nonatomic, retain) WallTable *wallTable;
@property (nonatomic, retain) UILongPressGestureRecognizer *longPress;

@property (nonatomic, strong) NSArray *readNewsArray;

- (void)configureWallTableWithColumns:(NSArray*)columns;
- (void)updateTickersFormat;
- (void)getSnapAPI:(NSTimer*)snapTimer;

- (void)didSelectRow:(NSInteger)row;
- (void)selectRow:(NSInteger)row;
- (void)reloadTable;

- (NSString*)selectedTickerSymbol;

- (BOOL)isEditing;
- (void)setEditing:(BOOL)editing;
- (void)deleteRowAtIndexPath:(NSIndexPath*)indexPath;
- (void)insertTicker:(Ticker*)ticker atIndexPath:(NSIndexPath*)indexPath;

@end
