//
//  TickerLookupCell.m
//  Quodd_iPad
//
//  Created by Nishant on 28/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "TickerLookupCell.h"

@implementation TickerLookupCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)loadWithDictionary:(NSDictionary*)dict
{
    _arrayOfKeys = @[@"Ticker", @"Company Name", @"Exchange", @"Type", @"CURRENCY", @"CUSIP"];
    
    [_ticker setTitle:dict[@"ticker"] forState:UIControlStateNormal];
    [_company setTitle:dict[@"name"] forState:UIControlStateNormal];
    [_exchg setTitle:dict[@"exchg"] forState:UIControlStateNormal];
    [_type setTitle:dict[@"type"] forState:UIControlStateNormal];
    
    if ([dict.allKeys containsObject:@"currency"]) {
        [_currency setTitle:dict[@"currency"] forState:UIControlStateNormal];
    }
    else [_currency setTitle:@"" forState:UIControlStateNormal];
    
    [_cusip setTitle:dict[@"cusip"] forState:UIControlStateNormal];
}

- (IBAction)copyToPasteBoard:(UIButton*)senderButton
{
    if (senderButton.titleLabel.text.length>0)
    {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = senderButton.titleLabel.text;
        
        _copiedLabel.text = [NSString stringWithFormat:@"%@ copied to Pasteboard.",_arrayOfKeys[senderButton.tag]];
        
         [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.5];
        [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self cache:NO];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(hide)];
        _copiedLabel.hidden = NO;
        [UIView commitAnimations];
    }
}

- (void)hide
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:1.0];
    [UIView setAnimationDuration:.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self cache:NO];
    _copiedLabel.hidden = YES;
    [UIView commitAnimations];
}

- (IBAction)showDetailAlert:(UIButton*)senderButton
{
    if (senderButton.titleLabel.text.length>0)
    {
        [[[UIAlertView alloc] initWithTitle:senderButton.titleLabel.text
                                    message:nil
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}

@end
