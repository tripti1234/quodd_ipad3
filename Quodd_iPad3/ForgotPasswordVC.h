//
//  ForgotPasswordVC.h
//  Quodd_iPad3
//
//  Created by Nishant on 30/07/13.
//  Copyright (c) 2013 Paxcel Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController <UIWebViewDelegate>

@property (nonatomic, retain) IBOutlet UIWebView *resetPassword;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)back;

@end
