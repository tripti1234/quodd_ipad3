//
//  AlertSummaryCell.h
//  Quodd_iPad2
//
//  Created by Nishant on 15/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertSummaryCellDelegate <NSObject>

- (void)checkBoxAtIndex:(NSInteger)index;
- (void)editDeleteAlertAtIndex:(NSInteger)index;

@end

@interface AlertSummaryCell : UITableViewCell

@property(unsafe_unretained, nonatomic) id<AlertSummaryCellDelegate> delegate;

@property (nonatomic, retain) IBOutlet UIButton *checkBox;      //historical
@property (nonatomic, retain) IBOutlet UIButton *ticker;
@property (nonatomic, retain) IBOutlet UIButton *date;           //historical
@property (nonatomic, retain) IBOutlet UIButton *component;
@property (nonatomic, retain) IBOutlet UIButton *condition;
@property (nonatomic, retain) IBOutlet UIButton *value;
@property (nonatomic, retain) IBOutlet UIButton *name;
@property (nonatomic, retain) IBOutlet UIButton *comments;
//@property (nonatomic, retain) IBOutlet UILabel *newsId;
@property (nonatomic, retain) IBOutlet UIButton *editDeleteButton;


- (void)loadCellAtIndex:(NSInteger)index withDictionary:(NSDictionary*)dict selected:(BOOL)selected;

- (IBAction)editDeleteBtn:(UIButton*)senderButton;
- (IBAction)checkBoxPressed:(UIButton*)senderButton;

- (IBAction)showDetailAlert:(UIButton*)senderButton;

@end
