//
//  NewsHeadlineView.m
//  Quood_iPad
//
//  Created by Nishant on 16/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "NewsHeadlineView.h"
#import <QuartzCore/QuartzCore.h>

@implementation NewsHeadlineView

- (id)initWithFrame:(CGRect)frame inSection:(NSInteger)sectionNumber WithDictionary:(NSDictionary *)dict andStatus:(BOOL)isRead
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(discButtonPressed:)];
        [self addGestureRecognizer:tapGesture];
        
        self.userInteractionEnabled = YES;
        self.section = sectionNumber;
        
        CGRect superViewFrame = self.bounds;
        superViewFrame.size.width -= 50;
        CGRectInset(superViewFrame, 0.0, 5.0);
        
        CGFloat buffer = 10;
        
        CGRect labelFrame = CGRectMake(superViewFrame.origin.x + buffer,
                                       superViewFrame.origin.y,
                                       superViewFrame.size.width-2*buffer,
                                       27);

        CGRect subTitleFrame = CGRectMake(labelFrame.origin.x,
                                          labelFrame.size.height,
                                          labelFrame.size.width,
                                          18);
        
        UILabel *labelHeadline = [[UILabel alloc] initWithFrame:labelFrame];
        labelHeadline.text = dict[@"headline"];
        labelHeadline.font = [UIFont boldSystemFontOfSize:15.0];
        labelHeadline.backgroundColor = [UIColor clearColor];
        labelHeadline.textColor = [UIColor whiteColor];
        //label.numberOfLines = 2;
        //[label sizeToFit];
        [self addSubview:labelHeadline];
        
        UILabel *subtitle = [[UILabel alloc] initWithFrame:subTitleFrame];
        subtitle.text = [NSString stringWithFormat:@"%@ - %@",dict[@"source"],dict[@"date"]];
        subtitle.font = [UIFont systemFontOfSize:14.0];
        subtitle.backgroundColor = [UIColor clearColor];
        subtitle.textColor = [UIColor lightGrayColor];
        [self addSubview:subtitle];
        
        CGRect buttonFrame = CGRectMake(superViewFrame.size.width, 10, 20, 30);
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = buttonFrame;
        button.backgroundColor = [UIColor clearColor];
        [button addTarget:self action:@selector(discButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        self.discButton = button; //for open-close status
        
        UIImageView *status = [[UIImageView alloc] initWithFrame:buttonFrame];
        status.image = isRead ? [UIImage imageNamed:@"news-read.png"] : [UIImage imageNamed:@"news-unread.png"];
        status.tag = 11;
        [self addSubview:status];
        
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"news-bg.png"]];
        
    }
    
    return self;
}

- (void) discButtonPressed : (id) sender
{
    NSInteger currentSecIndex = NSNotFound;
    if ([_delegate respondsToSelector:@selector(openSectionIndex)])
    {
            currentSecIndex = [_delegate openSectionIndex];
    }
    
    if (currentSecIndex != NSNotFound && currentSecIndex == _section)
    {
        [self toggleButtonPressed:TRUE isSelected:NO];
    }
    else
    {
        [self toggleButtonPressed:TRUE isSelected:YES];
    }
}

- (void) toggleButtonPressed : (BOOL) flag isSelected: (BOOL) flag2
{
    self.discButton.selected = !self.discButton.selected;
    if(flag)
    {
        if (flag2)
        {
            if ([self.delegate respondsToSelector:@selector(sectionOpened:)])
            {
                [self.delegate sectionOpened:self.section];
            }
        }
        else
        {
            if ([self.delegate respondsToSelector:@selector(sectionClosed:)])
            {
                [self.delegate sectionClosed:self.section];
            }
        }
    }
}

@end
