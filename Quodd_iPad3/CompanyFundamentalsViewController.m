//
//  CompanyFundamentalsViewController.m
//  Quodd_iPad
//
//  Created by Nishant on 10/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "CompanyFundamentalsViewController.h"
#import <QuartzCore/QuartzCore.h>

#define COMP_FUNDA_LIST_TAG 9999

#define TICKER_COUNT    @"6"
#define TICKER_TYPE     @"S"
#define TICKER_ORDERBY  @"Symbol"

@interface CompanyFundamentalsViewController ()

@property (nonatomic,retain) NSDictionary *companyFundaURLs;
@property (nonatomic,retain) NSMutableDictionary *loadedIndices;
//@property (nonatomic,retain) NSMutableArray *tickerLookupResponse;

@property (nonatomic,retain) NSArray *tickerLookupKeys;
@property (nonatomic,retain) NSMutableArray *tickerLookupResults;

@property (nonatomic, retain) NSOperationQueue *queue;

@end

@implementation CompanyFundamentalsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    fundamentalsList.backgroundColor = VIEW_BACKGROUND_IMAGE;
    
//    BOOL dowJonesNewsFlag = [DICT_ENTITLEMENTS[@"dowJonesNewsFlag"] boolValue];
    NSString *newsSource = isDowJonesEntitled ? @"DOWJONES" : @"COMTEX";
    NSString *newsURL = [NSString stringWithFormat:COMPANY_NEWS_URL,@"%@",newsSource];

    NSString *marketGraderURL = [NSString stringWithFormat:MARKETGRADER_EQUITY_URL, @"%@", currentUserName];
    
    _companyFundaURLs = [[NSDictionary alloc] initWithObjectsAndKeys:
                         COMPANY_SNAPSHOT_URL, @"Company Snapshot",
                         newsURL, @"Company News",
                         marketGraderURL, @"MarketGrader Equity Research",
                         PIR_CORPORATE_PROFILE_URL, @"PIR Corporate Profile",
                         ANALYST_COVERAGE_URL, @"Analyst Coverage",
                         EARNINGS_URL, @"Earnings",
                         KEY_STATS_FIN_URL, @"Key Stats & Financials",
                         INSIDER_ACTIVITY_URL, @"Insider Activity",
                         INST_OWNERSHIP_URL, @"Institutional Ownership",
                         SEC_FILING_URL, @"SEC Filings",
                         FIN_HIGHLIGHTS_URL, @"Financial Highlights",
                         ANNUAL_BALSHEET_URL, @"Balance Sheet (Annual)",
                         ANNUAL_INC_URL, @"Income Statement (Annual)",
                         ANNUAL_CASH_FLOW_URL, @"Cash Flow Statement (Annual)",
                         QUARTER_INC_URL, @"Income Statement (Quarterly)",
                         QUARTER_BALSHEET_URL, @"Balance Sheet (Quarterly)",
                         nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (!_tickerSymbol) {
        _tickerSymbol = @"AAPL";
    }
    
    [Flurry logEvent:@"Company Fundamentals View" withParameters:@{@"Symbol": _tickerSymbol}];
    
    [fundamentalsList reloadData];
    
    self.searchDisplayController.searchBar.text = @"";

    [self loadFundaViewForTitle:_selectedTitle];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (IBAction)backPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)backgroundTouched
{
    [self.searchDisplayController setActive:NO animated:YES];
}

- (void)loadFundaViewForTitle:(NSString*)title
{
    @try {
        NSURL *fundaURL = [NSURL URLWithString:[NSString stringWithFormat:_companyFundaURLs[title],_tickerSymbol]];
        [companyFundaWebView loadRequest:[NSURLRequest requestWithURL:fundaURL]];
        
        NSInteger index = [_companyFundaTitles indexOfObject:title];
        
        NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [fundamentalsList reloadRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [fundamentalsList selectRowAtIndexPath:selectedIndexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];
    }
    @catch (NSException *exception) {
        
        NSString *method = [NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__];
        [[[UIAlertView alloc] initWithTitle:method message:exception.description delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [webviewLoadingIndicator startAnimating];
    webView.hidden = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [webviewLoadingIndicator stopAnimating];
    webView.hidden = NO;
    
    static bool flag = YES;
    if (flag) {
        //[webView reload];           //reloading to fix css issue.
        flag=NO;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [webviewLoadingIndicator stopAnimating];
    webView.hidden = NO;
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == COMP_FUNDA_LIST_TAG)
    {
        return _companyFundaTitles.count;
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        return _tickerLookupResults.count;
    }
    else return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return tableView.tag == COMP_FUNDA_LIST_TAG ? tableView.sectionHeaderHeight : 0;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == COMP_FUNDA_LIST_TAG) {
        
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 100)];
        headerLabel.text = [NSString stringWithFormat:@"   %@",_tickerSymbol];
        headerLabel.backgroundColor = [UIColor clearColor];
        headerLabel.textColor = [UIColor whiteColor];
        headerLabel.font = [UIFont boldSystemFontOfSize:17.0];
        
        return headerLabel;
    }
    else return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fundaBg-active"]];
    
    if (tableView.tag == COMP_FUNDA_LIST_TAG)
    {
        cell.textLabel.text = _companyFundaTitles[indexPath.row];
        cell.detailTextLabel.text = @"";
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"fundaBg.png"]];
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        //cell.textLabel.text = _tickerLookupResponse[indexPath.row][@"tickerName"];
        //cell.detailTextLabel.text = _tickerLookupResponse[indexPath.row][@"companyName"];
        cell.textLabel.text = _tickerLookupResults[indexPath.row][0];
        cell.detailTextLabel.text = _tickerLookupResults[indexPath.row][1];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self backgroundTouched];

    if (tableView.tag == COMP_FUNDA_LIST_TAG)
    {
        _selectedTitle = _companyFundaTitles[indexPath.row];
    }
    else
    {
        _tickerSymbol = _tickerLookupResults[indexPath.row][0];
        self.searchDisplayController.searchBar.text = @"";
        [fundamentalsList reloadData];
    }

    [self loadFundaViewForTitle:_selectedTitle];
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    BOOL isBackspace = text.length==0;
    BOOL isSearchButton = [text isEqualToString:@"\n"];
    
    if (isBackspace || isSearchButton)
        return YES;
    
    NSCharacterSet *letters = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"];
    
    BOOL isFirstLetter = searchBar.text.length==0;
    BOOL isLetter = [text rangeOfCharacterFromSet:letters].location != NSNotFound;
    
    if (isFirstLetter)
        return isLetter;
    
    NSCharacterSet *allowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~#$%^*-+=\\/:."] invertedSet];
    
    BOOL isValidCharacter = ([text rangeOfCharacterFromSet:allowedChars].location == NSNotFound);
    BOOL isValidSize = searchBar.text.length<10;
    
    return (isValidCharacter && isValidSize);
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar                     // called when text starts editing
{
    if (_tickerLookupResults.count>0)
    {
        if (![_tickerLookupResults[0][0] isEqualToString:searchBar.text])
        {
            //hit api
            [self fetchSearchResultsForString:searchBar.text];
        }
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    _tickerSymbol = searchBar.text.uppercaseString;
    [fundamentalsList reloadData];
    [self loadFundaViewForTitle:_companyFundaTitles[0]];
    
    [self backgroundTouched];
}

#pragma mark - UISearchDisplayDelegate
- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    if (searchString.length)
    {
        [self fetchSearchResultsForString:searchString];
        return YES;
    }
    else
    {
        [self backgroundTouched];
        return NO;
    }
}

- (void)fetchSearchResultsForString:(NSString*)searchString
{
    NSString *tickerLookUpURL = [NSString stringWithFormat:TICKER_LOOKUP_URL, searchString, TICKER_COUNT, TICKER_TYPE, TICKER_ORDERBY];
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:tickerLookUpURL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSDictionary *responseData = [[NSMutableDictionary alloc] initWithDictionary:JSON];
                                             
                                             _tickerLookupKeys = [[NSMutableArray alloc] initWithArray:responseData[@"$KEYS"]];
                                             _tickerLookupResults = [[NSMutableArray alloc] initWithArray:responseData[@"$RESULTS"]];
                                             
                                             //_tickerLookupResponse = [[NSMutableArray alloc] initWithArray:JSON];
                                             [self.searchDisplayController.searchResultsTableView reloadData];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             [self backgroundTouched];
                                             
                                             [[[UIAlertView alloc] initWithTitle:@"Error Retrieving Search Results"
                                                                         message:error.localizedDescription
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil] show];
                                         }];
    [operation start];

}

@end