//
//  TickerLookupViewController.m
//  Quodd_iPad
//
//  Created by Nishant on 20/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "TickerLookupViewController.h"
#import "TickerLookupCell.h"

#import <QuartzCore/QuartzCore.h>

#define LOOKUP_RESULTS_TABLE_TAG 10
#define SPECIAL_ISSUES_TABLE_TAG 11

#define TICKERLOOKUP_TABLEROW_HEIGHT 35.0

#define TICKER_COUNT    @"100"

#define kAddOn          @"ADD-ON"
#define kIssueType      @"ISSUE-TYPE"


@interface TickerLookupViewController ()

@property (nonatomic, retain) NSArray *viewsArray;

@property (nonatomic, retain) NSArray *searchByButtons;
@property (nonatomic, retain) NSArray *searchByOptions;
@property (nonatomic, retain) NSArray *searchTypeButtons;
@property (nonatomic, retain) NSArray *searchTypeOptions;

@property (nonatomic, retain) NSString *searchBy;
@property (nonatomic, retain) NSString *searchType;

//ticker lookup response
@property (nonatomic, retain) NSArray *tickerLookupKeys;
@property (nonatomic, retain) NSMutableArray *tickerLookupResults;

@property (nonatomic, retain) NSArray *fcTopSymbols;

@property (nonatomic, retain) UIPopoverController *selectPopOver;

@property (nonatomic, retain) NSString *selectedFCListTicker; //view2
@property (nonatomic, retain) NSString *filterFlag;

//view4 - special issues
@property (nonatomic, retain) NSArray *specialsIssueType;
@property (nonatomic, retain) NSArray *specialsAddOn;


@end

@implementation TickerLookupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _viewsArray = [[NSArray alloc] initWithObjects: view2, view3, view4, nil];
    
    _searchByButtons = [[NSArray alloc] initWithObjects:searchByName, searchByCUSIP, searchBySymbol, nil];
    _searchByOptions = [[NSArray alloc] initWithObjects:@"Company", @"CUSIP", @"Symbol", nil];
    _searchTypeButtons = [[NSArray alloc] initWithObjects:typeStock, typeMutualFund, typeIndex, typePref, typeAny, nil];
    _searchTypeOptions = [[NSArray alloc] initWithObjects:@"S", @"M", @"I", @"O", @"A", nil];

    _searchBy   = @"Company";       //default value
    _searchType = @"A";             //default value
    
    selectButton.layer.cornerRadius = 7.5;
    selectButton.layer.masksToBounds = YES;
    
    [self clearButtonPressed];
    
    [self fetchFCTopSymbols]; //for picker view
    [self fetchSpecialIssues];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    [Flurry logEvent:@"Ticker Lookup"];
    
    tickerLookupSegment.selectedSegmentIndex=0;
    [self segmentChanged:tickerLookupSegment];
    [tickerTextField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    //NSLog(@"%s",__PRETTY_FUNCTION__);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation==UIInterfaceOrientationLandscapeRight);
}

#pragma mark -
- (IBAction)closeView
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)fetchFCTopSymbols
{
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:FC_LIST_URL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             _fcTopSymbols = [[NSArray alloc] initWithArray:JSON];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Error Retrieving FCTopSymbols Data. %@", error.localizedDescription);
                                         }];
    [operation start];
    
}

- (void)fetchSpecialIssues
{
    NSURLRequest *request = [ProjectHandler urlRequestWithURL:SPECIAL_ISSUES_URL];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             _specialsIssueType = [[NSArray alloc] initWithArray:JSON[kIssueType]];
                                             _specialsAddOn = [[NSArray alloc] initWithArray:JSON[kAddOn]];
                                             
                                             [tableSpecialIssues reloadData];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Error retrieving SpecialIssues. %@", error.localizedDescription);
                                         }];
    [operation start];
    
}

- (IBAction)segmentChanged:(UISegmentedControl*)senderSegment
{
    [tickerTextField resignFirstResponder];
    [rootTextField resignFirstResponder];
    [descriptionTextField resignFirstResponder];
    
//    if (senderSegment.selectedSegmentIndex == 3) {
//        [[[UIAlertView alloc] initWithTitle:@"COMING SOON" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//        return;
//    }
    
    NSInteger index = senderSegment.selectedSegmentIndex;
    
    if (index>0) {
        [view4 removeFromSuperview];
        
        UIView *subview = _viewsArray[index-1];
        subview.frame = CGRectMake(0, 44, 1024, index==3 ? 704 : 200);
        [self.view addSubview:subview];
    }
    else {
        [view2 removeFromSuperview];
        [view3 removeFromSuperview];
        [view4 removeFromSuperview];
    }

    [self.view bringSubviewToFront:tickerLookupSegment];
}

- (void)startingAPI
{
    [tickerTextField resignFirstResponder];

    [rootTextField resignFirstResponder];
    [descriptionTextField resignFirstResponder];

    tableTickerLookupResults.alpha = 0.75;
    [loadingResults startAnimating];
    tableTickerLookupResults.hidden = YES;
}

- (void)finishingAPI
{
    tableTickerLookupResults.alpha = 1;
    [loadingResults stopAnimating];
    tableTickerLookupResults.hidden = NO;
    
    [tableTickerLookupResults reloadData];
}

#pragma mark view1
- (IBAction)searchBy:(UIButton*)senderButton
{
    for (UIButton *button in _searchByButtons) {
        [button setSelected:NO];
    }
    [senderButton setSelected:YES];
    
    _searchBy = _searchByOptions[[_searchByButtons indexOfObject:senderButton]];
    
    BOOL enableFlag = !(senderButton==searchByCUSIP);
    
    for (UIButton *button in _searchTypeButtons) {
        button.enabled = enableFlag;
    }
}

- (IBAction)searchType:(UIButton*)sender
{
    for (UIButton *button in _searchTypeButtons) {
        [button setSelected:NO];
    }
    
    [sender setSelected:YES];
    
    _searchType = _searchTypeOptions[[_searchTypeButtons indexOfObject:sender]];
}

- (IBAction)searchButtonPressed
{
    if (tickerTextField.text.length>0) {
        
        NSString *tickerLookUpURL = [[NSString alloc] init];
        
        if ([searchByCUSIP isSelected])
        {
            tickerLookUpURL = [NSString stringWithFormat:CUSIP_LOOKUP_URL, tickerTextField.text];
        }
        else
        {
            NSString *count = tickerTextField.text.length>2 ? TICKER_COUNT : @"5";
            tickerLookUpURL = [NSString stringWithFormat:TICKER_LOOKUP_URL, tickerTextField.text, count, _searchType, _searchBy];
        }
        
        [self fetchTickerLookupResponseWithURL:tickerLookUpURL];
    }
    else
        [[[UIAlertView alloc] initWithTitle:@"Empty field" message:@"Please enter search text." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)fetchTickerLookupResponseWithURL:(NSString*)url 
{
    [self startingAPI];

    NSURLRequest *request = [ProjectHandler urlRequestWithURL:url];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest: request
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                         {
                                             NSDictionary *responseData = [[NSMutableDictionary alloc] initWithDictionary:JSON];
                                             
                                             _tickerLookupKeys = [[NSMutableArray alloc] initWithArray:responseData[@"$KEYS"]];
                                             _tickerLookupResults = [[NSMutableArray alloc] initWithArray:responseData[@"$RESULTS"]];
                                             
                                             [self finishingAPI];
                                         }
                                         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                         {
                                             //NSLog(@"Error Retrieving TickerLookup Data. %@", error.localizedDescription);
                                             [self finishingAPI];
                                         }];
    [operation start];
}

- (IBAction)clearButtonPressed
{
    tableTickerLookupResults.hidden = YES;
    [selectButton setTitle:@"Select" forState:UIControlStateNormal];
}

#pragma mark view2
- (IBAction)selectButtonPressed
{
    [_selectPopOver dismissPopoverAnimated:YES];
        
    UIPickerView *selectPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 400, 216)];
    selectPicker.dataSource = self;
    selectPicker.delegate = self;
    selectPicker.showsSelectionIndicator = YES;

    //scroll to already selected row.
    for (int i=0; i<_fcTopSymbols.count; i++)
    {
        NSDictionary *dict = _fcTopSymbols[i];
        
        if ([dict[@"ticker"] isEqualToString:_selectedFCListTicker])
        {
            [selectPicker selectRow:i inComponent:0 animated:NO];
            break;
        }
    }
    
    UIViewController * popoverVC = [[UIViewController alloc] init];
    popoverVC.view = selectPicker;
    popoverVC.contentSizeForViewInPopover = selectPicker.frame.size;
    
    _selectPopOver = [[UIPopoverController alloc] initWithContentViewController:popoverVC];

    [_selectPopOver presentPopoverFromRect:selectButton.frame
                                    inView:view2
                  permittedArrowDirections:UIPopoverArrowDirectionLeft
                                  animated:YES];
}

- (IBAction)topSymbolsGoButtonPressed
{
    [_selectPopOver dismissPopoverAnimated:YES];

    if (![selectButton.titleLabel.text isEqualToString:@"Select"])
    {
        NSString *topSymbolsResponseURL = [NSString stringWithFormat:FC_LOOKUP_URL,_selectedFCListTicker,@"SYMB",@"false",@"0"];
        
        [self fetchTickerLookupResponseWithURL:topSymbolsResponseURL];
    }
}

#pragma mark view3
- (IBAction)rootDescSwitched:(UIButton*)senderButton
{
    if (senderButton == rootButton) {
        [descriptionButton setSelected:NO];
        [descriptionTextField setText:@""];
        
        [rootButton setSelected:YES];
        [rootTextField becomeFirstResponder];
    }
    else {
        [rootButton setSelected:NO];
        [rootTextField setText:@""];

        [descriptionButton setSelected:YES];
        [descriptionTextField becomeFirstResponder];
    }
}

- (IBAction)checkmarkPressed:(UIButton*)senderButton
{
    BOOL filterBool = !senderButton.isSelected;
    senderButton.selected = filterBool;
    _filterFlag = filterBool ? @"true" : @"false";
}

- (IBAction)fcLookupGoButtonPressed
{
    NSString *tickerText = rootButton.isSelected ? rootTextField.text : descriptionTextField.text;
    NSString *tickerType = rootButton.isSelected ? @"SYMB" : @"DESC";
    
    if (tickerText.length>0)
    {
        NSString *fcListResponse = [NSString stringWithFormat:FC_LOOKUP_URL,tickerText,tickerType,_filterFlag,@"0"];
        [self fetchTickerLookupResponseWithURL:fcListResponse];
    }
    else
        [[[UIAlertView alloc] initWithTitle:@"Empty field"
                                    message:@"Please enter search text."
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
}

#pragma mark view4
- (UITableViewCell*)specialIssuesCell:(UITableViewCell*)cell ForRow:(NSInteger)row
{
//    for (UILabel *subview in cell.contentView.subviews) {
//        [subview removeFromSuperview];
//    }

    CGFloat buffer = 10;
    
    CGFloat width = tableSpecialIssues.frame.size.width - 2*buffer;
    CGFloat height = TICKERLOOKUP_TABLEROW_HEIGHT;
    
    UILabel *leftLabel  = [[UILabel alloc] initWithFrame:CGRectMake(buffer, 0, width/2, height)];
    UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(width/2, 0, width/2, height)];
    
    leftLabel.text = _specialsIssueType[row];
    rightLabel.text = _specialsAddOn[row];
        
    [cell.contentView addSubview:leftLabel];
    [cell.contentView addSubview:rightLabel];
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

#pragma mark - UITextField
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == rootTextField)
    {
        [self rootDescSwitched:rootButton];
    }
    else if (textField == descriptionTextField)
    {
        [self rootDescSwitched:descriptionButton];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == tickerTextField)
    {
        [self searchButtonPressed];
    }
    else if (textField == rootTextField || textField == descriptionTextField)
    {
        [self fcLookupGoButtonPressed];
    }
    
    return YES;
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rows = tableView.tag == LOOKUP_RESULTS_TABLE_TAG ? _tickerLookupResults.count : _specialsIssueType.count;
    return rows;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch (tableView.tag)
    {
        case LOOKUP_RESULTS_TABLE_TAG:
        {
            UILabel *temp = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.sectionHeaderHeight)];
            
            temp.text = @"  Ticker             Company                                                   Exchg                              TYPE                  CURRENCY      CUSIP";
            
            return temp;
        }
            break;
            
        default:
        {
            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.sectionHeaderHeight)];
                        
            CGFloat buffer = 0;
            
            CGFloat width = tableView.frame.size.width - 2*buffer;
            CGFloat height = TICKERLOOKUP_TABLEROW_HEIGHT*0.8;
            
            UILabel *leftLabel  = [[UILabel alloc] initWithFrame:CGRectMake(buffer, 0, width/2, height)];
            UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(width/2, 0, width/2, height)];
            
            leftLabel.text = @"Issue Type";
            rightLabel.text = @"Quodd Add-On";

            leftLabel.font = [UIFont boldSystemFontOfSize:18.0];
            rightLabel.font = [UIFont boldSystemFontOfSize:18.0];
            
            leftLabel.textAlignment = NSTextAlignmentCenter;
            rightLabel.textAlignment = NSTextAlignmentCenter;

            leftLabel.textColor = [UIColor whiteColor];
            rightLabel.textColor = [UIColor whiteColor];

            UIColor *evenBgColor = [UIColor colorWithRed:10.0/255.0 green:24.0/255.0 blue:38.0/255.0 alpha:1.0];

            leftLabel.backgroundColor = evenBgColor;
            rightLabel.backgroundColor = evenBgColor;
            
            [headerView addSubview:leftLabel];
            [headerView addSubview:rightLabel];
            
            headerView.backgroundColor = [UIColor clearColor];
            
            return headerView;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return TICKERLOOKUP_TABLEROW_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag)
    {
        case LOOKUP_RESULTS_TABLE_TAG:
        {
            static NSString *CellIdentifier = @"TickerLookupCell";
            
            TickerLookupCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            if (cell == nil)
            {
                NSArray *topObjects = [[NSBundle mainBundle] loadNibNamed:@"TickerLookupCell" owner:self options:nil];
                for (id currentObject in topObjects)
                {
                    if ([currentObject isKindOfClass:[TickerLookupCell class]])
                    {
                        cell = (TickerLookupCell *)currentObject;
                        break;
                    }
                }
            }
            
            NSDictionary *dict = [[NSDictionary alloc] initWithObjects:_tickerLookupResults[indexPath.row] forKeys:_tickerLookupKeys];
            [cell loadWithDictionary:dict];
            
            return cell;
        }
            break;
            
        default:
        {
            static NSString *CellIdentifier = @"Cell";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            
            return [self specialIssuesCell:cell ForRow:indexPath.row];
        }
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - UIPickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return _fcTopSymbols.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *returnString = [NSString stringWithFormat:@"%@ [%@  @ %@]",_fcTopSymbols[row][@"desc"],_fcTopSymbols[row][@"ticker"],_fcTopSymbols[row][@"exchange"]];
    
    return returnString;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [selectButton setTitle:[self pickerView:thePickerView titleForRow:row forComponent:component] forState:UIControlStateNormal];
    _selectedFCListTicker = _fcTopSymbols[row][@"ticker"];
}

@end
