//
//  AppDelegate.m
//  Quodd_iPad2
//
//  Created by Nishant on 25/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import <Crashlytics/Crashlytics.h>
#import "Flurry.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Crashlytics
    [Crashlytics startWithAPIKey:@"d58d6b6837f558ea854d224dd9cb840dddde32ca"];

    //AFNetworking
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    
    //Flurry
    [Flurry setCrashReportingEnabled:NO]; //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
    [Flurry startSession:@"MKFN9NN6SJ76VKW7GKQ8"];
    
    
    //kSetValueForBool(kOptionShowType, NO);
    //kSetOptionDisplayTypeAlternate(NO);
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.loginVC = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    self.window.rootViewController = self.loginVC;

    
    _columnWidth = [[NSDictionary alloc] initWithObjectsAndKeys:
                    @"60", @"marketCenter",
                    @"120", @"yms",
                    @"80", @"time",
                    @"80", @"strike",
                    @"80", @"oI",
                    @"80", @"delta",
                    @"80", @"gamma",
                    @"80", @"theta",
                    @"80", @"intV",
                    @"80", @"timeV",
                    @"80", @"theoV",
                    @"80", @"dif",
                    @"80", @"iVol",
                    @"80", @"exp",
                    @"50", @"days",
                    @"40", @"cpp",
                    @"100", @"ask",
                    @"60", @"askExchg",
                    @"60", @"bidExchg",
                    @"110", @"s",
                    @"80", @"bid",
                    @"80", @"chg",
                    @"100", @"exchg",
                    @"80", @"high",
                    @"100", @"last",
                    @"80", @"low",
                    @"100", @"ltt",
                    @"70", @"ltv",
                    @"80", @"open",
                    @"80", @"perChg",
                    @"105", @"lastClosed",
                    @"120", @"vol",
                    @"80", @"vwap",
                    @"100", @"ticker",
                    @"180", @"tickerNameByUser",
                    @"25", @"T",
                    @"25", @"A",
                    @"25", @"ni",
                    @"25", @"isHalt",
                    nil];

    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
