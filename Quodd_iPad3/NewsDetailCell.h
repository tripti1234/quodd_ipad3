//
//  NewsDetailCell.h
//  Quood_iPad
//
//  Created by Nishant on 14/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailCell : UITableViewCell <UIWebViewDelegate>

- (void)loadNewsForURL:(NSString*)urlString;

@end
