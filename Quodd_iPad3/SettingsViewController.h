//
//  SettingsViewController.h
//  Quodd_iPad3
//
//  Created by Nishant on 11/09/13.
//  Copyright (c) 2013 Paxcel Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *userNameLabel;

@property (nonatomic, retain) IBOutlet UIButton *standardOSI;
@property (nonatomic, retain) IBOutlet UIButton *alternateOSI;


- (IBAction)closeView;
- (IBAction)optionsDisplayType:(UIButton*)senderButton;

@end
