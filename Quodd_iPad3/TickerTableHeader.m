//
//  TickerTableHeader.m
//  Quodd_iPad2
//
//  Created by Nishant on 09/07/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import "TickerTableHeader.h"

@interface TickerTableHeader ()

@end

@implementation TickerTableHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self)
    {
        // Initialization code
        self.frame = frame;
        
        UIButton *headerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        headerButton.frame = self.frame;
        headerButton.backgroundColor = [UIColor colorWithRed:9.0/255.0
                                                       green:11.0/255.0
                                                        blue:13.0/255.0
                                                       alpha:1.0];
        [headerButton addTarget:self
                         action:@selector(headerButton)
               forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:headerButton];
        
    }
    return self;
}

- (void)headerViewWithColumns:(NSArray *)columns
{
    static NSDictionary *columnWidth;
    
    if (!columnWidth) {
        columnWidth = [[NSDictionary alloc] initWithDictionary:[(AppDelegate *)[[UIApplication sharedApplication] delegate] columnWidth]];
    }

    CGFloat buffer=10;
    
    CGFloat desiredTotalWidth = self.frame.size.width - 2*buffer;
    CGFloat actualTotalWidth = 0.0;
    for (NSString *col in columns) {
        actualTotalWidth += [columnWidth[col] floatValue];
    }
    
    CGFloat multiplicationFactor = desiredTotalWidth / actualTotalWidth;
    
    CGFloat x = 0;
    CGFloat colWidth;// = headerFrame.size.width / _columns.count;
    CGFloat colHeight = self.frame.size.height;
    UILabel *label;
    
    for (UILabel *subview in self.subviews) {
        if ([subview isKindOfClass:[UILabel class]])
            [subview removeFromSuperview];
    }
    
    for (int i = 0; i< columns.count; i++)
    {
        colWidth = [columnWidth[columns[i]] floatValue] * multiplicationFactor;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(x+buffer, 0, colWidth, colHeight)];
        label.font = [UIFont systemFontOfSize:15];
        label.textColor = [UIColor whiteColor];
        label.backgroundColor = [UIColor clearColor];
        
        if (_formattedKeys[columns[i]]) {
            label.text = _formattedKeys[columns[i]];
        }
        else label.text = columns[i];
        
        if ([@[@"T", @"A", @"ni", @"isHalt", @"marketCenter", @"askExchg", @"bidExchg", @"cpp"] containsObject:columns[i]]) {
            label.textAlignment = NSTextAlignmentCenter;
        } else if (i>0) {
            label.textAlignment = NSTextAlignmentRight;
        }
        
        [self addSubview:label];
        
        x += colWidth;
    }
}

- (void)headerButton
{
    if (_delegate != nil && [_delegate respondsToSelector:@selector(modifyHeaderColumns)])
    {
        [_delegate modifyHeaderColumns];
    }
}

@end
