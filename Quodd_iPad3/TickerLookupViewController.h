//
//  TickerLookupViewController.h
//  Quodd_iPad
//
//  Created by Nishant on 20/05/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TickerLookupViewController : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UISegmentedControl *tickerLookupSegment;
    
    IBOutlet UIButton *searchByName;
    IBOutlet UIButton *searchByCUSIP;
    IBOutlet UIButton *searchBySymbol;
    
    IBOutlet UIButton *typeStock;
    IBOutlet UIButton *typeMutualFund;
    IBOutlet UIButton *typeIndex;
    IBOutlet UIButton *typePref;
    IBOutlet UIButton *typeAny;
    IBOutlet UITextField *tickerTextField;
    
    IBOutlet UIView *view2;
    IBOutlet UIButton *selectButton;
    
    IBOutlet UIView *view3;
    IBOutlet UIButton *rootButton;
    IBOutlet UIButton *descriptionButton;
    IBOutlet UITextField *rootTextField;
    IBOutlet UITextField *descriptionTextField;

    IBOutlet UIView *view4;
    IBOutlet UITableView *tableSpecialIssues;

    
    IBOutlet UIActivityIndicatorView *loadingResults;
    IBOutlet UITableView *tableTickerLookupResults;
    
}

- (IBAction)closeView;
- (IBAction)segmentChanged:(UISegmentedControl*)senderSegment;
- (IBAction)searchBy:(UIButton*)sender;
- (IBAction)searchType:(UIButton*)sender;
- (IBAction)searchButtonPressed;
- (IBAction)clearButtonPressed;

- (IBAction)selectButtonPressed;
- (IBAction)topSymbolsGoButtonPressed;

- (IBAction)rootDescSwitched:(UIButton*)senderButton;
- (IBAction)checkmarkPressed:(UIButton*)senderButton;
- (IBAction)fcLookupGoButtonPressed;

@end
