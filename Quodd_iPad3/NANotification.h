//
//  NANotification.h
//  Quood_iPad
//
//  Created by Nishant on 14/06/13.
//  Copyright (c) 2013 Paxcel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NANotification : UIView

- (id)initWithText:(NSString*)title;

@end
